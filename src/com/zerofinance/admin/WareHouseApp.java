package com.zerofinance.admin;

import com.floreysoft.gwt.picker.client.utils.PickerLoader;
import com.google.gwt.core.client.EntryPoint;
import com.zerofinance.server.login.oauth.OurOAuthParams;

public class WareHouseApp implements EntryPoint {

	private String API_KEY = "AIzaSyDMpMPo6UJKXNhvOgYMSygDsgiaGalWKZ4";

	@Override
	public void onModuleLoad() {
	    PickerLoader.loadApi(OurOAuthParams.GOOGLE_API_SECRET, new Runnable() {
	        public void run() {
	        	new Showcase();
	        }
	      });		
	}

}
