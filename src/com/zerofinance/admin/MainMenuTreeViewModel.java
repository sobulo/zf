/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.zerofinance.admin;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.prefetch.RunAsyncCode;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;
import com.zerofinance.admin.shellcontent.AdminCW;
import com.zerofinance.admin.shellcontent.AllStatusCW;
import com.zerofinance.admin.shellcontent.ApplicationBlotterCW;
import com.zerofinance.admin.shellcontent.BulkDealUpdateCW;
import com.zerofinance.admin.shellcontent.BulkPricingUpdateCW;
import com.zerofinance.admin.shellcontent.BulkProductUploadCW;
import com.zerofinance.admin.shellcontent.EditorCW;
import com.zerofinance.admin.shellcontent.MailingListCW;
import com.zerofinance.admin.shellcontent.MapChartCW;
import com.zerofinance.admin.shellcontent.MapTypesCW;
import com.zerofinance.admin.shellcontent.OrderBlotterCW;
import com.zerofinance.admin.shellcontent.OrderStatusCW;
import com.zerofinance.admin.shellcontent.PDFReportImagesCW;
import com.zerofinance.admin.shellcontent.ProductPricingCW;
import com.zerofinance.admin.shellcontent.ReviewerCW;
import com.zerofinance.admin.shellcontent.StatusCW;
import com.zerofinance.admin.shellcontent.StatusChartCW;
import com.zerofinance.admin.shellcontent.WelcomePanelCW;

/**
 * The {@link TreeViewModel} used by the main menu.
 */
public class MainMenuTreeViewModel implements TreeViewModel {

  /**
   * The cell used to render categories.
   */
  private static class CategoryCell extends AbstractCell<Category> {
    @Override
    public void render(Context context, Category value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * The cell used to render examples.
   */
  private static class ContentWidgetCell extends AbstractCell<ContentWidget> {
    @Override
    public void render(Context context, ContentWidget value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * A top level category in the tree.
   */
  public class Category {

    private final ListDataProvider<ContentWidget> examples =
        new ListDataProvider<ContentWidget>();
    private final String name;
    private NodeInfo<ContentWidget> nodeInfo;
    private final List<RunAsyncCode> splitPoints =
        new ArrayList<RunAsyncCode>();

    public Category(String name) {
      this.name = name;
    }

    public void addExample(ContentWidget example, String style, RunAsyncCode splitPoint) {
    	if(style != null)
    		example.addStyleName(style);
      examples.getList().add(example);
      if (splitPoint != null) {
        splitPoints.add(splitPoint);
      }
      contentCategory.put(example, this);
      contentToken.put(Showcase.getContentWidgetToken(example), example);
    }

    public String getName() {
      return name;
    }

    /**
     * Get the node info for the examples under this category.
     * 
     * @return the node info
     */
    public NodeInfo<ContentWidget> getNodeInfo() {
      if (nodeInfo == null) {
        nodeInfo = new DefaultNodeInfo<ContentWidget>(examples,
            contentWidgetCell, selectionModel, null);
      }
      return nodeInfo;
    }

    /**
     * Get the list of split points to prefetch for this category.
     * 
     * @return the list of classes in this category
     */
    public Iterable<RunAsyncCode> getSplitPoints() {
      return splitPoints;
    }
  }

  /**
   * The top level categories.
   */
  private final ListDataProvider<Category> categories = new ListDataProvider<Category>();

  /**
   * A mapping of {@link ContentWidget}s to their associated categories.
   */
  private final Map<ContentWidget, Category> contentCategory = new HashMap<ContentWidget, Category>();

  /**
   * The cell used to render examples.
   */
  private final ContentWidgetCell contentWidgetCell = new ContentWidgetCell();

  /**
   * A mapping of history tokens to their associated {@link ContentWidget}.
   */
  private final Map<String, ContentWidget> contentToken = new HashMap<String, ContentWidget>();

  /**
   * The selection model used to select examples.
   */
  private final SelectionModel<ContentWidget> selectionModel;

  public MainMenuTreeViewModel(SelectionModel<ContentWidget> selectionModel) {
    this.selectionModel = selectionModel;
    initializeTree();
  }

  /**
   * Get the {@link Category} associated with a widget.
   * 
   * @param widget the {@link ContentWidget}
   * @return the associated {@link Category}
   */
  public Category getCategoryForContentWidget(ContentWidget widget) {
    return contentCategory.get(widget);
  }

  /**
   * Get the content widget associated with the specified history token.
   * 
   * @param token the history token
   * @return the associated {@link ContentWidget}
   */
  public ContentWidget getContentWidgetForToken(String token) {
    return contentToken.get(token);
  }

  public <T> NodeInfo<?> getNodeInfo(T value) {
    if (value == null) {
      // Return the top level categories.
      return new DefaultNodeInfo<Category>(categories, new CategoryCell());
    } else if (value instanceof Category) {
      // Return the examples within the category.
      Category category = (Category) value;
      return category.getNodeInfo();
    }
    return null;
  }

  public boolean isLeaf(Object value) {
    return value != null && !(value instanceof Category);
  }

  /**
   * Get the set of all {@link ContentWidget}s used in the model.
   * 
   * @return the {@link ContentWidget}s
   */
  Set<ContentWidget> getAllContentWidgets() {
    Set<ContentWidget> widgets = new HashSet<ContentWidget>();
    for (Category category : categories.getList()) {
      for (ContentWidget example : category.examples.getList()) {
        widgets.add(example);
      }
    }
    return widgets;
  }

  /**
   * Initialize the top level categories in the tree.
   */
  private void initializeTree() {
    List<Category> catList = categories.getList();
    //Welcome
    {
      Category category = new Category("Welcome");
      catList.add(category);
      // CwCheckBox is the default example, so don't prefetch it.
      category.addExample(new WelcomePanelCW(),"homeWatermark", null);
    }

    //prospective
    {
    	//credit apps
    	Category category = new Category("Bank Reports");
    	category.addExample(new ApplicationBlotterCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(ApplicationBlotterCW.class));
    	category.addExample(new MapChartCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(MapChartCW.class));    	
    	category.addExample(new StatusChartCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(StatusChartCW.class));
    	catList.add(category);
    	
    	//orders
    	category = new Category("Shop Reports");
    	category.addExample(new ProductPricingCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(ProductPricingCW.class));
    	category.addExample(new OrderBlotterCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(OrderBlotterCW.class));
    	catList.add(category);
    }
       
    //prospective
    {
    	Category category = new Category("Edit/Search Bank");
       	category.addExample(new AllStatusCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(AllStatusCW.class));
       	category.addExample(new StatusCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(StatusCW.class));
       	catList.add(category);
       	
       	//orders
       	category = new Category("Edit/Search Shop");
       	category.addExample(new OrderStatusCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(OrderStatusCW.class));
       	category.addExample(new BulkPricingUpdateCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(BulkPricingUpdateCW.class));
       	category.addExample(new BulkDealUpdateCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(BulkDealUpdateCW.class));
       	
    	catList.add(category);
    }
    
    
    //prospective
    {
    	Category category = new Category("Config");
    	catList.add(category);
        category.addExample(new EditorCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(EditorCW.class));     
        category.addExample(new ReviewerCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(ReviewerCW.class));     
        category.addExample(new AdminCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(AdminCW.class));
        category.addExample(new MailingListCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(MailingListCW.class));        
        category.addExample(new MapTypesCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(MapTypesCW.class));         	        
        category.addExample(new BulkProductUploadCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(BulkProductUploadCW.class));                
        category.addExample(new PDFReportImagesCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(PDFReportImagesCW.class));        
    }      
  }
  
  public ContentWidget setBackgroundStyle(ContentWidget w, String styl)
  {
	  w.addStyleName(styl);
	  return w;
  }
}
