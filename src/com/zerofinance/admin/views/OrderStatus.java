package com.zerofinance.admin.views;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.PanelUtilities;
import com.zerofinance.admin.shellcontent.CommentsPanel;
import com.zerofinance.admin.shellcontent.widgs.ChangeFormStateHandler;
import com.zerofinance.admin.shellcontent.widgs.DrivePickerDemo;
import com.zerofinance.admin.shellcontent.widgs.SearchIDBox;
import com.zerofinance.admin.views.tables.ShowcaseTable;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.ApplicationFormConstants;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.WorkflowStateInstance;

public class OrderStatus extends ResizeComposite{

	@UiField
	SearchIDBox idBox;
	@UiField
	Label formState;
	@UiField
	Label formMessage;
	@UiField
	Label formStateDisplay;
	@UiField
	Button changeServerState;
	@UiField
	CommentsPanel commentsPanel;
	@UiField
	Label name;
	@UiField
	Label created;
	@UiField
	Label modified;
	@UiField
	Label company;
	@UiField
	Label email;
	@UiField
	Label phone;
	@UiField
	Label total;
	@UiField
	Anchor credit;	
	@UiField
	ShowcaseTable itemizedOrder;
	
	ChangeFormStateHandler changeHander;
	final static String FORM_LINK = "javascript:;";
	final static String EDIT_BASE = GWT.getHostPageBaseURL() + "../"; 
	//TODO remove dev edit form link, oauth now makes it impossible to use superdevmode to debug urgh!
	final static String EDIT_FORM_LINK = GWT.isProdMode()? EDIT_BASE + "#apply" : EDIT_BASE + "?gwt.codesvr=127.0.0.1:9997#apply";
	//String displayedLoanID = null;
	private AsyncCallback<List<TableMessage>> orderInfoCallBack = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) { 
			PanelUtilities.errorBox.show(caught.getMessage());
			idBox.setText("");
			idBox.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			idBox.setEnabled(true);
			if(result == null || result.size() == 0)
			{	
				String[] id = idBox.getText().split(":");
				String msg = "Unable to find order with id ";
				if(id.length > 1)
					msg += id[0];
				PanelUtilities.errorBox.show(msg);
				idBox.setText("");
				return;
			}
			//Window.alert("Result size: " + result.size());
			TableMessage summary = result.remove(0);
			idBox.setText(summary.getText(DTOConstants.ORDER_ID));
			changeDetailInfo(summary);
			//Window.alert("Changed Detail");
			itemizedOrder.showTable(result);
			//Window.alert("Displayed Order");
		}
	};
	

	
	private static ApplicationStatusUiBinder uiBinder = GWT
			.create(ApplicationStatusUiBinder.class);

	interface ApplicationStatusUiBinder extends
			UiBinder<Widget, OrderStatus> {
	}

	public OrderStatus() {
		initWidget(uiBinder.createAndBindUi(this));
		idBox.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				searchLoanID();
			}
		});
		//changeHander = new ChangeFormStateHandler(formState, formStateDisplay, formMessage);
		//changeServerState.addClickHandler(changeHander);
		changeServerState.setEnabled(false);
	}
	
	public void setOrderID(String ID)
	{
		clear();
		idBox.setText("Loading, please wait ...");
		idBox.setEnabled(false);
		changeServerState.setEnabled(false);
		changeHander.loanID = null;
		commentsPanel.clear();		
		if(ID == null || ID.trim().length() == 0) return;
		PanelUtilities.SHOP_SERVICE.getItemizedOrder(ID, orderInfoCallBack);
	}
	
	private void searchLoanID()
	{
		Long id = idBox.getValue();
		if(id == null)
		{
			PanelUtilities.errorBox.show(idBox.getText() + " [id should consist of numbers only]");
			return;
		}		
		clear();
		idBox.setEnabled(false);
		changeServerState.setEnabled(false);
		//changeHander.loanID = null;
		commentsPanel.clear();		
		idBox.setText(id + ": searching, please wait ...");		
		PanelUtilities.SHOP_SERVICE.getItemizedOrder(id, orderInfoCallBack);
	}
		
	private void changeDetailInfo(TableMessage orderDetails)
	{
		name.setText(orderDetails.getText(DTOConstants.ORDER_CUSTOMER_NAME));
		email.setText(orderDetails.getText(DTOConstants.ORDER_CUSTOMER_EMAIL));
		phone.setText(orderDetails.getText(DTOConstants.ORDER_CUSTOMER_PHONE));
		company.setText(orderDetails.getText(DTOConstants.ORDER_CUSTOMER_COMPANY));
		created.setText(GUIConstants.DEFAULT_DATE_FORMAT.format(orderDetails.getDate(DTOConstants.ORDER_DATE_CREATE)));
		modified.setText(GUIConstants.DEFAULT_DATE_FORMAT.format(orderDetails.getDate(DTOConstants.ORDER_DATE_MODIFIED)));
		total.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(orderDetails.getNumber(DTOConstants.ORDER_TOTAL)));
		String state = "";
		formState.setText(state);
		formStateDisplay.setText(state);
 	}
	
	private void clear()
	{
		itemizedOrder.clear();
		idBox.setValue(null);
		name.setText("");
		email.setText("");
		phone.setText("");
		company.setText("");
		formState.setText("");
		formStateDisplay.setText("");
	}
}
