package com.zerofinance.admin.views.tables;


import com.google.gwt.view.client.ProvidesKey;
import com.zerofinance.shared.TableMessage;

public class TableMessageKeyProvider implements ProvidesKey<TableMessage>{

    /**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<TableMessage> KEY_PROVIDER = new TableMessageKeyProvider();

	@Override
	public Object getKey(TableMessage item) {
		return item == null ? null : item.getMessageId();
	}
}
