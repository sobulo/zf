package com.zerofinance.admin.shellcontent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.CWArguments;
import com.zerofinance.admin.ContentWidget;
import com.zerofinance.admin.HelpPageGenerator;
import com.zerofinance.admin.Showcase;
import com.zerofinance.admin.views.ApplicationStatus;
import com.zerofinance.admin.views.OrderStatus;
import com.zerofinance.shared.TableMessage;

public class OrderStatusCW extends ContentWidget implements CWArguments{
	String savedId;
	OrderStatus statusPanel;
	public OrderStatusCW() {
		super("Order Status", "View details about a particular oder as well as editing order state");
	}

	@Override
	public void setParameterValues(String args) {
		if(statusPanel == null)
			savedId = args;
		else
			statusPanel.setOrderID(args);
	}

	@Override
	public Widget onInitialize() {
		statusPanel = new OrderStatus();
		if(savedId != null)
			statusPanel.setOrderID(savedId);
		return statusPanel;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(OrderStatusCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	static class OrderJumper implements ValueChangeHandler<TableMessage> 
	{

		@Override
		public void onValueChange(ValueChangeEvent<TableMessage> event) {
			History.newItem(Showcase.getContentWidgetToken(OrderStatusCW.class) + "/" + event.getValue().getMessageId());
		}
	}
}
