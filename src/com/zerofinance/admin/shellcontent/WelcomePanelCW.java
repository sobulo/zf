package com.zerofinance.admin.shellcontent;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.ContentWidget;
import com.zerofinance.admin.HelpPageGenerator;
import com.zerofinance.admin.views.WelcomePanel;
import com.zerofinance.base.MyRefreshCallback;

public class WelcomePanelCW extends ContentWidget implements MyRefreshCallback{

	WelcomePanel display;
	public WelcomePanelCW() {
		super("Welcome", "Use this page to login and out of the portal");
		display = new WelcomePanel();
	}

	@Override
	public Widget onInitialize() {
		return display;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		callback.onSuccess(onInitialize());
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	public WelcomePanel getPanel()
	{
		return display;
	}

	@Override
	public void updateScreen() {
		display.updateScreen();
	}

}
