package com.zerofinance.admin.shellcontent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.ContentWidget;
import com.zerofinance.admin.HelpPageGenerator;
import com.zerofinance.admin.views.BulkDealUploadPanel;
import com.zerofinance.admin.views.BulkVendorUploadPanel;

public class BulkDealUpdateCW extends ContentWidget{

	public BulkDealUpdateCW() {
		super("Bulk Deal Update", "Use this module to create/edit deal lists");
	}

	@Override
	public Widget onInitialize() {
		return new BulkDealUploadPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(BulkDealUpdateCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
