package com.zerofinance.admin.shellcontent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.shared.DTOConstants;

public class MailingListCW extends CWEditParamsBase{

	public MailingListCW() {
		super("Mailing List", "Listed below is list of people that have subscribed for the mailing list. " +
				" Use the excel button in the footer bar to generate an extract that can be imported into SendGrid.");
	}

	@Override
	protected boolean getShowValues() {
		return true;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_MAILING_LIST;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(MailingListCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}

}
