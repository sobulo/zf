package com.zerofinance.admin.shellcontent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.ContentWidget;
import com.zerofinance.admin.HelpPageGenerator;
import com.zerofinance.admin.views.BulkVendorUploadPanel;

public class BulkProductUploadCW extends ContentWidget{

	public BulkProductUploadCW() {
		super("Bulk Product Upload", "Use this module to upload products in bulk. Exercise caution as it overwrites any existing" +
				" details/prices if the product exists already. Typos in model number can also result in duplicate listings for products.");
	}

	@Override
	public Widget onInitialize() {
		return new BulkVendorUploadPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(BulkProductUploadCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}

}
