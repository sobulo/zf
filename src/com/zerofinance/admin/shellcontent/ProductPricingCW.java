package com.zerofinance.admin.shellcontent;

import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.admin.ContentWidget;
import com.zerofinance.admin.HelpPageGenerator;
import com.zerofinance.admin.PanelUtilities;
import com.zerofinance.admin.shellcontent.widgs.SearchDateBox;
import com.zerofinance.admin.views.tables.ShowcaseTable;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;

public class ProductPricingCW extends ContentWidget{
	

	public ProductPricingCW() {
		super("Product Pricing", "View product pricing information. You can download to excel as template for bulk pricing uploads");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		final ListBox manufacturerBox = new ListBox();
		final Button search = new Button("Search");
		for(String manufacturer : NameTokens.DEALERSHIP)
			manufacturerBox.addItem(manufacturer);
		HorizontalPanel p = new HorizontalPanel();
		p.setSpacing(20);
		p.add(manufacturerBox);
		p.add(search);
		final ShowcaseTable searchResults = new ShowcaseTable();	
		search.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
					search.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					searchResults.showTable(result);
					search.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				String manufacturer = manufacturerBox.getValue(manufacturerBox.getSelectedIndex());
				search.setEnabled(false);
				PanelUtilities.SHOP_SERVICE.getProductsPricing(manufacturer, searchCallback);
			}
		});
		container.addNorth(p, 50);
		container.add(searchResults);
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ProductPricingCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
