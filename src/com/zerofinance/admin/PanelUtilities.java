/**
 * 
 */
package com.zerofinance.admin;

import com.google.gwt.core.client.GWT;
import com.zerofinance.admin.shellcontent.widgs.SimpleDialog;
import com.zerofinance.base.LoginService;
import com.zerofinance.base.LoginServiceAsync;
import com.zerofinance.base.MortgageService;
import com.zerofinance.base.MortgageServiceAsync;
import com.zerofinance.base.ShopService;
import com.zerofinance.base.ShopServiceAsync;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);
    public final static ShopServiceAsync SHOP_SERVICE = GWT.create(ShopService.class);

    public static MortgageServiceAsync getLoanMktService()
    {
    	return LOAN_MKT_SERVICE;
    }
    
    public static LoginServiceAsync getLoginService()
    {
    	return READ_SERVICE;
    }	
}
