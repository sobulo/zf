package com.zerofinance.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;
import com.zerofinance.base.ShopService;
import com.zerofinance.server.entities.OrderDetails;
import com.zerofinance.server.entities.ProductDetails;
import com.zerofinance.server.entities.ProductPricing;
import com.zerofinance.server.entities.ShopDAO;
import com.zerofinance.server.entities.loans.ApplicationParameters;
import com.zerofinance.server.entities.loans.EntityDAO;
import com.zerofinance.server.login.LoginHelper;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.LoginRoles;
import com.zerofinance.shared.MissingEntitiesException;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.TableMessageHeader;
import com.zerofinance.shared.TableMessageHeader.TableMessageContent;

public class ShopServiceImpl extends RemoteServiceServlet implements ShopService{
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}

	@Override
	public TableMessage getProductInfo(String manufacturer, String modelNumber) {	
		ProductPricing[] pricing = new ProductPricing[1];
		ProductDetails details = ShopDAO.getProductInfo(pricing, manufacturer, modelNumber, ObjectifyService.begin());
		return getProductDetailsAsTableMessage(details, pricing[0]);
	}

	@Override
	public String saveProductInfo(TableMessage productInfo) {
		Key<ProductDetails> pk = null;
		if(productInfo.getMessageId().length() > 0)
			pk = ObjectifyService.factory().stringToKey(productInfo.getMessageId());
		
		Double[] priceVars = new Double[7];
		for(int i = 0; i < priceVars.length; i++)
			priceVars[i] = productInfo.getNumber(i);
		ProductDetails p =  ShopDAO.saveProductInfo(pk, productInfo.getText(DTOConstants.PRODUCT_MANUFACTURER), 
				productInfo.getText(DTOConstants.PRODUCT_MODEL), productInfo.getText(DTOConstants.PRODUCT_CLASSIFICATION),
				productInfo.getText(DTOConstants.PRODUCT_DESCRIPTION), productInfo.getText(DTOConstants.PRODUCT_FEATURES),
				null, priceVars, productInfo.getText(DTOConstants.PRODUCT_TITLE), productInfo.getText(DTOConstants.PRODUCT_IMAGE_URL));
		return p.getKey().getString();
	}
	
	private TableMessage getProductDetailsAsTableMessage(ProductDetails details, ProductPricing pricing)
	{
		if(details == null || pricing ==null) return null;
		TableMessage m = new TableMessage(7, 8, 0);
		m.setMessageId(details.getKey().getString());
		m.setText(DTOConstants.PRODUCT_CLASSIFICATION, details.getClassification());
		m.setText(DTOConstants.PRODUCT_TITLE, details.getTitle());
		m.setText(DTOConstants.PRODUCT_IMAGE_URL, details.getImageUrl());
		m.setText(DTOConstants.PRODUCT_DESCRIPTION, details.getDescription() == null? null : details.getDescription().getValue());
		m.setText(DTOConstants.PRODUCT_MANUFACTURER, details.getManufacturer());
		m.setText(DTOConstants.PRODUCT_MODEL, details.getModelNumber());
		m.setText(DTOConstants.PRODUCT_FEATURES, details.getFeatures());
		for(int i = 0; i < pricing.getPriceVariations().length; i++)
			m.setNumber(i, pricing.getPriceVariations()[i]);
		return m;
	}
	
	private TableMessage getProductPricingHeader()
	{
		TableMessageHeader h = new TableMessageHeader(10);
		h.setText(0,  "Manufacturer", TableMessageContent.TEXT);
		h.setText(1,  "Model No.", TableMessageContent.TEXT);
		h.setText(2,  "Dealer Price", TableMessageContent.NUMBER);
		h.setText(3,  "Retail Price", TableMessageContent.NUMBER);
		for(int i = 1; i <= 6; i++)
			h.setText(3 + i,  i + " Month Price", TableMessageContent.NUMBER);
		return h;
	}
	
	private TableMessage getProductPricingAsTableMessage(ProductDetails details, ProductPricing pricing)
	{
		if(details == null || pricing ==null) return null;
		TableMessage m = new TableMessage(2, 8, 0);
		m.setMessageId(details.getKey().getString());
		m.setText(DTOConstants.PRODUCT_MANUFACTURER, details.getManufacturer());
		m.setText(DTOConstants.PRODUCT_MODEL, details.getModelNumber());
		m.setNumber(0, pricing.getDealerPrice());
		for(int i = 0; i < pricing.getPriceVariations().length; i++)
			m.setNumber(i+1, pricing.getPriceVariations()[i]);		
		return m;
	}	

	@Override
	public HashMap<String, String> getProductDefinitiions() {
		HashMap<String, String> result = new HashMap<String, String>();
		for(Key<ProductDetails> pdk : ShopDAO.getAllProductDefinitions())
			result.put(pdk.getString(), pdk.getName());
		return result;
	}

	@Override
	public List<TableMessage> getProductsTable(String manufacturer, String[] categories, Integer limit) throws MissingEntitiesException {
		ArrayList<Key<ProductDetails>> detailList = new ArrayList<Key<ProductDetails>>();
		Objectify ofy = ObjectifyService.begin();
		for(String cat : categories)
		{
			Query<ProductDetails> query = ofy.query(ProductDetails.class).filter("classification", cat);
			if(manufacturer != null)
				query = query.filter("manufacturer", manufacturer);
			if(limit != null)
				query = query.limit(limit);
			detailList.addAll(query.listKeys());								
		}
		return getProductsTable(detailList, ofy);
	}
	
	private List<TableMessage> getProductsTable(Collection<Key<ProductDetails>> detailList, Objectify ofy) throws MissingEntitiesException
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getProductsHeader());
		List<Key<ProductPricing>> priceKeys = new ArrayList<Key<ProductPricing>>();
		for(Key<ProductDetails> dk : detailList)
			priceKeys.add(ProductPricing.getKey(dk));
		ArrayList<Key<? extends Object>> allKeys = new ArrayList<Key<? extends Object>>();
		allKeys.addAll(detailList);	
		allKeys.addAll(priceKeys);
		Map<Key<Object>, Object> objects = ofy.get(allKeys);
		for(Key<ProductPricing> pk : priceKeys)
		{
			ProductDetails d = (ProductDetails) objects.get(pk.getParent());
			String msg = "";
			if(d == null)
				msg += "Unable to get details for key: " + pk.getParent();
			ProductPricing p = (ProductPricing) objects.get(pk);
			if(p == null)
				msg += "Unable to get pricing for key: " + pk;
			TableMessage m = getProductDetailsAsTableMessage(d, p);
			if(msg.length() > 0) throw new MissingEntitiesException(msg);
			result.add(m);
		}
		return result;		
	}
	
	public TableMessageHeader getProductsHeader()
	{
		TableMessageHeader h = new TableMessageHeader(3);
		h.setText(0, "Manufacturer", TableMessageContent.TEXT);
		h.setText(1, "Model", TableMessageContent.TEXT);
		h.setText(2, "Classification", TableMessageContent.TEXT);
		return h;
	}

	@Override
	public List<TableMessage> getCart() {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getCartHeader());
		CartSessionHelper.fetchCustomerCart(getThreadLocalRequest().getSession(), result);
		return result;
	}

	TableMessageHeader getCartHeader()
	{
		TableMessageHeader h = new TableMessageHeader(6);
		h.setText(DTOConstants.CART_PRODUCT_ID, "Item", TableMessageContent.TEXT);
		h.setText(DTOConstants.CART_PRODUCT_TITLE, "Description", TableMessageContent.TEXT);
		int numOffset = 2;
		h.setText(DTOConstants.CART_PRODUCT_REPAY + numOffset, "Lease (months)", TableMessageContent.NUMBER);
		h.setText(DTOConstants.CART_PRODUCT_QTY + numOffset, "Qty", TableMessageContent.NUMBER);
		h.setText(DTOConstants.CART_PRODUCT_UNIT_PRICE + numOffset, "Unit Price", TableMessageContent.NUMBER);
		h.setText(DTOConstants.CART_PRODUCT_TOTAL_PRICE + numOffset, "Total Price", TableMessageContent.NUMBER);
		return h;
	}
	
	TableMessageHeader getOrderSummaryHeader()
	{	    
	    TableMessageHeader h = new TableMessageHeader(10);
	    h.setText(0, "ID", TableMessageContent.TEXT);
	    h.setText(1, "Name", TableMessageContent.TEXT);
	    h.setText(2, "Email", TableMessageContent.TEXT);
	    h.setText(3, "Phone", TableMessageContent.TEXT);
	    h.setText(4, "Address", TableMessageContent.TEXT);
	    h.setText(5, "Total", TableMessageContent.NUMBER);
	    h.setText(6, "Created", TableMessageContent.DATE);
	    h.setText(7, "Modified", TableMessageContent.DATE);
	    h.setText(8, "Status", TableMessageContent.TEXT);
	    h.setText(9, "Message", TableMessageContent.TEXT);
	    return h;
	}

	@Override
	public String addToCart(String productID, int numOfMonths, int numOfItems,
			boolean isUpdate) {
		CartSessionHelper.addToCart(productID, numOfItems, numOfMonths, getThreadLocalRequest().getSession(), isUpdate);
		return "added new items to cart";
	}

	@Override
	public List<TableMessage> checkout(String name, String email, String phone,
			String company) throws MissingEntitiesException {
		OrderDetails detail = ShopDAO.createNewOrder(getThreadLocalRequest().getSession(), name, phone, email, company);
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(2);
		result.add(getOrderSummaryHeader());
		result.add(ShopDAO.getOrderSummaryMessage(detail));
		return result;
	}

	@Override
	public TableMessage getProductInfo(String productID) {
		Key<ProductDetails> pdk = new Key<ProductDetails>(productID);
		Key<ProductPricing> ppk = ProductPricing.getKey(pdk);
		Map<Key<Object>, Object> pd = ObjectifyService.begin().get(pdk, ppk);
		return getProductDetailsAsTableMessage((ProductDetails) pd.get(pdk),(ProductPricing) pd.get(ppk));
	}

	@Override
	public List<TableMessage> getOrders() {
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(email == null) return null;
		LoginRoles role = LoginHelper.getRole(email);
		//if(!role.equals(LoginRoles.ROLE_PUBLIC))
		//	return null;
		return getOrders(email);
	}

	@Override
	public List<TableMessage> getOrders(String email) {
		List<OrderDetails> myOrders = ObjectifyService.begin().query(OrderDetails.class).filter("email", email).list();
		return getOrders(myOrders);
	}
	
	public List<TableMessage> getOrders(List<OrderDetails> orders)
	{
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getOrderSummaryHeader());
		for(OrderDetails od : orders)
			result.add(ShopDAO.getOrderSummaryMessage(od));	
		return result;
	}
	

	@Override
	public List<TableMessage> getOrdersByDate(Date start, Date end) {
		List<OrderDetails> myOrders = ObjectifyService.begin().query(OrderDetails.class).filter("createDate >=", start).
				filter("createDate <=", end).list();
		return getOrders(myOrders);
	}

	@Override
	public List<TableMessage> getItemizedOrder(String orderID) {
		Key<OrderDetails> odk = new Key<OrderDetails>(orderID);
		OrderDetails od = ObjectifyService.begin().get(odk);
		return getItemizedOrder(od);
	}
	
	List<TableMessage> getItemizedOrder(OrderDetails od)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(ShopDAO.getOrderSummaryMessage(od));
		result.add(getCartHeader());
		result.addAll(ShopDAO.getOrderItemsMessages(od));
		return result;		
	}

	@Override
	public List<TableMessage> getItemizedOrder(long orderID) {
		Key<OrderDetails> odk = new Key<OrderDetails>(OrderDetails.class, orderID);
		OrderDetails od = ObjectifyService.begin().find(odk);
		if(od == null)
			return null;
		return getItemizedOrder(od);

	}

	@Override
	public List<TableMessage> getProductsPricing(String manufacturer) throws MissingEntitiesException {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		ArrayList<Key<ProductDetails>> detailList = new ArrayList<Key<ProductDetails>>();
		List<Key<ProductPricing>> priceKeys = new ArrayList<Key<ProductPricing>>();
		result.add(getProductPricingHeader());

		Query<ProductDetails> query = ObjectifyService.begin().query(ProductDetails.class).filter("manufacturer", manufacturer);
		detailList.addAll(query.listKeys());								
		
		for(Key<ProductDetails> dk : detailList)
			priceKeys.add(ProductPricing.getKey(dk));
		
		ArrayList<Key<? extends Object>> allKeys = new ArrayList<Key<? extends Object>>();
		allKeys.addAll(detailList);	
		allKeys.addAll(priceKeys);
		Map<Key<Object>, Object> objects = ObjectifyService.begin().get(allKeys);
		for(Key<ProductPricing> pk : priceKeys)
		{
			ProductDetails d = (ProductDetails) objects.get(pk.getParent());
			String msg = "";
			if(d == null)
				msg += "Unable to get details for key: " + pk.getParent();
			ProductPricing p = (ProductPricing) objects.get(pk);
			if(p == null)
				msg += "Unable to get pricing for key: " + pk;
			TableMessage m = getProductPricingAsTableMessage(d, p);
			if(msg.length() > 0) throw new MissingEntitiesException(msg);
			result.add(m);
		}
		
		return result;
	}

	@Override
	public List<TableMessage> getProductsTable(String dealID)
			throws MissingEntitiesException {
		Key<ApplicationParameters> apk = ApplicationParameters.getKey(dealID);
		ApplicationParameters dl = ObjectifyService.begin().find(apk);
		if(dl == null)
			throw new MissingEntitiesException("Unable to find deal list with id: " + dealID);
		HashMap<String, String> productKeys = dl.getParams();
		TreeMap<Integer, Key<ProductDetails>> productRanking = new TreeMap<Integer, Key<ProductDetails>>();
		ArrayList<Key<ProductDetails>> detailList = new ArrayList<Key<ProductDetails>>();
		for(String id : productKeys.keySet())
		{
			Key<ProductDetails> dlk = new Key<ProductDetails>(id);
			productRanking.put(Integer.valueOf(productKeys.get(id)), dlk);
		}
		return getProductsTable(productRanking.values(), ObjectifyService.begin());
	}

	@Override
	public String subscribeToMailingList(String name, String email) {
		Key<ApplicationParameters> listKey = ApplicationParameters.getKey(DTOConstants.APP_PARAM_MAILING_LIST);
		Objectify ofy = ObjectifyService.beginTransaction();
		String returnVal = "";
		try
		{
			ApplicationParameters mailingList = ofy.get(listKey);
			HashMap<String, String> addresses = mailingList.getParams();
			if(addresses.containsKey(email))
				returnVal = email + " has already subscribed to our mailing list. Look out for our monthly updates on the latest offers available at zerofinance.com.ng";
			else
			{
				returnVal = "Thanks for subscribing to our mailing list. Updates on our latest offers will be sent to " + returnVal;
				addresses.put(email, name);
				mailingList.setParams(addresses);
				ofy.put(mailingList);
				ofy.getTxn().commit();
			}
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return returnVal;
	}
}
