/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zerofinance.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com.googlecode.objectify.ObjectifyService;
import com.zerofinance.server.entities.OrderDetails;
import com.zerofinance.server.entities.OrderItem;
import com.zerofinance.server.entities.ProductDetails;
import com.zerofinance.server.entities.ProductPricing;
import com.zerofinance.server.entities.loans.ApplicantUniqueIdentifier;
import com.zerofinance.server.entities.loans.ApplicationParameters;
import com.zerofinance.server.entities.loans.InetImageBlob;
import com.zerofinance.server.entities.loans.MortgageApplicationFormData;
import com.zerofinance.server.entities.loans.MortgageAttachment;
import com.zerofinance.server.entities.loans.MortgageComment;
import com.zerofinance.server.entities.loans.SalesLead;
import com.zerofinance.server.entities.loans.SalesLeadIDHelper;
import com.zerofinance.server.messages.EmailController;


//import java.util.GregorianCalendar;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GeneralFuncs {
	
    public static <T> boolean addToHashSet( Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());
        //seems we can't rely on boolean result of add as datanucleus backed
        //hashet subclass successfully adds @ least on local server even when
        //element is already present. contains works though so check that first
        if(hash.contains(o))
            return false;
        return hash.add(o);
    }

    public static <T> boolean removeFromHashSet(Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());

        if(!hash.contains(o))
            return false;

        return hash.remove(o);
    }

    public static <K, V> boolean addToHashMap(HashMap<K, V> hashMap, K key, V value)
    {
        if(hashMap.containsKey(key))
            return false;
        hashMap.put(key, value);
        return true;
    }

    public static <K, V> boolean updateHashMap(HashMap<K, V> hashMap, K key, V val)
    {
        if(!hashMap.containsKey(key))
            return false;
        hashMap.put(key, val);
        return true;
    }

    public static <K, V> boolean removeFromHashMap(HashMap<K, V> hashMap, K key)
    {
        return( hashMap.remove(key) == null? false : true);
    }

   /* public static Date getDate(int year, int month, int day)
    {
        //offset month by 1 as gregorian calendar represents months from 0-11
        return (new GregorianCalendar(year, month - 1, day)).getTime();
    }*/

    /**
     *
     * @param <T>
     * @param a - first array
     * @param b - second array to compare with first
     * @param intersect
     * @param aOnly - elements found in aOnly
     * @param bOnly - elements found in bOnly
     */
    public static <T extends Comparable< T > > void arrayDiff(T[] a, T[] b,
            ArrayList<T> intersect, ArrayList<T> aOnly, ArrayList<T> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }
    }
    
    public static boolean isOfyRegistered = false;
    public static void registerClassesWithOfy()
    {
    	if(isOfyRegistered)
    		return;
    	
    	//shop
    	ObjectifyService.register(ProductDetails.class);
    	ObjectifyService.register(ProductPricing.class);
    	ObjectifyService.register(OrderDetails.class);
    	//ObjectifyService.register(OrderItem.class);
    	
    	//loans
    	ObjectifyService.register(ApplicationParameters.class);
	    ObjectifyService.register(ApplicantUniqueIdentifier.class);
	    ObjectifyService.register(SalesLead.class);
	    ObjectifyService.register(MortgageComment.class);
	    ObjectifyService.register(MortgageApplicationFormData.class);
	    ObjectifyService.register(InetImageBlob.class);
	    ObjectifyService.register(SalesLeadIDHelper.class);
	    ObjectifyService.register(MortgageAttachment.class); 
	    ObjectifyService.register(EmailController.class); 
	    
    	
    	isOfyRegistered = true;
    	
    }
    
    /*public static <T> void arrayDiff(Key<T>[] a, Key<T>[] b,
            ArrayList<Key<T>> intersect, ArrayList<Key<T>> aOnly, ArrayList<Key<T>> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }

    }*/    
}
