package com.zerofinance.server.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.zerofinance.server.CartSessionHelper;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.MissingEntitiesException;
import com.zerofinance.shared.TableMessage;

public class ShopDAO {
	public static ProductDetails getProductInfo(
			ProductPricing[] pricingResultBuffer, String manufacturer,
			String modelNumber, Objectify ofy) {
		Key<ProductDetails> pk = ProductDetails.getKey(modelNumber,
				manufacturer);
		ProductDetails result = ofy.find(pk);
		if (result != null)
			pricingResultBuffer[0] = ofy.get(ProductPricing.getKey(pk));
		return result;
	}

	public static ProductDetails saveProductInfo(Key<ProductDetails> pk,
			String manufacturer, String model, String classification,
			String description, String features, Double dealerPrice,
			Double[] priceVars, String title, String url) {
		ProductDetails p;
		ProductPricing prc;
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			if (pk != null) {
				Map<Key<Object>, Object> objs = ofy.get(pk,
						ProductPricing.getKey(pk));
				p = (ProductDetails) objs.get(pk);
				p.setClassification(classification);
				p.setDescription(new Text(description));
				p.setFeatures(features);

				prc = (ProductPricing) objs.get(ProductPricing.getKey(pk));
				prc.setDealerPrice(dealerPrice);
				prc.setPriceVariations(priceVars);
				ofy.put(p, prc);
			} else {
				p = new ProductDetails(manufacturer, model, classification,
						description == null ? null : new Text(description),
						features, title, url);
				ofy.put(p);
				prc = new ProductPricing(p.getKey(), dealerPrice, priceVars);
				ofy.put(prc);
			}
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return p;
	}
	
	public static ProductPricing updateProductPricing(ProductPricing pp, Double[] priceVars, Double dealerPrice, Objectify ofy)
	{
		pp.setPriceVariations(priceVars);
		pp.setDealerPrice(dealerPrice);
		return pp;
	}

	public static List<Key<ProductDetails>> getAllProductDefinitions() {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(ProductDetails.class).listKeys();
	}
	
	public static OrderDetails createNewOrder(HttpSession sess, String name, String number, String emailAddy, String company) throws MissingEntitiesException
	{
		ArrayList<TableMessage> customerCart = new ArrayList<TableMessage>();
		CartSessionHelper.fetchCustomerCart(sess, customerCart);
		if(customerCart.size() == 0)
			throw new MissingEntitiesException("Stale session? Lease cart is empty");
		ArrayList<OrderItem> cartList = new ArrayList<OrderItem>();
		for(TableMessage cartItem : customerCart)
		{
			Key<ProductDetails> pdk = new Key<ProductDetails>(cartItem.getMessageId());
			OrderItem item = new OrderItem(pdk, (int) Math.round(cartItem.getNumber(DTOConstants.CART_PRODUCT_QTY)),
					(int) Math.round(cartItem.getNumber(DTOConstants.CART_PRODUCT_REPAY)), cartItem.getNumber(DTOConstants.CART_PRODUCT_UNIT_PRICE),
					cartItem.getText(DTOConstants.CART_PRODUCT_ID), cartItem.getText(DTOConstants.CART_PRODUCT_TITLE));
			cartList.add(item);
		}
		Objectify ofy = ObjectifyService.beginTransaction();
		OrderDetails order = null;
		try
		{
			order = new OrderDetails(name, number, emailAddy, company, cartList);
			ofy.put(order);
			CartSessionHelper.emptyCart(sess);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return order;
	}
	
	public static TableMessage getOrderSummaryMessage(OrderDetails detail)
	{
		TableMessage m = new TableMessage(8, 2, 2);
		m.setText(DTOConstants.ORDER_CUSTOMER_COMPANY, detail.company);
		m.setText(DTOConstants.ORDER_CUSTOMER_EMAIL, detail.email);
		m.setText(DTOConstants.ORDER_CUSTOMER_NAME, detail.fullName);
		m.setText(DTOConstants.ORDER_CUSTOMER_PHONE, detail.phoneNumber);
		m.setText(DTOConstants.ORDER_LOAN_ID, detail.leadKey == null?"":detail.leadKey.getString());
		m.setText(DTOConstants.ORDER_ID, String.valueOf(detail.orderID));
		m.setText(DTOConstants.ORDER_STATUS, detail.status.toString());
		m.setText(DTOConstants.ORDER_MESSAGE, detail.orderMessage == null? null : detail.orderMessage.getValue());
		m.setDate(DTOConstants.ORDER_DATE_CREATE, detail.createDate);
		m.setDate(DTOConstants.ORDER_DATE_MODIFIED, detail.lastModified);
		double total = 0.0;
		int maxTenor = 0;
		for(OrderItem item : detail.orderList)	
		{
			if(maxTenor < item.numOfPayments)
				maxTenor = item.numOfPayments;
			total += (item.unitPrice * item.quantity * item.numOfPayments);
		}
		m.setNumber(DTOConstants.ORDER_TOTAL, total);
		m.setNumber(DTOConstants.ORDER_TENOR, maxTenor);
		m.setMessageId(detail.getKey().getString());
		return m;
	}
	
	public static List<TableMessage> getOrderItemsMessages(OrderDetails order)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		for(OrderItem item : order.orderList)
		{
			TableMessage m = new TableMessage(2, 4, 0);
			m.setText(DTOConstants.CART_PRODUCT_ID, item.productID);
			m.setText(DTOConstants.CART_PRODUCT_TITLE, item.productTitle);
			m.setMessageId(item.productKey.getString());
			m.setNumber(DTOConstants.CART_PRODUCT_QTY, item.quantity);
			m.setNumber(DTOConstants.CART_PRODUCT_UNIT_PRICE, item.unitPrice);
			m.setNumber(DTOConstants.CART_PRODUCT_REPAY, item.numOfPayments);
			m.setNumber(DTOConstants.CART_PRODUCT_TOTAL_PRICE, item.unitPrice * item.quantity);
			result.add(m);
		}
		return result;
	}
}
