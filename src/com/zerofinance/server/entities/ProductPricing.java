package com.zerofinance.server.entities;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class ProductPricing {
	@Id
	String type;
	Double dealerPrice;
	Double[] priceVariations;
	@Parent Key<ProductDetails> detailsKey;
	static String defaultType = "DEFAULT";
	
	ProductPricing()
	{
		type = defaultType;
	};
	
	ProductPricing(Key<ProductDetails> parentKey, Double dealerPrice, Double[] priceVariations)
	{
		this();
		detailsKey = parentKey;
		this.dealerPrice = dealerPrice;
		this.priceVariations = priceVariations;
	}
	
	public static Key<ProductPricing> getKey(Key<ProductDetails> parent, String id)
	{
		return new Key<ProductPricing>(parent, ProductPricing.class, id);
	}
	
	public Key<ProductPricing> getKey()
	{
		return getKey(detailsKey, type);
	}
	
	public static Key<ProductPricing> getKey(Key<ProductDetails> parent)
	{
		return getKey(parent, defaultType); 
	}

	void setDealerPrice(Double dealerPrice) {
		this.dealerPrice = dealerPrice;
	}

	void setPriceVariations(Double[] priceVariations) {
		this.priceVariations = priceVariations;
	}

	public Double getDealerPrice() {
		return dealerPrice;
	}

	public Double[] getPriceVariations() {
		return priceVariations;
	}

	Key<ProductDetails> getDetailsKey() {
		return detailsKey;
	}

	
}
