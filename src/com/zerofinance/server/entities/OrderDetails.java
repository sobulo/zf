package com.zerofinance.server.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;
import com.zerofinance.server.entities.loans.SalesLead;
import com.zerofinance.shared.OrderStatus;

@Unindexed
public class OrderDetails {
	@Id
	long orderID;
	String phoneNumber;
	String fullName;
	@Indexed String email;
	@Indexed String company;
	@Indexed Date createDate;
	Date lastModified;
	Text orderMessage;
	OrderStatus status;
	Key<SalesLead> leadKey;
	@Embedded List<OrderItem> orderList; 
	//TODO add key of credit application
	
	Key<OrderDetails> getKey()
	{
		return new Key<OrderDetails>(OrderDetails.class, orderID);
	}
	
	OrderDetails()
	{
		orderList = new ArrayList<OrderItem>();
		createDate = new Date();
		status = OrderStatus.PENDING_CREDIT_APPLICATION;
	}
	
	public void setLeadKey(Key<SalesLead> lk)
	{
		String loanEmail = lk.getParent().getName();
		if(loanEmail.equalsIgnoreCase(email))
			leadKey = lk;
		else
			throw new IllegalArgumentException("Mistmatch found: attempted to map loan with email " + loanEmail + " to order id " 
												+ orderID + " with email " + email);
	}
	
	public OrderDetails(String name, String number, String emailAddy, String company, ArrayList<OrderItem> cartList)
	{
		this();
		this.phoneNumber = number;
		this.email = emailAddy.toLowerCase().trim();
		this.company = company;
		this.fullName = name;
		this.orderList = cartList;
		this.orderID = ObjectifyService.factory().allocateId(OrderDetails.class);
	}
	
	@PrePersist void timeStamp()
	{
		lastModified = new Date();
	}
}