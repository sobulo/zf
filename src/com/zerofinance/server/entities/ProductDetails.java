package com.zerofinance.server.entities;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * manufacturer + model number should be unique
 * 
 * open question as to whether to add a tonne of other fields or just have it all as free form text in the description 
 * bit
 * 
 * @author sobulo@fertiletech.com
 *
 */
@Unindexed
@Cached
public class ProductDetails {
	@Id
	String key;
	@Indexed String manufacturer;
	String modelNumber;
	@Indexed String classification; //air conditioner, tv, etc
	String title;
	Text description;
	Text features;
	String imageUrl;
	
	ProductDetails(){}
		
	ProductDetails(String manufacturer, String modelNumber,
			String classification, Text description, String features, String title, String imageUrl) {
		super();
		this.manufacturer = manufacturer;
		this.modelNumber = modelNumber;
		this.classification = classification;
		this.description = description;
		this.features = features==null?null:new Text(features);
		this.title = title;
		this.key = getKeyString(modelNumber, manufacturer);
		this.imageUrl = imageUrl;
	}
	
	public Key<ProductDetails> getKey()
	{
		return getKey(modelNumber, manufacturer);
	}

	private static String getKeyString(String modelNumber, String manufacturer)
	{
		return modelNumber.trim().toLowerCase() + " | " + manufacturer.trim().toLowerCase(); 		
	}
	
	public static Key<ProductDetails> getKey(String modelNumber, String manufacturer)
	{
		return new Key<ProductDetails>(ProductDetails.class, getKeyString(modelNumber, manufacturer));
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public String getModelNumber() {
		return modelNumber;
	}
	
	void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public String getClassification() {
		return classification;
	}
	
	void setClassification(String classification) {
		this.classification = classification;
	}
	
	public Text getDescription() {
		return description;
	}
	
	void setDescription(Text description) {
		this.description = description;
	}
	
	public String getFeatures() {
		return (features == null? null : features.getValue());
	}
	
	void setFeatures(String features) {
		this.features = (features == null? null : new Text(features));
	}	
	
	public String getTitle()
	{
		return title;
	}
	
	public String getImageUrl()
	{
		return imageUrl;
	}
}
