package com.zerofinance.server.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class OrderItem {

	@Indexed Key<ProductDetails> productKey;
	String productID;
	String productTitle;
	int quantity;
	int numOfPayments;
	double unitPrice;
	
	OrderItem(){}
	
	OrderItem(Key<ProductDetails> pk, int qty, int numMonths, double price, String productID, String productDesc)
	{
		this.productKey = pk;
		this.numOfPayments = numMonths;
		this.unitPrice = price;
		this.quantity = qty;
		this.productID = productID;
		this.productTitle = productDesc;
	}
}
