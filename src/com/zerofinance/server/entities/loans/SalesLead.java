/**
 * 
 */
package com.zerofinance.server.entities.loans;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfNotNull;
import com.zerofinance.shared.WorkflowStateInstance;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class SalesLead{
	
	@Id
	long key;
	
	//customer fields -- potential index candidates, for now useful to have these without having to load forms
	String phoneNumber;
	String companyName;
	String fullName;
	Double annualSalary;
	Double loanAmount;
	Integer tenor;
	String displayMessage;
	
	WorkflowStateInstance currentState;
	
	//indexed fields
	@Indexed(IfNotNull.class) String bankLoanId;
	@Indexed Date dateCreated;
	@Indexed Long keyCopy;
	
	//auto-populated fields
	Date dateUpdated; 
	String updateUser;	
	
	@Parent Key<ApplicantUniqueIdentifier> parentKey;
	
	public String getEmail(){
		return parentKey.getName();
	}
	

	public String getBankLoanId() {
		return bankLoanId;
	}

	void setBankLoanId(String bankLoanId) {
		this.bankLoanId = bankLoanId;
	}

	public Boolean getLoanApproved() {
		return currentState.isApprovedState();
	}

	public SalesLead() {} //empty constructor required by objectify
	
	public SalesLead(String email, long id)
	{
		this.parentKey = new Key<ApplicantUniqueIdentifier>(ApplicantUniqueIdentifier.class, email);
		this.currentState = WorkflowStateInstance.APPLICATION_STARTED;
		this.dateCreated = new Date(); 	
		this.key = id;
	}
	
	void setDecisionFields(
			String phoneNumber,
			String companyName,
			String fullName,
			Double annualSalary,
			Double loanAmount,
			Integer tenor
	)
	{
		this.phoneNumber = phoneNumber;
		this.companyName = companyName;
		this.fullName = fullName;
		this.annualSalary = annualSalary;
		this.tenor = tenor;
		this.loanAmount = loanAmount;
	}
	
	SalesLead makeCopy(long id)
	{
		if(!allowNewSiblings()) throw new RuntimeException("Copying this lead is not allowed. Contact support for assistance");
		SalesLead copy = new SalesLead(parentKey.getName(), id);
		copy.setDecisionFields(phoneNumber, companyName, fullName, null, null, null);
		return copy;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	
	public String getCompanyName() {
		return companyName;
	}
		
	public String getFullName() {
		return fullName;
	}
		
	public Double getAnnualSalary() {
		return annualSalary;
	}
			
	public Date getDateCreated() {
		return dateCreated;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public long getLeadID()
	{
		return key;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	String getUpdateUser() {
		return updateUser;
	}
	
	public Key<SalesLead> getKey()
	{
		return new Key<SalesLead>(parentKey, SalesLead.class, key);
	}

	public WorkflowStateInstance getCurrentState() {
		return currentState;
	}


	public void setCurrentState(WorkflowStateInstance state) {
		this.currentState = state;
	}
	
	public String getDisplayMessaage()
	{
		return displayMessage;
	}
	
	void setDisplayMessage(String msg)
	{
		displayMessage = msg;
	}


	public Double getLoanAmount() {
		return loanAmount;
	}


	public Integer getTenor() {
		return tenor;
	}
	
	public boolean allowNewSiblings()
	{
		Calendar oneYearAgo = GregorianCalendar.getInstance();
		oneYearAgo.roll(Calendar.YEAR, false);
		switch (currentState) 
		{
			case APPLICATION_APPROVED:
			case APPLICATION_CANCEL:
				return true;
			case APPLICATION_DENIED:
				return oneYearAgo.after(dateCreated);
			case APPLICATION_BLOCK:
			default:
				return false;
		}
	}
	
	@PrePersist
	void copyMyKeyForIndexing()
	{
		if(keyCopy == null)
			keyCopy = key;
	}
}