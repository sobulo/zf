package com.zerofinance.server;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.zerofinance.server.entities.ProductDetails;
import com.zerofinance.server.entities.ProductPricing;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.TableMessage;

public class CartSessionHelper {
	private final static String CART_SESSION = "zeromecart";
	public final static int QTY_IDX = 0;
	public final static int MTH_IDX = 1;
	
	public static HashMap<Key<ProductDetails>, int[]> getCart(HttpSession sess)
	{
		HashMap<Key<ProductDetails>, int[]> cart = (HashMap<Key<ProductDetails>, int[]>) sess.getAttribute(CART_SESSION);
		if(cart == null)
			cart = new HashMap<Key<ProductDetails>, int[]>();
		return cart;
	}
	
	private static void saveCart(HashMap<Key<ProductDetails>, int[]> cart, HttpSession sess)
	{
		sess.setAttribute(CART_SESSION, cart);
	}
	
	public static void emptyCart(HttpSession sess)
	{
		sess.removeAttribute(CART_SESSION);
	}
	
	public static void addToCart(String productID, int numOfItems, int numOfmonths, HttpSession sess, boolean isUpdate)
	{
		Key<ProductDetails> pdk = ObjectifyService.factory().stringToKey(productID);
		HashMap<Key<ProductDetails>, int[]> cart = getCart(sess);
		int[] itemStats = cart.get(pdk);
		if(itemStats == null)
		{
			itemStats = new int[2];
			itemStats[QTY_IDX] = 0;
		}
		itemStats[MTH_IDX] = numOfmonths;
		if(isUpdate)
			itemStats[QTY_IDX] += numOfItems;
		else
			itemStats[QTY_IDX] = numOfItems;
		if(itemStats[QTY_IDX] == 0)
			cart.remove(pdk);
		else
			cart.put(pdk, itemStats);
		saveCart(cart, sess);
	}
	
	public static void fetchCustomerCart(HttpSession sess, List<TableMessage> result)
	{
		HashMap<Key<ProductDetails>, int[]> cart = getCart(sess);
		if(cart == null)
			return;

		Map<Key<Object>, Object> detailAndPricingMap = getCartObjects(cart);
		
		for(Key<ProductDetails> pdk : cart.keySet())
		{
			Key<ProductPricing> ppk = ProductPricing.getKey(pdk);
			ProductDetails details = (ProductDetails) detailAndPricingMap.get(pdk);
			ProductPricing pricing = (ProductPricing) detailAndPricingMap.get(ppk);
			TableMessage itemRow = new TableMessage(2, 4, 0);
			itemRow.setMessageId(pdk.getString());
			itemRow.setText(DTOConstants.CART_PRODUCT_ID, details.getModelNumber() + " (" + details.getManufacturer() + ")");
			itemRow.setText(DTOConstants.CART_PRODUCT_TITLE, details.getTitle());
			itemRow.setNumber(DTOConstants.CART_PRODUCT_REPAY, cart.get(pdk)[MTH_IDX]);
			itemRow.setNumber(DTOConstants.CART_PRODUCT_QTY, cart.get(pdk)[QTY_IDX]);
			int numMonths = (int) Math.round(itemRow.getNumber(DTOConstants.CART_PRODUCT_REPAY));
			itemRow.setNumber(DTOConstants.CART_PRODUCT_UNIT_PRICE, pricing.getPriceVariations()[numMonths]/numMonths);
			itemRow.setNumber(DTOConstants.CART_PRODUCT_TOTAL_PRICE, itemRow.getNumber(DTOConstants.CART_PRODUCT_UNIT_PRICE) * itemRow.getNumber(DTOConstants.CART_PRODUCT_QTY));
			result.add(itemRow);
		}
	}
	
	public static Map<Key<Object>, Object> getCartObjects(HashMap<Key<ProductDetails>, int[]> cart)
	{
		HashSet<Key<? extends Object>> pdpKeys = new HashSet<Key<? extends Object>>();
		
		pdpKeys.addAll(cart.keySet());
		
		for(Key<ProductDetails> pdk : cart.keySet())
			pdpKeys.add(ProductPricing.getKey(pdk));
		
		Map<Key<Object>, Object> detailAndPricingMap = ObjectifyService.begin().get(pdpKeys);
		return detailAndPricingMap;
	}

}
