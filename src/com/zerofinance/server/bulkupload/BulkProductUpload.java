package com.zerofinance.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.zerofinance.server.GeneralFuncs;
import com.zerofinance.server.entities.ProductDetails;
import com.zerofinance.server.entities.ShopDAO;
import com.zerofinance.shared.NameTokens;

public class BulkProductUpload extends HttpServlet {
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	
	private static final Logger log = Logger.getLogger(BulkProductUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "vendorUploadData";
	private final static String VENDOR_FILE_NAME = "vendorUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			if (upldSessObj != null) {
				// retrive data from session object
				ArrayList<VendorStruct> vendorDataList = (ArrayList<VendorStruct>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				String fileName = (String) req.getSession().getAttribute(
						VENDOR_FILE_NAME);
				try {
					// create product listings
					Objectify ofy = ObjectifyService.begin();
					HashSet<Key<ProductDetails>> uniqueProducts = new HashSet<Key<ProductDetails>>();
					for (VendorStruct elem : vendorDataList) {
						ProductDetails newProduct = ShopDAO.saveProductInfo(null, elem.manufacturer, elem.model, elem.category, elem.description, elem.features,
								elem.getDealerPrice(), elem.getPriceVariations(), elem.title, elem.url);
						if (elem.model.equals("") || elem.manufacturer.equals(""))
							continue; //sanity check, shouldn't occur

						// todo: actual creation of objects in database
						okVendors.append(++createCount).append(": ")
								.append(elem.manufacturer).append("(").append(elem.model).append(")<br/>");
						uniqueProducts.add(newProduct.getKey());
					}
					out.println("created: " + createCount + " product listings. Total valid requests: " +
					vendorDataList.size() + ". Number of unique keys: " + uniqueProducts.size() + "<br/>");
					out.println(okVendors.toString());
				} catch (Exception ex) {
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}

				out.println("<br/><hr/><br/>Created the following vendors<br/>");
				out.println(okVendors.toString());
				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
			} else {
				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				String extensionName = ".PNG";
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
						extensionName = "." + Streams.asString(stream);
					} else {
						log.warning("Bulk VENDOR upload attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.PRODUCT_UPLOAD_ACCESSORS;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<VendorStruct> sessionData = new ArrayList<VendorStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);

						String folderPrefix = "https://sites.google.com/a/fertiletech.com/zero-finance-assets/";

						log.warning("Making getValue Calls");
						for (int i = 1; i < numRows; i++) {
							log.warning("Row: " + (i+1) + " of " + numRows);
							try {
								StringBuilder row = new StringBuilder("<tr>");
								VendorStruct tempVendorData = new VendorStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData,
											extensionName);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}

								if (tempVendorData.isValid) {

									row.append("<td>")
											.append(tempVendorData.model)
											.append("</td>").append("<td>")
											.append(tempVendorData.description)
											.append("</td>").append("<td>")
											.append(tempVendorData.features)
											.append("</td></tr>");
									goodDataHTML += row.toString();
									sessionData.add(tempVendorData);
								} else {
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							String goodHeaderSuffix = "<TH>Model OK?</TH><TH>Description OK?</TH><TH>Features OK?</TH>";
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ goodHeaderSuffix
									+ "</TR>"
									+ goodDataHTML
									+ htmlTableEnd;
						}

						log.warning("Printing output ...");
						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME,
								sessionData);
						req.getSession().setAttribute(VENDOR_FILE_NAME,
								item.getName());
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, VendorStruct tenantData, String ext)
			throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = "\\/\\/|\\|\\|";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			String folderPrefix = "https://sites.google.com/a/fertiletech.com/zero-finance-assets/";
			if (headerName.equals(BulkConstants.MODEL_NO_HEADER)) {
				String url = folderPrefix + val + ext;
				tenantData.url = url;
				tenantData.model = val;
				val = "<a href='" + url + "'><img src='" + url
						+ "'" + " alt='" + val + "'/></a>";
			} else if (headerName.equals(BulkConstants.CATEGORY_HEADER)) {
				String category = val.toUpperCase();
				if (NameTokens.CATEGORIES.contains(category))
					tenantData.category = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedCategories());

			} else if (headerName.equals(BulkConstants.FEATURES_HEADER)) {
				String[] vals = val.split(splitSentinent);
				StringBuilder desc = new StringBuilder("<ul>");
				for (String v : vals) {
					if (v.length() > 0)
						desc.append("<li>").append(v).append("</li>");
				}
				desc.append("</ul>");
				tenantData.features = desc.length() > 9 ? desc.toString() : "";
			} else if (headerName.equals(BulkConstants.TITLE_HEADER)) {
				tenantData.title = val;
			} else if (headerName.equals(BulkConstants.DESCRIPTION_HEADER)) {
				String[] vals = val.split(splitSentinent);
				String desc = "";
				for (String v : vals)
					desc += "<p>" + v + "</p>";
				tenantData.description = desc;
			} else if (headerName.equals(BulkConstants.MANUFACTURER_HEADER)) {
				String manufacturer = val.toUpperCase();
				if (manufacturer.equals("THERMOCOOL"))
					manufacturer = "HAIER THERMOCOOL";
				if (NameTokens.DEALERSHIP.contains(manufacturer))
					tenantData.manufacturer = manufacturer;
				else
					throw new IllegalArgumentException(manufacturer
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedDealers());
			} else if (headerName.equals(BulkConstants.PRICE_DEALER_HEADER))
				tenantData.dealerPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_BUY_HEADER))
				tenantData.buyPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_ONE_HEADER))
				tenantData.oneMonthPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_TWO_HEADER))
				tenantData.twoMonthPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_THREE_HEADER))
				tenantData.threeMonthPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_FOUR_HEADER))
				tenantData.fourMonthPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_FIVE_HEADER))
				tenantData.fiveMonthPrice = getNumber(temp);
			else if (headerName.equals(BulkConstants.PRICE_LEASE_SIX_HEADER))
				tenantData.sixMonthPrice = getNumber(temp);

		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public Double getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return (Double) (val);
	}
	
	private class VendorStruct implements Serializable {
		String category;
		String model;
		String title;
		String features;
		String description;
		String manufacturer;
		String url;
		Double dealerPrice;
		Double buyPrice;
		Double oneMonthPrice;
		Double twoMonthPrice;
		Double threeMonthPrice;
		Double fourMonthPrice;
		Double fiveMonthPrice;
		Double sixMonthPrice;
		boolean isValid = true;

		Double[] getPriceVariations() {
			Double[] result = new Double[7];
			result[0] = buyPrice;
			result[1] = oneMonthPrice;
			result[2] = twoMonthPrice;
			result[3] = threeMonthPrice;
			result[4] = fourMonthPrice;
			result[5] = fiveMonthPrice;
			result[6] = sixMonthPrice;
			return result;
		}

		public Double getDealerPrice() {
			if (dealerPrice != null && dealerPrice > 0)
				return dealerPrice;
			return null;
		}
	}	
}
