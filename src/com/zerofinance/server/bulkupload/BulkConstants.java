package com.zerofinance.server.bulkupload;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;

public class BulkConstants {
    public final static ColumnAccessor[] PRODUCT_UPLOAD_ACCESSORS =
            new ColumnAccessor[15];
    public final static ColumnAccessor[] PRICING_UPLOAD_ACCESSORS =
            new ColumnAccessor[10];    
    public final static ColumnAccessor[] DEAL_UPLOAD_ACCESSORS =
            new ColumnAccessor[3];    
    public final static String MANUFACTURER_HEADER = "MANUFACTURER";
    public final static String MODEL_NO_HEADER = "MODEL";
    public final static String CATEGORY_HEADER = "CATEGORY";
    public final static String COLORS_HEADER = "COLORS";
    public final static String TITLE_HEADER = "TITLE";
    public final static String FEATURES_HEADER = "FEATURES";
    public final static String DESCRIPTION_HEADER = "DESCRIPTION";
    public final static String PRICE_DEALER_HEADER = "DEALER PRICE";
    public final static String PRICE_BUY_HEADER = "BUYER PRICE";    
    public final static String PRICE_LEASE_ONE_HEADER = "1 MONTH";
    public final static String PRICE_LEASE_TWO_HEADER = "2 MONTH";
    public final static String PRICE_LEASE_THREE_HEADER = "3 MONTH";
    public final static String PRICE_LEASE_FOUR_HEADER = "4 MONTH";
    public final static String PRICE_LEASE_FIVE_HEADER = "5 MONTH";
    public final static String PRICE_LEASE_SIX_HEADER = "6 MONTH";    
    public final static String DEAL_ACTION_HEADER = "PRIORITY";



    static{
        PRODUCT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(MANUFACTURER_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(MODEL_NO_HEADER, true);     
        PRODUCT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(CATEGORY_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(TITLE_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(FEATURES_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(DESCRIPTION_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[6] = new NumberColumnAccessor(PRICE_DEALER_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[7] = new NumberColumnAccessor(PRICE_BUY_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[8] = new NumberColumnAccessor(PRICE_LEASE_ONE_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[9] = new NumberColumnAccessor(PRICE_LEASE_TWO_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[10] = new NumberColumnAccessor(PRICE_LEASE_THREE_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[11] = new NumberColumnAccessor(PRICE_LEASE_FOUR_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[12] = new NumberColumnAccessor(PRICE_LEASE_FIVE_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[13] = new NumberColumnAccessor(PRICE_LEASE_SIX_HEADER, true);
        PRODUCT_UPLOAD_ACCESSORS[14] = new ExcelManager.DefaultAccessor(COLORS_HEADER, false);

        PRICING_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(MANUFACTURER_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(MODEL_NO_HEADER, true);     
        PRICING_UPLOAD_ACCESSORS[2] = new NumberColumnAccessor(PRICE_DEALER_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[3] = new NumberColumnAccessor(PRICE_BUY_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[4] = new NumberColumnAccessor(PRICE_LEASE_ONE_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[5] = new NumberColumnAccessor(PRICE_LEASE_TWO_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[6] = new NumberColumnAccessor(PRICE_LEASE_THREE_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[7] = new NumberColumnAccessor(PRICE_LEASE_FOUR_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[8] = new NumberColumnAccessor(PRICE_LEASE_FIVE_HEADER, true);
        PRICING_UPLOAD_ACCESSORS[9] = new NumberColumnAccessor(PRICE_LEASE_SIX_HEADER, true);

        DEAL_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(MANUFACTURER_HEADER, true);
        DEAL_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(MODEL_NO_HEADER, true);     
        DEAL_UPLOAD_ACCESSORS[2] = new NumberColumnAccessor(DEAL_ACTION_HEADER, true);
    }
   
}
