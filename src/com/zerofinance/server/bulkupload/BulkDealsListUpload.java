package com.zerofinance.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.zerofinance.server.GeneralFuncs;
import com.zerofinance.server.entities.ProductDetails;
import com.zerofinance.server.entities.ProductPricing;
import com.zerofinance.server.entities.ShopDAO;
import com.zerofinance.server.entities.loans.ApplicationParameters;
import com.zerofinance.server.entities.loans.EntityDAO;
import com.zerofinance.shared.NameTokens;

public class BulkDealsListUpload extends HttpServlet {
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	
	private static final Logger log = Logger.getLogger(BulkDealsListUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "dealsUploadData";
	private final static String VENDOR_FILE_NAME = "dealsUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			log.warning("Got call for deal list");
			out.println("got call for deal list");
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			log.warning("About to make a cast call");
			out.println("fetching id of requested deal");
			String dealListName = (String) req.getSession().getAttribute(VENDOR_FILE_NAME);
			log.warning("Modifying: " + dealListName);
			out.println("Modifying: " + dealListName);
			if (upldSessObj != null) {
				// retrive data from session object
				ArrayList<DealStruct> vendorDataList = (ArrayList<DealStruct>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				//String fileName = (String) req.getSession().getAttribute(
				//		VENDOR_FILE_NAME);
				try {
					// create product listings
					Objectify ofy = ObjectifyService.begin();
					StringBuilder errors = new StringBuilder();
					StringBuilder ok = new StringBuilder();
					Key<ApplicationParameters> dealKey = ApplicationParameters.getKey(dealListName); 
					ApplicationParameters dl = ofy.get(dealKey);
					log.warning("Found deal list");
					out.println("Found deal list");
					HashMap<String, String> params = dl.getParams();
					if(params == null)
					{
						log.warning("Params is null, creating empty hashmap");
						out.println("Params is null, creating empty hashmap");
						params = new HashMap<String, String>();
					}
					log.warning("No of existing entries: " + params.size());
					out.println("No of existing entries: " + params.size());
					for(DealStruct elem : vendorDataList)
					{
						log.warning("Processing " + createCount + " of " + vendorDataList.size());
						out.println("Processing " + createCount + " of " + vendorDataList.size());
						if(elem.isAdd)
						{
							log.warning("Found add request: " + elem.id);
							out.println("Found add request: " + elem.id);
							if(params.containsKey(elem.id.getString()))
								errors.append("<li>").append(elem.id.getName()).append(" exists in this deal list already," +
										" skipping add request</li>");
							else
							{
								params.put(elem.id.getString(), String.valueOf(elem.ordering));
								ok.append("<li>") .append(elem.id.getName()).append(" ADD ok</li>");
							}
						}
						else
						{
							if(params.containsKey(elem.id.getString()))
							{
								params.remove(elem.id.getString());
								ok.append("<li>").append(elem.id.getName()).append(" REMOVE ok</li>");
							}
							else
								errors.append("<li>").append(elem.id.getName()).
								append(" not in deal list, skipping remove request</li>");							
						}
					}
		
					if(errors.length() == 0)
					{
						dl.setParams(params);
						ofy.put(dl);
						out.println("SAVED DEAL LISTn with " + vendorDataList.size() + " records");
					}	
					else
					{
						out.println("<H3>ERRORS FOUND ABORTING</H3><ul>");
						out.println(errors.toString());
						out.println("</ul>");
						
					}
					out.println("Following were ok: <ul>");
					out.println(ok.toString());
					out.println("</ul>");
				} catch (Exception ex) {
					out.println("Exception Occured");
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
				req.setAttribute(VENDOR_FILE_NAME, null);
			} else {
    				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				String dealListID = null;
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
						dealListID = Streams.asString(stream);
					} else {
						log.warning("Bulk Pricing update attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.DEAL_UPLOAD_ACCESSORS ;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<DealStruct> sessionData = new ArrayList<DealStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);

						log.warning("Making getValue Calls");
						Objectify ofy = ObjectifyService.begin();
						for (int i = 1; i < numRows; i++) {
							log.warning("Row: " + (i+1) + " of " + numRows);
							try {
								StringBuilder row = new StringBuilder("<tr>");
								DealStruct tempVendorData = new DealStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}

								if (tempVendorData.isValid) {
									Key<ProductDetails> pdk = ProductDetails.getKey(tempVendorData.model, tempVendorData.manufacturer);
									if(ofy.find(pdk) == null)
									{
										row.append("<td>Product Not Found</td>");
										tempVendorData.isValid = false;
									}
									else
									{
										tempVendorData.id = pdk;
										row.append("<td>")
												.append(pdk.getName() + " product found")
												.append("</td></tr>");
										goodDataHTML += row.toString();
										sessionData.add(tempVendorData); 
									}
								}
								
								if(!tempVendorData.isValid){
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						htmlHeader += "<TH>Comments</TH>";
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ "</TR>"
									+ goodDataHTML
									+ htmlTableEnd;
						}

						log.warning("Printing output ...");
						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME,
								sessionData);
						req.getSession().setAttribute(VENDOR_FILE_NAME,
								dealListID);
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}


	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, DealStruct tenantData)
			throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = "\\/\\/|\\|\\|";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			String folderPrefix = "https://sites.google.com/a/fertiletech.com/zero-finance-assets/";
			if (headerName.equals(BulkConstants.MODEL_NO_HEADER)) {
				tenantData.model = val;
			}
			else if (headerName.equals(BulkConstants.MANUFACTURER_HEADER)) {
				String manufacturer = val.toUpperCase();
				if (manufacturer.equals("THERMOCOOL"))
					manufacturer = "HAIER THERMOCOOL";
				if (NameTokens.DEALERSHIP.contains(manufacturer))
					tenantData.manufacturer = manufacturer;
				else
					throw new IllegalArgumentException(manufacturer
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedDealers());
			} else if (headerName.equals(BulkConstants.DEAL_ACTION_HEADER))
			{
				tenantData.ordering = getNumber(temp);
				if(tenantData.ordering > 0)
					tenantData.isAdd = true;
				else 
					tenantData.isAdd = false;
			}

		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public long getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return Math.round((Double) (val));
	}
}

class DealStruct implements Serializable {
	transient String model;
	transient String manufacturer;
	Key<ProductDetails> id;
	boolean isAdd = true;
	boolean isValid = true;
	long ordering;
}