package com.zerofinance.shared;

public class DTOConstants {
	public final static String COMPANY_NAME = "ADDOSSER MFB";
	public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";	
	public final static String GCS_LOAN_ID_PARAM = "com.fertiletech.gcsloan";

    public final static String APP_PARAM_ADMINS = "user-admin";
    public final static String APP_PARAM_EDITOR = "user-editor";
    public final static String APP_PARAM_REVIEWER = "user-reviewer";
    public final static String APP_PARAM_MAP_TITLES = "map-titles";	
	public final static String APP_PARAM_SAMSUNG_DEALS = NameTokens.ALT_SAMSUNG;
	public final static String APP_PARAM_CHRISTMAS_DEAS = NameTokens.ALT_DEALS;
	public final static String APP_PARAM_LG_DEALS = NameTokens.ALT_LG;
	public final static String APP_PARAM_THERMO_DEALS = NameTokens.ALT_COOL;
	public final static String APP_PARAM_MAILING_LIST = "deal-mailing-list";
	
	public static final int PRICE_BUY = 0;
	public  static final int PRICE_LEASE1 = 1;
	public  static final int PRICE_LEASE2 = 2;
	public  static final int PRICE_LEASE3 = 3;
	public  static final int PRICE_LEASE4 = 4;
	public final int PRICE_LEASE5 = 5;
	public final int PRICE_LEASE6 = 6;
	
    public final static int PRODUCT_MANUFACTURER = 0;
    public final static int PRODUCT_MODEL = 1;
    public final static int PRODUCT_TITLE =  2;
    public final static int PRODUCT_CLASSIFICATION = 3;    
    public final static int PRODUCT_DESCRIPTION = 4;
    public final static int PRODUCT_FEATURES = 5;
    public final static int PRODUCT_IMAGE_URL = 6;
    
    public final static int CART_PRODUCT_ID = 0;
    public final static int CART_PRODUCT_TITLE = 1;
    public final static int CART_PRODUCT_REPAY = 0;
    public final static int CART_PRODUCT_QTY = 1;
    public final static int CART_PRODUCT_UNIT_PRICE = 2;
    public final static int CART_PRODUCT_TOTAL_PRICE = 3;
    
    public final static int ORDER_ID = 0;
    public final static int ORDER_CUSTOMER_NAME = 1;
    public final static int ORDER_CUSTOMER_EMAIL = 2;
    public final static int ORDER_CUSTOMER_PHONE = 3;
    public final static int ORDER_CUSTOMER_COMPANY = 4;
    public final static int ORDER_STATUS = 5;
    public final static int ORDER_MESSAGE = 6;
    public final static int ORDER_LOAN_ID = 7;
    public final static int ORDER_TOTAL = 0;
    public final static int ORDER_TENOR = 1;
    public final static int ORDER_DATE_CREATE = 0;
    public final static int ORDER_DATE_MODIFIED = 1;
    
    //ID indices
    public final static int LOAN_KEY_IDX = 0;
    public final static int LOAN_STATUS_IDX = 1;
    public final static int LOAN_MESSAGE_IDX = 2;
    
    //TODO this will probably clash with loan key logic above
    public final static int GUARANTOR_ONE_KEY_IDX = 1;
    public final static int GUARANTOR_TW0_KEY_IDX = 2;    
    
    public final static int LOAN_IDX = 0;
    public final static int LOAN_ID_IDX = 1;
    public final static int LOAN_STATE_IDX = 2;
    public final static int LOAN_EDIT_IDX = 3;
	public static final String NPMB_FORM_HEADER = "zf-header-pdf";
	public static final String NPMB_FORM_LOGO = "zf-logo-pdf";

    //fields for displaying sales leads to users
    public final static int PUB_STATUS_IDX = 0;
    public final static int PUB_MSG_IDX = 1;
    public final static int PUB_CRT_IDX = 0;
    public final static int PUB_MOD_IDX = 1;
    public final static int PUB_LOAN_AMT_IDX = 0;
    public final static int PUB_TENOR_IDX = 1;

	//text indices
	public final static int FULL_NAME_IDX = 0;
	public final static int EMAIL_IDX = 1;
	public final static int PHONE_NUMBER_IDX = 2;
	public final static int COMPANY_NAME_IDX = 3;
	public final static int NOTE_IDX = 4;
	public final static int STATUS_IDX = 5;
	
	//number indices
    public final static int LEAD_ID_IDX = 0;
	public final static int SALARY_IDX = 1;
	public final static int LOAN_AMT_IDX = 2;
    
    //date indices
    public final static int DATE_CREATED_IDX = 0;
    public final static int DATE_MODIFIED_IDX = 1;

    /*attach idices?*/
	public final static int CATEGORY_IDX = 0;
	public final static int DETAIL_IDX = 1;
	public final static int LAT_IDX = 0;
	public final static int LNG_IDX = 1;
}
