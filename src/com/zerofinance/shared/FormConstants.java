/**
 * 
 */
package com.zerofinance.shared;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.zerofinance.base.GUIConstants;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class FormConstants {
	//form keys, do not change the values (e.g "sna") as it will affect the database
    public final static String APPLICANT_SURNAME_KEY= ApplicationFormConstants.SURNAME;
    public final static String APPLICANT_OTHERNAME_KEY= ApplicationFormConstants.FIRST_NAME;
    public final static String MARITAL_STATUS_KEY = ApplicationFormConstants.MARITAL_STATUS;
    public final static String PRIMARY_PHONE_KEY = ApplicationFormConstants.TEL_NO;
    public final static String EMAIL_KEY = ApplicationFormConstants.EMAIL;
    public final static String ADDRESS_KEY = ApplicationFormConstants.RESIDENTIAL_ADDRESS;
    public final static String BUS_STOP_KEY = ApplicationFormConstants.ADDRESS;
    public final static String SECONDARY_PHONE_KEY = ApplicationFormConstants.MOBILE_NO;
    public final static String EMPLOYER_KEY = ApplicationFormConstants.EMPLOYER;
    public final static String BRANCH_KEY = ApplicationFormConstants.EMPLOYER_ADDRESS;
    public final static String OFFICE_PHONE_KEY = ApplicationFormConstants.EMPLOYER_TEL;
    public final static String OFFICE_EMAIL_KEY = ApplicationFormConstants.OFFICE_EMAIL;
    public final static String JOB_RANK_KEY = ApplicationFormConstants.POSITION;
    public final static String HIRE_DURATION_KEY = ApplicationFormConstants.EMPLOYER_YEARS;
    public final static String JOB_TITLE_KEY = ApplicationFormConstants.PROFESSION;
    public final static String EMPLOYEE_ID_KEY = ApplicationFormConstants.EMPLOYEE_ID;
    public final static String DEPT_KEY= ApplicationFormConstants.DEPT_NAME;
    public final static String SALARY_KEY= ApplicationFormConstants.MONTHLY_GROSS_PAY;
    public final static String SALARY_DATE_KEY = ApplicationFormConstants.RETIREMENT;
    //public final static String EXISTING_LOAN_KEY = ApplicationFormConstants.; 
    public final static String REQUESTED_LOAN_AMOUNT_KEY = ApplicationFormConstants.LOAN_AMOUNT;
    public final static String TENOR_KEY = ApplicationFormConstants.TENOR;
    //public final static String RESIDENT_KEY = "rsd"; //not used in loan app form
    public final static String DATE_OF_BIRTH_KEY = ApplicationFormConstants.DATE_OF_BIRTH;
    public final static String SEX_KEY = ApplicationFormConstants.GENDER;
    public final static String ANNUAL_SALARY_KEY = ApplicationFormConstants.TOTAL_ANNUAL_PAY;
    public final static String LOAN_PURPOSE_KEY = ApplicationFormConstants.LOAN_PURPOSE;
    public final static String IDENTIFICATION_KEY = ApplicationFormConstants.ID_TYPE;
    public final static String NATIONALITY_KEY = ApplicationFormConstants.NATIONALITY;
    public final static String ORIGIN_STATE_KEY = ApplicationFormConstants.STATE_ORIGIN;
    public final static String LOCAL_GOVT_KEY = ApplicationFormConstants.LGA;
    public final static String MOTHERS_MAIDEN_KEY = ApplicationFormConstants.MOTHER_MAIDEN;
    public final static String EMPLOYMENT_DATE_KEY = ApplicationFormConstants.EMPLOYMENT_DATE;
    public final static String TAX_ID_KEY = ApplicationFormConstants.TAX_ID;
    public final static String CONFIRMATION_DATE_KEY = ApplicationFormConstants.CONFIRM_DATE;    
    public final static String BANK_NAME_KEY = ApplicationFormConstants.BANK_NAME;
    public final static String ACCT_NO_KEY = ApplicationFormConstants.ACCT_NO;
    public final static String ACCT_TYPE_KEY = ApplicationFormConstants.ACCT_TYPE;    
    public final static String KIN_SURNNAME_KEY = ApplicationFormConstants.NEXT_OF_KIN;    
    public final static String KIN_ADDRESS_KEY = ApplicationFormConstants.KIN_ADDRESS;
    public final static String KIN_OTHER_NAME_KEY = ApplicationFormConstants.NEXT_OF_KIN_OTHER;
    public final static String KIN_PHONE_KEY = ApplicationFormConstants.KIN_NO;        
    public final static String KIN_EMAIL_KEY = ApplicationFormConstants.KIN_EMAIL;        
    public final static String KIN_RELATION_KEY = ApplicationFormConstants.RELATIONSHIP;  
    public final static String MONTHLY_REPAY_KEY = "Avg. Monthly"; //calculated field;   
    
    //for server side usage only & not persisted in database
    public final static String LOAN_ID_PARAM = "loan-request";
    public final static String LOAN_INT_ID_PARAM = "loan-request-id";
    //these fields can't be left empty
    public final static HashMap<String, String> nameFieldsMap = new HashMap<String, String>();
    public final static HashMap<String, FormValidators[]> requiredFieldsMap = new HashMap<String, FormValidators[]>();
        
    //these fields must conform to certain rules
    static
    {
        nameFieldsMap.put(APPLICANT_SURNAME_KEY, "Surname");
        nameFieldsMap.put(APPLICANT_OTHERNAME_KEY, "Other Names");
        nameFieldsMap.put(PRIMARY_PHONE_KEY, "Phone Number");
        nameFieldsMap.put(ADDRESS_KEY, "Residential Address");
        nameFieldsMap.put(EMAIL_KEY, "Email");
        nameFieldsMap.put(BUS_STOP_KEY, "Bus stop/Area");
        nameFieldsMap.put(EMPLOYER_KEY, "Employer");
        nameFieldsMap.put(BRANCH_KEY, "Branch/Address");
        nameFieldsMap.put(SALARY_KEY, "Monthly Salary");
        nameFieldsMap.put(ANNUAL_SALARY_KEY, "Annual Salary");
        nameFieldsMap.put(REQUESTED_LOAN_AMOUNT_KEY, "Requested Loan Amount");
        //FormConstants.nameFieldsMap.put(EXISTING_LOAN_KEY, "Total of any Existing Loans");
        nameFieldsMap.put(DATE_OF_BIRTH_KEY, "Date of Birth");
        nameFieldsMap.put(SECONDARY_PHONE_KEY, "Secondary Phone");
        nameFieldsMap.put(OFFICE_PHONE_KEY, "Office Phone");
        nameFieldsMap.put(OFFICE_EMAIL_KEY, "Office Email");        
        
    	FormValidators[] mandatoryOnly = {FormValidators.MANDATORY};
    	FormValidators[] emailOnly = {FormValidators.EMAIL};
    	FormValidators[] phoneOnly = {FormValidators.PHONE};
    	FormValidators[] mandatoryPlusPhone = {FormValidators.MANDATORY, FormValidators.PHONE};
    	FormValidators[] mandatoryPlusEmail = {FormValidators.MANDATORY, FormValidators.EMAIL};
    	FormValidators[] mandatoryPlusParsesOk = {FormValidators.MANDATORY, FormValidators.PARSES};
    	FormValidators[] mandatoryPlusDateParsesOk = {FormValidators.MANDATORY, FormValidators.DATE_OLD_ENOUGH};
    	FormValidators[] mandatoryPlusLoanParsesOk = {FormValidators.MANDATORY, FormValidators.PARSES_LOAN_AMOUNT};
    	
        requiredFieldsMap.put(APPLICANT_SURNAME_KEY, mandatoryOnly);
        requiredFieldsMap.put(APPLICANT_OTHERNAME_KEY, mandatoryOnly);
        requiredFieldsMap.put(PRIMARY_PHONE_KEY, mandatoryPlusPhone);
        requiredFieldsMap.put(EMAIL_KEY, mandatoryPlusEmail);
        requiredFieldsMap.put(ADDRESS_KEY, mandatoryOnly);
        requiredFieldsMap.put(BUS_STOP_KEY, mandatoryOnly);
        requiredFieldsMap.put(EMPLOYER_KEY, mandatoryOnly);
        requiredFieldsMap.put(BRANCH_KEY, mandatoryOnly);
        requiredFieldsMap.put(SALARY_KEY, mandatoryPlusParsesOk);
        requiredFieldsMap.put(ANNUAL_SALARY_KEY, mandatoryPlusParsesOk);
        requiredFieldsMap.put(REQUESTED_LOAN_AMOUNT_KEY, mandatoryPlusLoanParsesOk);
        //requiredFieldsMap.put(EXISTING_LOAN_KEY, mandatoryPlusParsesOk);
        requiredFieldsMap.put(DATE_OF_BIRTH_KEY, mandatoryPlusDateParsesOk);
        requiredFieldsMap.put(SECONDARY_PHONE_KEY, phoneOnly);
        requiredFieldsMap.put(OFFICE_PHONE_KEY, phoneOnly);
        requiredFieldsMap.put(OFFICE_EMAIL_KEY, emailOnly);
    }
    
    public final static HashSet<String> ensureHashSet = new HashSet<String>();
    public static void ensureKeyNotUsed(String val)
    {
    	//TODO create a regression test suite, and fill in the code
    }
    
    
    public final static String REQUIREMENTS1 = "<p style='font-weight:bold; font-size:10px;'>Please note that issuance of dud cheque is punishable under Banks and Other Financial Institution Act (BOFIA 1999)" +" <br />I hereby confirm applying for the above credit facility and certify that all the information provideed by me above and attached thereto is true, correct and complete. I authorise you to make any enquiry you consider necessary and appropriate for " +
    		"the purpose of evaluation this application.</p>"
    		+ "<p style='font-size:12px;'><b>Requirements: </b><br />" +
    		"(1) Execution of Lease Application Form. (2) Copy of staff ID Card. (3) 6 Months Bank Statement. (4) 3 Months Payslips " +
    		"(5) Submission of 1 Duly Completed Guarantors Form with Copy of Staff ID Card and Passport photograph (6) Asset Request Schedule. (7) Lessee's Post dates repayment cheques. (8) Post dates cheque of guarantor if the lease amount is ₦500,000 and above. (9) Pro-forma Invoice addressed to Addosser Microfinanace Bank Limited</p>";

    public final static String REQUIREMENTS2 ="<p style='font-weight:bold;'>The condition of this quarantee are:<br /> " +
    		"<ol>" +
    		"<li>If the said applicant defaults in one lease rental payment to <b>ADDOSSER MICROFINANCE BANK</b> then I would indemnify the company against the actual amount due and interest thereon.</li>" +
    		"<li>In the event that the applicant defaults in one lease rental payment or loses his job, the entire facility shall be called in and the total outstanding interest shall become payable immidiately by me." +
    		"And to this effect, a cheque is hereby deposited with the bank as a source of repayment </li>" +
    		"<li>The guarantor shall not have the right to withdraw from this guarantee.</li>" +
    		"<li>This guarantee shall be a continous one and the liablity of the guarantor will be valid until the principal and interest is fully repaid.</li>" +
    		"</ol>" +
    		"<b>Please note that it is dangerous to quaranty someone not well known to you </b></p>"; 
    
    public final static String COMPANY_LIST[] =  {"ACCESS BANK PLC", "ADDAX", "AIICO INSURANCE PLC", "ALCATEL", "ARMOUR GROUP", 
    												"AIRTEL", "AKINTOLA WILLIAMS DELLOITTE", "BROLL PROPERTY", "BRITISH AMERICAN TOBACCO", 
    												"CADBURY", "CHEVRON NIG.", "CITI BANK", "CONOIL", "COCACOLA NIGERIA", "CUSTODIAN & ALLIED INSURANCE PLC", 
    												"DIAMOND BANK PLC", "ECOBANK PLC", "EMC COMPUTERS SYSTEM", "ETISALAT", "ERNST&YOUNG", "EXXON MOBIL", 
    												"FCMB", "FIDELITY", "FIRST BANK", "FIRST SECURITIES DISCOUNT HOUSE", "GENERAL ELECTRIC", "GLOBACOM", 
    												"GLAXOSMITH KLINE CONSUMER NIGERIA PLC", "GUINNESS NIGERIA", "GTBANK", "HP", "HUAWEI", "IBM", "INTERSWITCH",
    												"INTERNATIONAL TURNKEY SOLUTION", "JANDL OIL", "KPMG", "LASAA", "LAFARGE WAPCO", "LIRS/FIRS E.T.C", 
    												"MANSARD INSURANCE", "MARINA SECURITIES", "MTN", "MOBIL OIL NIGERIA PLC", "MUTUAL BENEFIT ASSUARANCE PLC", "NESTLE", 
    												"NIGERIAN BREWERIES PLC", "NIGERIA BOTTLING COMPANY", "NNPC", "OANDO", "PZ", "PWC", "QUALISERVE TECHNOLOGIES", 
    												"ROYAL EXCHANGE PLC", "SAIPEM", "SAHARA ENERGY", "SAMSUNG NIGERIA", "SKYE BANK PLC", "SHELL PETROLEUM", "STANBIC IBTC", 
    												"STANDARD CHARTERED", "STERLING BANK PLC", "SWIFT NETWORKS", "TOTAL NIGERIA PLC", "UBA PLC", "UACN", "UPDC", "UNION BANK PLC", 
    												"UNITY BANK PLC", "UNILEVER NIG. PLC", "VISAFONE COMMUNICATION", "WAKANOW.COM", "WEMA BANK", "ZENITH BANK", "7UP BOTTLING COMPANY"};
    
    public final static String LOAN_PURPOSE[] = {"Personal", "Commerical"};
    public final static String SEX[] = {"Male", "Female"};
    public final static String MARRIAGE_STATES[] = {"Single", "Married", "Divorced", "Widow(er)"};
    //public final static String MARITAL
    public final static HashMap<String, String> SAMPLE_DATA = new HashMap<String, String>();
    
    public enum HirePeriods implements Serializable
    {
    	LESS_THAN_SIX_MONTHS, SIX_TO_TWELVE_MONTHS, ONE_TO_TWO_YEARS, TWO_TO_FIVE_YEARS, MORE_THAN_FIVE_YEARS;
    	
    	public String toString()
    	{
    		return super.toString().replace('_', ' ');
    	}
    	
    	public static HirePeriods getEnumVal(String str)
    	{
    		return HirePeriods.valueOf(str.replace(' ', '_'));
    	}
    }
    
    private interface Validators
    {
    	boolean validate(String key, String val, HashMap<String, String> messages, boolean isSubmit);
    }
    
    public enum FormValidators implements Validators
    {
    	EMAIL
    	{
			@Override
			public boolean validate(String key, String val,
					HashMap<String, String> messages, boolean isSubmit) 
			{
				if(val != null && val.length() > 0 && !ValidationUtils.isValidEmail(val))
				{
					addError(messages, key, "invalid address");
					return false;
				}
				return true;
			}
    	},
    	PHONE
    	{
			@Override
			public boolean validate(String key, String val,
					HashMap<String, String> messages, boolean isSubmit) {
				if(val != null && val.length() > 0 && !ValidationUtils.isValidNum(val))
				{
					addError(messages, key, "invalid phone number, e.g. use 01-123-4567 or 0801-234-5678");
					return false;
				}
				return true;			}
    	},
    	PARSES
    	{
			@Override
			public boolean validate(String key, String val,
					HashMap<String, String> messages, boolean isSubmit) {
				
				if(val == null || val.length() == 0) 
					return true;
				
				try
				{
					Integer x = Integer.valueOf(val);
				}
				catch(NumberFormatException ex)
				{
					addError(messages, key, "Enter a whole number. No letters or fractions/decimals allowed");
					return false;
				}
				return true;
			}
    	},
    	PARSES_LOAN_AMOUNT
    	{
			@Override
			public boolean validate(String key, String val,
					HashMap<String, String> messages, boolean isSubmit) {
				
				if(val == null || val.length() == 0) 
					return true;
				
				try
				{
					Integer x = Integer.valueOf(val);
					if(x < 100000 || x > 1000000)
						addError(messages, key, "Loan amount must be between 100,000 and 1,000,000 Naira");
				}
				catch(NumberFormatException ex)
				{
					addError(messages, key, "Enter a whole number. No letters or fractions/decimals allowed");
					return false;
				}
				return true;
			}
    	},
    	DATE_OLD_ENOUGH
    	{
			@Override
			public boolean validate(String key, String val,
					HashMap<String, String> messages, boolean isSubmit) 
			{
				if(val == null || val.length() == 0) 
					return true;				
				
				Date d = stringToDate(val);
				if( d == null)
				{
					addError(messages, key, "badly formatted date");
					return false;
				}
				Date twentyYearsAgo = new Date();
				CalendarUtil.addMonthsToDate(twentyYearsAgo, -12 * 21);
				if(d.after(twentyYearsAgo))
				{
					addError(messages, key, "You must be at least 21 years old to apply");
					return false;
				}
				return true;
			}
    	},
    	MANDATORY
    	{

			@Override
			public boolean validate(String key, String val, HashMap<String, String> errorMessage, boolean isSubmit) {
				if(isSubmit && (val == null || val.equals("")))
				{
					addError(errorMessage, key, "Enter a value");
					return false;
				}
				return true;
			}
    	};

    	public HashMap<String, String> mapToBufferHelper = new HashMap<String, String>();
    	public boolean validate(String key, String val, StringBuilder errorMessage, boolean isSubmit)
    	{
    		boolean result = validate(key, val, mapToBufferHelper, isSubmit);
    		if(!result)
    			addError(errorMessage, mapToBufferHelper.get(key));
    		return result;
    	}
    	
    	public void addError(StringBuilder errorBuffer, String msg)
    	{
			errorBuffer.append("<li>").append(msg).append("</li>");
    	}
    	
    	public void addError(HashMap<String, String> errorMap, String key, String msg)
    	{
    		if(errorMap == null)
    			return;
			errorMap.put(key, getKeyDescription(key) + " (" + msg + ")");
    	}
    	
    	private String getKeyDescription(String key)
    	{
    		return nameFieldsMap.get(key);
    	}
    }
    
	public static void setDateParameters(DateBox db) {
		Date d = new Date();
        db.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		db.setValue(null);
	}
	
    public final static int COMPANY_INFO_NAME_IDX = 0;
    public final static int COMPANY_INFO_ADDR_IDX = 1;
    public final static int COMPANY_INFO_NUMS_IDX = 2;
    public final static int COMPANY_INFO_EMAIL_IDX = 3;
    public final static int COMPANY_INFO_WEB_IDX = 4;
    public final static int COMPANY_INFO_ACCR_IDX = 5;
    
    public final static String[] COMPANY_INFO = new String[7]; 
    
	public final static String PDF_REPORT_LOGO = "Company Report Logo";    
	public final static String PDF_NO_IMAGE = "Affix Photo Logo";
	public final static String[] ADMIN_UPLOADS = {PDF_REPORT_LOGO, PDF_NO_IMAGE};
    
    public static String[] getCompanyInfo()
    {
    	COMPANY_INFO[COMPANY_INFO_NAME_IDX] = "Addosser Microfinance Bank";
    	COMPANY_INFO[COMPANY_INFO_ADDR_IDX] = "32 Lewis Street, Lagos Island, Lagos";
    	COMPANY_INFO[COMPANY_INFO_NUMS_IDX] = "0700-AddosserMFB, 01-844-7210, 01-844-7211"; 
    	COMPANY_INFO[COMPANY_INFO_EMAIL_IDX] = "customerservices@addosser.com";
    	COMPANY_INFO[COMPANY_INFO_ACCR_IDX] = "Addosser MFB";
    	COMPANY_INFO[COMPANY_INFO_WEB_IDX] = "http://www.addosser.com";
    	
    	return COMPANY_INFO;
    }	
	public static Date stringToDate(String val)
	{
		DateBox db = new DateBox();
		setDateParameters(db);
		Date d = GUIConstants.DEFAULT_DATEBOX_FORMAT.parse(db, val, false);
		return d;
	}

    
    public enum FormTypes implements Serializable
    {
    	LOAN_FORM, ACKNOWLEDGEMENT_FORM, PRINT_FORM, APPOINTMENT_FORM;
    }
    
    public enum CustomMessageTypes implements Serializable{
       GENERIC_MESSAGE;
   } 
    
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		long subVal = Math.round(result.getNumber(DTOConstants.GUARANTOR_TW0_KEY_IDX));		
		if( subVal == 1)
			guard2Submitted = true;
		else if(subVal == 0)
			guard2Submitted = false;
		else
			guard2Submitted = null;
		
		subVal = Math.round(result.getNumber(DTOConstants.GUARANTOR_ONE_KEY_IDX));
		if( subVal == 1)
			guard1Submitted = true;
		else if(subVal == 0)
			guard1Submitted = false;
		else
			guard1Submitted = null;
		
		subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[3];
		subs[0] = appSubmitted;
		subs[1] = guard1Submitted;
		subs[2] = guard2Submitted;
		return subs;
	}
	
    static 
    {	
    	Arrays.sort(COMPANY_LIST); //sort company list
    	
    	/*SAMPLE_DATA.put(APPLICANT_SURNAME_KEY, "Subulo");
    	SAMPLE_DATA.put(APPLICANT_OTHERNAME_KEY, "Segun Razak");
    	SAMPLE_DATA.put(MARITAL_STATUS_KEY, "Single");
    	SAMPLE_DATA.put(PRIMARY_PHONE_KEY, "+2348038331338");
    	SAMPLE_DATA.put(EMAIL_KEY, "sobulo@fertiletech.com");
    	SAMPLE_DATA.put(ADDRESS_KEY, "3B Oliade Benson, Maryland Ikeja");
    	SAMPLE_DATA.put(BUS_STOP_KEY, "Idioko");
    	SAMPLE_DATA.put(SECONDARY_PHONE_KEY, "+2348084425674");
    	SAMPLE_DATA.put(EMPLOYER_KEY, "FTBS");
    	SAMPLE_DATA.put(BRANCH_KEY, "Maryland Lagos");
    	SAMPLE_DATA.put(OFFICE_PHONE_KEY, "012693281");
    	SAMPLE_DATA.put(OFFICE_EMAIL_KEY, "info@fertiletech.com");
    	SAMPLE_DATA.put(JOB_RANK_KEY, "Executive officer");
    	SAMPLE_DATA.put(HIRE_DURATION_KEY, "3");
    	SAMPLE_DATA.put(JOB_TITLE_KEY, "Programmer");
    	SAMPLE_DATA.put(EMPLOYEE_ID_KEY, "012");
    	//SAMPLE_DATA.put(DEPT_KEY, "Software");
    	SAMPLE_DATA.put(SALARY_KEY, "500,000");
    	SAMPLE_DATA.put(SALARY_DATE_KEY, "24th");
    	SAMPLE_DATA.put(EXISTING_LOAN_KEY, "None");
    	SAMPLE_DATA.put(REQUESTED_LOAN_AMOUNT_KEY, "One Hundred Thousand Naira Only");
    	SAMPLE_DATA.put(TENOR_KEY, "Six Months");
    	SAMPLE_DATA.put(RESIDENT_KEY, "29 Norman Williams Crescent Ikoyi, Lagos");*/
    }	

}
