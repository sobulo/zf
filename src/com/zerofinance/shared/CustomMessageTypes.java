/**
 * 
 */
package com.zerofinance.shared;

import java.io.Serializable;

/**
 * @author Segun Razaq Sobulo
 *
 */
public enum CustomMessageTypes implements Serializable{
     GRADING_MESSAGE, BILLING_MESSAGE, GENERIC_MESSAGE;
}