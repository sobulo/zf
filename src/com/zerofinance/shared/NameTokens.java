package com.zerofinance.shared;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * @author Joshua Godi
 */
public class NameTokens {
	//home page
    public static final String WELCOME = "welcome";
    // Credit Pages
    public static final String APPLY = "apply";
    public static final String PRINT = "print";
    public static final String UPLOAD = "attach";
    public static final String STATUS = "status";
    //category pages
    public static final String AC = "air-conditioning";
    public static final String RFG = "refridgerator";
    public static final String GC = "gas-cooker";
    public static final String HAT = "home-audio-theatre";
    public static final String HAP = "home-appliance";
    public static final String GEN = "generator";
    public static final String TV = "television";
    public static final String WM = "washing-machine";
    
    //brand pages
    public static final String COOL = "Haier-Thermocool";
    public static final String SAMSUNG = "Samsung";
    public static String LG = "lg";

    //other pages
    public static final String DEALS = "great-deals";
    public static final String GUARANTEE = "guarantee";
    public static final String PHONE = "mobile";
    public static final String DETAILS = "details";
    public static final String CART = "cart";
    public static final String MYORDER = "itemized-order";
    public static final String THANKORDER = "order-boooked";
    public static final String ORDER_HISTORY = "order-history";
    public static final String RESPONSIBILITY = "responsibleshopping";

    public final static String ALT_PHONE = "MOBILE";
    public final static String ALT_AC = "AIR CONDITIONING UNITS";
    public final static String ALT_WM = "WASHING MACHINES";
    public final static String ALT_TV = "TV & VIDEO";
    public final static String ALT_GC = "GAS COOKERS";    
    public final static String ALT_RFG = "REFRIGERATORS";
    public final static String ALT_HAP = "HOUSEHOLD APPLIANCES";
    public final static String ALT_GEN = "GENERATORS";
    public final static String ALT_HAT = "HOME AUDIO & THEATER";
    public final static String ALT_SAMSUNG = "SAMSUNG";
    public final static String ALT_LG = "LG";
    public final static String ALT_COOL = "HAIER THERMOCOOL";
    public final static String ALT_DEALS = "SALES DEALS";
    public final static String ALT_CALCULATOR = "RESPONSIBLE SHOPPING";
    public final static String ALT_GUARANTEE = "100% GUARANTEE";
    
    public final static HashSet<String> DEALERSHIP = new HashSet<String>();
    public final static HashSet<String> CATEGORIES = new LinkedHashSet<String>();    
    // Getters for UiBinders
    static{
        DEALERSHIP.add(NameTokens.ALT_COOL);
        DEALERSHIP.add(NameTokens.ALT_SAMSUNG);
        DEALERSHIP.add(NameTokens.ALT_LG);        
        CATEGORIES.add(NameTokens.ALT_TV);
        CATEGORIES.add(NameTokens.ALT_HAT);
        CATEGORIES.add(NameTokens.ALT_AC);
        CATEGORIES.add(NameTokens.ALT_RFG);
        CATEGORIES.add(NameTokens.ALT_GC);
        CATEGORIES.add(NameTokens.ALT_WM);
        CATEGORIES.add(NameTokens.ALT_HAP);
        CATEGORIES.add(NameTokens.ALT_GEN);
        CATEGORIES.add(NameTokens.ALT_PHONE);
        
    }
    
    public final static String getAllowedDealers()
    {
    	String result = "";
    	for(String s : DEALERSHIP)
    		result += s + ", ";
    	return result;	
    }
    
    public final static String getAllowedCategories()
    {
    	String result = "";
    	for(String s : CATEGORIES)
    		result += s + ", ";
    	return result;	
    } 

	static HashMap<String, String> nameTargetMap;
    public final static HashMap<String, String> getNameTargetMap()
    {
    	if(nameTargetMap == null)
    		nameTargetMap = new LinkedHashMap<String, String>();
    	else return nameTargetMap;
    	
    	nameTargetMap.put(ALT_AC, AC);
    	nameTargetMap.put(ALT_GC, GC);
    	nameTargetMap.put(ALT_TV, TV);
    	nameTargetMap.put(ALT_RFG, RFG);
    	nameTargetMap.put(ALT_HAT, HAT);
    	nameTargetMap.put(ALT_HAP, HAP);
    	nameTargetMap.put(ALT_WM, WM);
    	nameTargetMap.put(ALT_GEN, GEN);
    	nameTargetMap.put(ALT_COOL, COOL);
    	nameTargetMap.put(ALT_SAMSUNG, SAMSUNG);
    	nameTargetMap.put(ALT_LG, LG );
    	nameTargetMap.put(ALT_DEALS, DEALS);
    	nameTargetMap.put(ALT_CALCULATOR, RESPONSIBILITY);
    	nameTargetMap.put(ALT_GUARANTEE, GUARANTEE);
    	nameTargetMap.put(ALT_PHONE, PHONE);
    	
    	return nameTargetMap;
    }
    
	public static String getWelcome() {
		return WELCOME;
	}

	public static String getAltAc() {
		return ALT_AC;
	}

	public static String getAltWm() {
		return ALT_WM;
	}



	public static String getAltTv() {
		return ALT_TV;
	}



	public static String getAltGc() {
		return ALT_GC;
	}



	public static String getAltRfg() {
		return ALT_RFG;
	}



	public static String getAltHap() {
		return ALT_HAP;
	}



	public static String getAltGen() {
		return ALT_GEN;
	}



	public static String getAltHat() {
		return ALT_HAT;
	}



	public static String getAltSamsung() {
		return ALT_SAMSUNG;
	}

	public static String getAltLg() {
		return ALT_LG;
	}

	public static String getAltCool() {
		return ALT_COOL;
	}



	public static String getAltDeals() {
		return ALT_DEALS;
	}



	public static String getAltCalculator() {
		return ALT_CALCULATOR;
	}
	
	public static String getAltGuarantee() {
		return ALT_GUARANTEE;
	}	
}
