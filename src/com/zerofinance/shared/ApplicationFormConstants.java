package com.zerofinance.shared;

import java.util.HashMap;

import com.zerofinance.shared.TableMessageHeader.TableMessageContent;


public class ApplicationFormConstants {
	
	//section A fields
	public static String ID = "Portal-ID";
	public static String ID_NUM = "Portal-NO";
	public static String ID_STATE = "ZF-State";
	public static String PURCHASE_ORDER_ID = "Zf-Order";
	public static String LINK_ORDER = "Zf-Link";
	public static String ID_EDIT = "ZF-Write";
	public static String SURNAME = "Surname";
	public static String FIRST_NAME = "Other Names";
	public static String ADDRESS = "Address";
	public static String TEL_NO = "Tel No";
	public static String MOBILE_NO = "Mobile No";
	public static String EMAIL = "Personal Email";
	public static String DATE_OF_BIRTH = "Date of Birth";
	public static String GENDER = "Sex";
	public static String PROFESSION = "Profession";
	public static String EMPLOYER = "Employer Name";
	public static String EMPLOYER_ADDRESS = "Employer Address";
	public static String EMPLOYER_WEBSITE = "Employer Website";
	public static String EMPLOYER_TEL = "Employer Phone";
	public static String POSITION = "Present Position";
	public static String RESIDENTIAL_ADDRESS = "Residential Address";
	public static String RESIDENTIAL_TEL_NO = "Residential Tel No";
	//public static String OFFICE_NO = "Office Tel No";
	public static String OFFICE_EMAIL = "Office Email";
	public static String EMPLOYER_YEARS = "Years of Current Employment";
	public static String RETIREMENT = "Years to Retirement";
	public static String MARITAL_STATUS = "Marital Status";
	public static String NEXT_OF_KIN = "Kin Last Name"; //TODO map
	public static String NEXT_OF_KIN_OTHER = "Kin Other Name";
	public static String RELATIONSHIP = "Kin Relationship";
	public static String KIN_ADDRESS = "Kin Address";
	public static String KIN_NO = "Kin Phone";
	public static String KIN_EMAIL = "Kin Email";
	public static String TOTAL_ANNUAL_PAY = "Total Annual Pay";
	public static String MONTHLY_GROSS_PAY = "Monthly Salary";
	public static String TENOR = "Tenor";
	public static String LOAN_AMOUNT = "Loan Amount";
	public final static String BANK_NAME = "Bank Name";
	public final static String ACCT_NO = "Account No";
	public final static String ACCT_TYPE = "Account Type";
	public final static String LOAN_PURPOSE = "Loan Purpose";
	public final static String EMPLOYEE_ID = "Employee NO./ID";
	public final static String DEPT_NAME = "Department";
	public final static String ID_TYPE = "ID Type";
	public final static String CONFIRM_DATE = "Confirmation Date";
	public final static String EMPLOYMENT_DATE = "Employment Date";
	public final static String MOTHER_MAIDEN = "Mother's Maiden Name";
	public final static String LGA = "Local Govt. Area";
	public final static String NATIONALITY = "Nationality";
	public final static String STATE_ORIGIN = "State of Origin";
	public final static String TAX_ID = "Tax ID";
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "All information provided on this form has been reviewed by me and " +
			"I hereby authorize Addosser Microfinance Bank Limited to verify the information provided on this form " +
			"as to my credit and employment history.";
	
	public static int OBLIGATION_IDX = 0;
	
	public static String[] TABLE_DESCRIPTIONS = {"Dependents", "Other Periodic Income", "Other Income", "Assets", 
												 "Obligations", "Banking", "Security"};
	
	public final static String[] ACCOUNT_TYPE_LIST = {"Savings", "Current"};
	public final static String[] PURCHASE_LIST = {"Personal", "Commercial"};
	public final static String[] GENDER_LIST = {"Male", "Female"};
	public final static String[] ID_TYPE_LIST = {"Staff ID Card"};
	public final static String[] RESIDENCE_TYPES_LIST = {"Owned", "Rented", "Other"};
	public final static String[] EDUCATION_LIST = {"O-Levels", "National Diploma", "Bachelors", "Masters", "Doctorate"};
	public final static String[] MARITAL_STATUS_LIST = {"Married", "Single", "Divorced/Separated", "Widowed"};
	public final static String[] GUARANTEE_TYPE_LIST = {"Guarantee of Employer", "Other Guarantee"};
	public final static String[] FREQUENCY_LIST = {"Annually (1 payment/year)", "Bi-Annual (2 payments/year)", "Quaterly (4 payments/year)", "Bi-Monthly (6 payments/year)", "Monthly (12 payments/year)","Semi-Monthly (24 payments/year)","Bi-weekly (26 payments/year)","Weekly (52 payments/year)"};
	//flex panel headers and widths
	public static String[][] DEPENDENT_TABLE_CFG = {{"Name", "Age", "Relationship"},{"50", "15", "35"}, {"dependent", "dependents"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] PERIODIC_TABLE_CFG = {{"Type", "Amount", "Month"},{"40", "30", "30"},{"",""}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] INCOME_SRC_TABLE_CFG = {{"Source", "Amount/Year"},{"60", "40"}, {"source", "sources"}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] ASSET_TABLE_CFG = {{"Property/Investment", "Estimated Value", "Yearly Income"},{"40", "30", "30"}, {"asset", "assets"}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] OBLIGATION_TABLE_CFG = {{"Name of Bank or Institution", "Principal Amount", "Outstanding Balance", "Monthly Repayment"},{"40", "20", "20", "20"}, {"other loan, debt, or obligation", "other loans, debts, or obligations"}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] BANKING_TABLE_CFG = {{"Bank", "Account No.", "Account Type", "Balance"},{"30", "30", "15", "25"}, {"bank account", "bank accounts"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] SECURITY_TABLE_CFG = {{"Asset Type", "Type of Charge Proposed", "Valuation"},{"35", "35", "30"}, {"loan security", "loan securities"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][][] ALL_CONFIG = {DEPENDENT_TABLE_CFG, PERIODIC_TABLE_CFG, INCOME_SRC_TABLE_CFG, 
											ASSET_TABLE_CFG, OBLIGATION_TABLE_CFG, BANKING_TABLE_CFG, SECURITY_TABLE_CFG};
	
	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> ORDER_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EMPLOYMENT_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_LOAN_VALIDATORS = new HashMap<String, FormValidator[]>();

	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] mandatoryPlusDateParsesOk = {FormValidator.MANDATORY, FormValidator.DATE_OLD_ENOUGH};
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};		
    	
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(TOTAL_ANNUAL_PAY, mandatoryPlusLoanParsesOk);
    	
    	ORDER_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	ORDER_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	ORDER_VALIDATORS.put(SURNAME, mandatoryOnly);
    	ORDER_VALIDATORS.put(RESIDENTIAL_ADDRESS, mandatoryOnly);

    	
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(RESIDENTIAL_ADDRESS, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(DATE_OF_BIRTH, mandatoryPlusDateParsesOk);
    	NPMB_PERSONAL_VALIDATORS.put(MOBILE_NO, phoneOnly);
    	
    	NPMB_EMPLOYMENT_VALIDATORS.put(OFFICE_EMAIL, emailOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(EMPLOYER_TEL, phoneOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(EMPLOYER, mandatoryOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(EMPLOYER_ADDRESS, mandatoryOnly);
    	
    	NPMB_PERSONAL_VALIDATORS.put(KIN_ADDRESS, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(NEXT_OF_KIN, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(KIN_EMAIL, emailOnly);
    	NPMB_PERSONAL_VALIDATORS.put(KIN_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(RELATIONSHIP, mandatoryOnly);
    	
    	NPMB_EMPLOYMENT_VALIDATORS.put(TOTAL_ANNUAL_PAY, mandatoryPlusLoanParsesOk);
    	NPMB_EMPLOYMENT_VALIDATORS.put(MONTHLY_GROSS_PAY, mandatoryPlusLoanParsesOk);  	
    	NPMB_EMPLOYMENT_VALIDATORS.put(BANK_NAME, mandatoryOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(ACCT_NO, mandatoryOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(ACCT_TYPE, mandatoryOnly);
    	   	
    	NPMB_LOAN_VALIDATORS.put(LOAN_AMOUNT, mandatoryPlusLoanParsesOk);
	}
	
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		
		long subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[1];
		subs[0] = appSubmitted;
		return subs;
	}	
	

}
