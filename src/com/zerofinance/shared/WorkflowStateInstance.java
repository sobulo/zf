package com.zerofinance.shared;

import java.util.HashSet;

public enum WorkflowStateInstance {
	
	APPLICATION_STARTED, APPLICATION_PERSONAL, 
	APPLICATION_EMPLOYMENT, APPLICATION_LOAN,
	APPLICATION_CONFIRM, APPLICATION_REVIEW,
	APPLICATION_PAUSED, APPLICATION_BLOCK,
	APPLICATION_CANCEL, APPLICATION_CALL,
	APPLICATION_MEET_REQUIRED, APPLICATION_PENDING,
	APPLICATION_DENIED, APPLICATION_APPROVED;
	
	private static HashSet<WorkflowStateInstance> APPROVED_STATES = new HashSet<WorkflowStateInstance>();
	private static HashSet<WorkflowStateInstance> DENIED_STATES = new HashSet<WorkflowStateInstance>();
	
	static
	{
		APPROVED_STATES.add(APPLICATION_APPROVED);
		DENIED_STATES.add(APPLICATION_DENIED);
	}
	
	public Boolean isApprovedState()
	{
		if(APPROVED_STATES.contains(this))
			return true;
		else if(DENIED_STATES.contains(this))
			return false;
		else
			return null;
	}
	
	public boolean isOpenState()
	{
		switch (this) {
		case APPLICATION_APPROVED:
		case APPLICATION_BLOCK:
		case APPLICATION_PAUSED:
		case APPLICATION_DENIED:
		case APPLICATION_CANCEL:
			return false;
		default:
			return true;
		}
	}

	public String getDisplayString()
	{
		switch (this) {
		case APPLICATION_APPROVED:
			
			return "Approved";
		case APPLICATION_BLOCK:
			return "Blocked/Banned";
		case APPLICATION_CALL:
			return "Call Customer";
		case APPLICATION_CANCEL:
			return "Canceled";
		case APPLICATION_CONFIRM:
			return "Signature Page";
		case APPLICATION_DENIED:
			return "Denied";
		case APPLICATION_EMPLOYMENT:
			return "Employment Page";
		case APPLICATION_LOAN:
			return "Loan Request Page";
		case APPLICATION_MEET_REQUIRED:
			return "Meeting Required";
		case APPLICATION_PAUSED:
			return "Paused/Suspended";
		case APPLICATION_PENDING:
			return "Awaiting Addosser MFB";
		case APPLICATION_PERSONAL:
			return "Personal Page";
		case APPLICATION_REVIEW:
			return "Under Addosser MFB Review";
		case APPLICATION_STARTED:
			return "Start Page";
		}
		return "Error - Contact IT";
	}
	
	public String getChartColor()
	{
		switch (this) {
		case APPLICATION_APPROVED:
			return "color: green";
		case APPLICATION_BLOCK:
			return "color: black"; 
		case APPLICATION_CALL:
			return "color: magenta";
		case APPLICATION_CANCEL:
			return "color: grey";
		case APPLICATION_CONFIRM:
			return "color: #e072df";
		case APPLICATION_DENIED:
			return "color: red";
		case APPLICATION_EMPLOYMENT:
			return "color: brown";
		case APPLICATION_LOAN:
			return "color: pink";
		case APPLICATION_MEET_REQUIRED:
			return "color: magenta";
		case APPLICATION_PAUSED:
			return "color: yellow";
		case APPLICATION_PENDING:
			return "color: purple";
		case APPLICATION_PERSONAL:
			return "color: orange";
		case APPLICATION_REVIEW:
			return "color: purple";
		case APPLICATION_STARTED:
			return "color: yellow";
		}
		return "color: black";
	}
	
}
