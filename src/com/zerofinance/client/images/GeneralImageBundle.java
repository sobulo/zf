/**
 * 
 */
package com.zerofinance.client.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{

	//home page images
	@Source("logo.png")
	ImageResource logoSmall();
	
	ImageResource bnnr1();
	
	ImageResource bnnr2();
	
	ImageResource bnnr3();	
	
	ImageResource xmas();
	
	ImageResource shopnow();
	
	ImageResource tvs();
	
	ImageResource aces();
	
	ImageResource wash();
	
	@Source("addoser.png")
	ImageResource addosser();

	ImageResource bestfont();	
	
	ImageResource guarantee();
	
	ImageResource respoms();
	
	//listing headers
	ImageResource ac_header();
	
	ImageResource wm_header();
	
	ImageResource gc_header();	
	
	ImageResource tv_header();
	
	ImageResource rfg_header();
	
	ImageResource hat_header(); 
	
	ImageResource hap_header();
	
	ImageResource gen_header(); 
	
	ImageResource checkout();
	
	ImageResource easylife();
	
	ImageResource servingyou();
	
	ImageResource samsung();
	
	ImageResource cool();
	
	ImageResource xmas_header();
	
	ImageResource ohistory();
	
	ImageResource ostatus();
	
	ImageResource thankyou();
	
	ImageResource xmasbackground();
	
	ImageResource lgbanner();
	
	ImageResource lg_header();
	
	ImageResource mobile_header();
}
