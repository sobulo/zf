package com.zerofinance.client.apply;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TabContent;
import org.gwtbootstrap3.client.ui.TabListItem;
import org.gwtbootstrap3.client.ui.TabPane;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.ZeroGoddess;
import com.zerofinance.base.ZeroGoddess.MyOrderMessageSetter;
import com.zerofinance.client.apply.widgs.BSControlValidator;
import com.zerofinance.client.apply.widgs.BSDatePicker;
import com.zerofinance.client.apply.widgs.BSListBox;
import com.zerofinance.client.apply.widgs.BSLoanFieldValidator;
import com.zerofinance.client.apply.widgs.BSTextWidget;
import com.zerofinance.client.apply.widgs.CurrencyBox;
import com.zerofinance.client.apply.widgs.FlexPanel;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.ApplicationFormConstants;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.FormConstants.HirePeriods;
import com.zerofinance.shared.FormValidator;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.WorkflowStateInstance;
import com.zerofinance.shared.oauth.ClientUtils;

public class MortgageApplicationFormPanel extends Composite implements MyOrderMessageSetter{

	@UiField
	TabListItem personalHeader;
	@UiField
	TabListItem employmentHeader;
	@UiField
	TabListItem signatureHeader;
	@UiField
	TabListItem loanHeader;
	@UiField
	TabPane personalBody;
	@UiField
	TabPane employmentBody;
	@UiField
	TabPane loanBody;
	@UiField
	TabPane signatureBody;	
	@UiField
	FlexPanel obligations;	
	@UiField
	BSTextWidget<String> surname;
	@UiField
	BSTextWidget<String> firstName;
	@UiField
	BSTextWidget<String> employerAddress;
	@UiField
	BSTextWidget<String> residentialAddress;
	@UiField
	BSTextWidget<String> mailingAddress;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> alternateNo;	
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<Date> dob;
	@UiField
	BSTextWidget<Date> confirmationDate;	
	@UiField
	BSTextWidget<Date> employmentDate;		
	@UiField
	BSTextWidget<String> gender;
	@UiField
	BSTextWidget<String> maidenName;
	@UiField
	BSTextWidget<String> taxID;
	@UiField
	BSTextWidget<String> nationality;
	@UiField
	BSTextWidget<String> originState;
	@UiField
	BSTextWidget<String> lga;	
	@UiField
	BSTextWidget<String> identificationType;
	@UiField
	BSTextWidget<String> profession;
	@UiField
	BSTextWidget<String> employer;
	@UiField
	BSTextWidget<String> position;
	@UiField
	BSTextWidget<String> employmentYears;
	@UiField
	BSTextWidget<String> employerNo;
	@UiField
	BSTextWidget<String> employerEmail;
	@UiField
	BSTextWidget<String> retirementYears;
	@UiField
	BSTextWidget<String> maritalStatus;
	@UiField
	BSTextWidget<String> totalAnnualPay;
	@UiField
	BSTextWidget<String> monthlyGrossPay;
	@UiField
	BSTextWidget<String> loanAmount;
	@UiField
	BSTextWidget<String> tenor;
	@UiField
	BSTextWidget<String> bankName;
	@UiField
	BSTextWidget<String> acctType;
	@UiField
	BSTextWidget<Integer> acctNo;
	@UiField
	Paragraph declarationContent;
	@UiField
	Small declarationAuthor;
	@UiField
	Button personalSave;
	@UiField
	Button employmentSave;
	@UiField
	Button loanSave;
	@UiField
	Button submit;
	@UiField
	BSTextWidget<String> employeeID;
	@UiField
	BSTextWidget<String> purchaseType;
	@UiField
	BSTextWidget<String> kinName;
	@UiField
	BSTextWidget<String> kinOtherName;
	@UiField
	BSTextWidget<String> kinRelationship;
	@UiField
	BSTextWidget<String> kinAddress;
	@UiField
	BSTextWidget<String> kinEmail;
	@UiField
	BSTextWidget<String> kinPhone;
	@UiField
	Button orderView;
	String loanId = null;
	@UiField
	BSTextWidget<String> department;
	
	HashMap<Button, WorkflowStateInstance> buttonToStateMap = new HashMap<Button, WorkflowStateInstance>();
    // Create an asynchronous callback to handle the result.
	String savedOrderId = "";
    final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(final HashMap<String, String> result) {
        	final String setEmail = email.getValue();
        	try
        	{
        		setFormData(result); 
        	}
        	catch(Exception e)
        	{
        		StringBuilder errorMessage = new StringBuilder("<b>Warning, a few fields might not have " +
        				"been loaded: " + e.getMessage() + "<b><hr><div style='font-size:smaller'><ul>");
        		for(String key :result.keySet())
        			errorMessage.append("<li>").append(result.get(key)).append("</li>");
        		errorMessage.append("</ul></div>");
				CustomerAppHelper.showErrorMessage(errorMessage.toString());
				
        	}
        	CustomerAppHelper.LOAN_MKT_SERVICE.getSupplementaryData(result.get(ApplicationFormConstants.ID), new AsyncCallback<ArrayList<HashMap<String,String>>[]>() {

				@Override
				public void onFailure(Throwable caught) {
					fetchFormDataCallback.onFailure(caught);
				}

				@Override
				public void onSuccess(ArrayList<HashMap<String, String>>[] tables) {
					try
					{
					loanId = result.get(ApplicationFormConstants.ID);
					boolean editable = Boolean.valueOf(result.get(ApplicationFormConstants.ID_EDIT));
					setFormTables(tables);
					WorkflowStateInstance state = WorkflowStateInstance.valueOf(result.get(ApplicationFormConstants.ID_STATE));
					enableTabs(state);
					activatePanel(state);
					//override editable, if an email set (assumption is this email set only if NOT ops/staff)
					if(editable)
						if(setEmail.trim().length() > 0 && !setEmail.equalsIgnoreCase(result.get(ApplicationFormConstants.EMAIL)))
						{
							CustomerAppHelper.showInfoMessage("This app was created from a different email address, setting to read only mode. Login with " + setEmail +
									" and/or logout to edit the application form");
							editable = false;
						}
					enableEditing(editable);
					}
					catch(Exception e)
					{
						Window.alert("Warning, some table rows might not have been loaded: " + e.getMessage());
					}
				}
			});
        	
			ZeroGoddess.setMyOrderMessage(result.get(ApplicationFormConstants.EMAIL), MortgageApplicationFormPanel.this);
        }

        @Override
        public void onFailure(Throwable caught) {
        	CustomerAppHelper.showErrorMessage("Unable to retrieve application form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        	enableButtons(false);
        }
    }; 
    
	@Override
	public void setOrderRepMessage(TableMessage m) {
		tenor.setValue(String.valueOf(Math.round(m.getNumber(DTOConstants.ORDER_TENOR))));
		((CurrencyBox)loanAmount.getFieldWidget()).setAmount(m.getNumber(DTOConstants.ORDER_TOTAL));
		orderView.setText(VIEW_ORDER);
		savedOrderId = m.getMessageId();
		orderView.setEnabled(true);
		loanAmount.setTitle(loanAmount.getLabel());
		tenor.setTitle(tenor.getLabel());
	}
	
	@Override
	public void handleOrderIdRequestFailure(Throwable caught) {
		if(caught instanceof IllegalArgumentException)
			CustomerAppHelper.showInfoMessage(caught.getMessage());
		else
			CustomerAppHelper.showErrorMessage("Unable to fetch any recent orders/purchases. Click shop now button to place an order. " + caught.getMessage());
		orderView.setText(PLACE_ORDER);
		savedOrderId = "";
		orderView.setEnabled(true);
		loanAmount.setTitle("Click Shop Now button to place an order. You will be returned to this screen upon completion of purchase");
		tenor.setTitle("Click Shop Now button to place an order. You will be returned to this screen upon placing your order");
	}    

	private AsyncCallback<String[]> submitCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	CustomerAppHelper.showErrorMessage("Unable to submit application, please try again. Error was: " + caught.getMessage());
        	enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
        	CustomerAppHelper.showInfoMessage("Your application form has been submitted. If you need to make edits to the submitted application, please contact support");
        	boolean editable = Boolean.valueOf(result[DTOConstants.LOAN_EDIT_IDX]);
        	enableEditing(editable);
        	nextPanel(WorkflowStateInstance.APPLICATION_CONFIRM);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};

	private AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	CustomerAppHelper.showErrorMessage("Unable to save values. Error was: " + caught.getMessage());
        	enableButtons(true);			
		}

		@Override
		public void onSuccess(String[] result) {
        	CustomerAppHelper.showInfoMessage("Saved successfully. You may continue editing your application");
        	enableButtons(true);
        	WorkflowStateInstance state = WorkflowStateInstance.valueOf(result[DTOConstants.LOAN_STATE_IDX]);
        	nextPanel(state);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};
	
	private static MortgageApplicationFormPanelUiBinder uiBinder = GWT
			.create(MortgageApplicationFormPanelUiBinder.class);

	interface MortgageApplicationFormPanelUiBinder extends
			UiBinder<Widget, MortgageApplicationFormPanel> {
	}

	final String PLACE_ORDER = "Shop " + GUIConstants.NAIRA_UNICODE + "ow";
	final String VIEW_ORDER = "View Order";	
	public MortgageApplicationFormPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		buttonToStateMap.put(personalSave, WorkflowStateInstance.APPLICATION_PERSONAL);
		buttonToStateMap.put(loanSave, WorkflowStateInstance.APPLICATION_LOAN);
		buttonToStateMap.put(submit, WorkflowStateInstance.APPLICATION_CONFIRM);
		buttonToStateMap.put(employmentSave, WorkflowStateInstance.APPLICATION_EMPLOYMENT);
		FormUtils.setupFlexPanel(obligations, ApplicationFormConstants.OBLIGATION_TABLE_CFG);
		BSListBox tempBox = (BSListBox) employmentYears.getFieldWidget();
		for(HirePeriods h : HirePeriods.values())
			tempBox.addItem(h.toString());
		FormUtils.setupListBox(retirementYears, 1, 31, "", "");
		FormUtils.setupListBox(maritalStatus, ApplicationFormConstants.MARITAL_STATUS_LIST);
		FormUtils.setupListBox(gender, ApplicationFormConstants.GENDER_LIST);
		FormUtils.setupListBox(identificationType, ApplicationFormConstants.ID_TYPE_LIST);
		FormUtils.setupListBox(tenor, 1, 6, "Month", "Months");
		FormUtils.setupListBox(purchaseType, ApplicationFormConstants.PURCHASE_LIST);
		FormUtils.setupListBox(acctType, ApplicationFormConstants.ACCOUNT_TYPE_LIST);
		
		
		ValueChangeHandler<String> declarationHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				setDeclarationAuthor(getFormData());
				
			}
		};
		surname.addValueChangeHandler(declarationHandler);
		firstName.addValueChangeHandler(declarationHandler);
		declarationContent.setText(ApplicationFormConstants.DECLARATION);
		setupDateBox(dob, 40);
		setupDateBox(confirmationDate, 4);
		setupDateBox(employmentDate, 5);
		//setup click handlers
		ClickHandler saveHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				submitAction((Button) event.getSource());
			}
		};
		
		ClickHandler submitHandler = new ClickHandler() {
			
			@Override
			public void onClick(final ClickEvent event) {
				final Button clickSource = (Button) event.getSource();
				submitAction(clickSource);
			}
		};
		
		setupValidators();
		personalSave.addClickHandler(saveHandler);
		employmentSave.addClickHandler(saveHandler);
		loanSave.addClickHandler(saveHandler);
		loanSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(orderView.getText().equals(PLACE_ORDER))
					CustomerAppHelper.showErrorMessage("You must book an order prior to requesting financing. Click Shop Now to purchase items, " +
							" upon checking out you will be provided a link to return to your application");
			}
		});
		submit.addClickHandler(submitHandler);
		orderView.setEnabled(false);

		orderView.addClickHandler(new ClickHandler() {
			PopupPanel p = new PopupPanel(true);
			{
				Grid g = new Grid(3, 3);
				HashMap<String, String > map = NameTokens.getNameTargetMap();
				HashMap<String, IconType> icons = CustomerAppHelper.getCategoryIconMap();
				int row, col;
				row = 0; col = 0;
				for(String cat : NameTokens.CATEGORIES)
				{
					Anchor icon = new Anchor();
					icon.setText(cat);
					icon.setTargetHistoryToken(map.get(cat));
					icon.setIcon(icons.get(cat));
					g.setWidget(row, col, icon);
					col++;
					if(col % 3 == 0)
					{
						row++;
						col = 0;
					}
				}
				p.add(g);
			}
			@Override
			public void onClick(ClickEvent event) {
				if(orderView.getText().equals(VIEW_ORDER))
					History.newItem(NameTokens.MYORDER + "/" + savedOrderId);
				else
				{
					p.showRelativeTo(orderView);
				}
			}
		});
	}
	
	public void setupDateBox(BSTextWidget<Date> datePicker, int years)
	{
		BSDatePicker dobBox = (BSDatePicker) datePicker.getFieldWidget();
		dobBox.setStartView(DateTimePickerView.DECADE);
		dobBox.setMinView(DateTimePickerView.MONTH);
		dobBox.setFormat("yyyy M d");
		dobBox.setValue(null);
		Date startDate = new Date();
		CalendarUtil.addDaysToDate(startDate, -(365 * years));
		CalendarUtil.setToFirstDayOfMonth(startDate);
		CalendarUtil.resetTime(startDate);
		dobBox.setStartDate(startDate);
		dobBox.setAutoClose(true);
		dobBox.reload();

	}
	
	public void enableTabs(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setEnabled(true);
		case APPLICATION_LOAN:
			loanHeader.setEnabled(true);
		case APPLICATION_EMPLOYMENT:
			employmentHeader.setEnabled(true);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setEnabled(true);
		}
	}
	
	public void activatePanel(WorkflowStateInstance state)
	{
		deactivatePanels();
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setActive(true);
			signatureBody.setActive(true);
			break;
		case APPLICATION_LOAN:
			loanHeader.setActive(true);
			loanBody.setActive(true);
			break;
		case APPLICATION_EMPLOYMENT:
			employmentHeader.setActive(true);
			employmentBody.setActive(true);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setActive(true);
			personalBody.setActive(true);
		}		
	}
	
	public void deactivatePanels()
	{
		signatureHeader.setActive(false);
		signatureBody.setActive(false);
		loanHeader.setActive(false);
		loanBody.setActive(false);
		employmentHeader.setActive(false);
		employmentBody.setActive(false);
		personalHeader.setActive(false);
		personalBody.setActive(false);
	}

	public void priorPanel(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_CONFIRM:
			switchPanel(signatureHeader, loanHeader, signatureBody, loanBody);
			break;
		case APPLICATION_LOAN:
			switchPanel(loanHeader, employmentHeader, loanBody, employmentBody);
			break;
		case APPLICATION_EMPLOYMENT:
			switchPanel(employmentHeader, personalHeader, employmentBody, personalBody);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}
	
	public void nextPanel(WorkflowStateInstance state)
	{
		boolean allowJump = !ClientUtils.BankUserCookie.getCookie().isOps();
		switch (state) {
		case APPLICATION_CONFIRM:
			if(allowJump) History.newItem(NameTokens.PRINT + "/" + loanId);
			break;
		case APPLICATION_LOAN:
			switchPanelToNext(loanHeader, signatureHeader, loanBody, signatureBody);
			break;
		case APPLICATION_EMPLOYMENT:
			switchPanelToNext(employmentHeader, loanHeader, employmentBody, loanBody);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			switchPanelToNext(personalHeader, employmentHeader, personalBody, employmentBody);
			break;
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:			
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}

	private void switchPanel(TabListItem currentHeader, TabListItem switchedHeader, TabPane currentBody, TabPane switchedBody)
	{
		currentHeader.setActive(false);
		currentBody.setActive(false);
		switchedHeader.setActive(true);
		switchedBody.setActive(true);
	}
	
	
	private void switchPanelToNext(TabListItem currentHeader, TabListItem nextHeader, TabPane currentBody, TabPane nextBody)
	{
		if(!currentHeader.isActive()) //something else is active, and yes this is a hack
			deactivatePanels();
		switchPanel(currentHeader, nextHeader, currentBody, nextBody);
		nextHeader.setEnabled(true);
	}
	
	public void submitAction(Button b) 
	{
		HashMap<String, String> customerInfo = getFormData();
		customerInfo.put(ApplicationFormConstants.ID_STATE, buttonToStateMap.get(b).toString());
		if(customerInfo.get(ApplicationFormConstants.PURCHASE_ORDER_ID) == null && savedOrderId != null && savedOrderId.trim().length() > 0)
		{
			customerInfo.put(ApplicationFormConstants.PURCHASE_ORDER_ID, savedOrderId);
			customerInfo.put(ApplicationFormConstants.LINK_ORDER, "");
		}
		if(validateFields(b))
			submitFormData(customerInfo, b);
	}
	
	public void enableButtons(boolean enabled)
	{
		personalSave.setEnabled(enabled);
		employmentSave.setEnabled(enabled);
		loanSave.setEnabled(enabled);
		submit.setEnabled(enabled);
	}
	
	private void submitFormData(HashMap<String, String> data, Button b)
	{
		GWT.log("Loan ID: " + loanId);
		ArrayList<HashMap<String, String>>[] suppData = getFormTables();
		enableButtons(false); //prevent user from hitting save multiple times while waiting on server
		if(b == submit)
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, true, submitCallback);
		else
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, false, saveCallback);
	}	
	
	private boolean validateFields(Button button)
	{
		errorMap.clear();
		TabContent x;
		
		ArrayList<BSControlValidator> validators;
		if(button == personalSave)
			validators = personalValidators;
		else if(button == employmentSave)
			validators = employmentValidators;
		else if(button == loanSave)
			validators = loanValidators;
		else
		{
			validators = new ArrayList<BSControlValidator>();
			validators.addAll(personalValidators);
			validators.addAll(employmentValidators);
			validators.addAll(loanValidators);
		}
		
		for(BSControlValidator v : validators)
			v.markErrors();
		
		if(errorMap.size() > 0)
		{
			StringBuilder b = new StringBuilder(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found.<ul>");
			for(String val : errorMap.values())
				b.append("<li>").append(val).append("</li>");
			b.append("</ul>");
			CustomerAppHelper.showErrorMessage(b.toString());
		}
		return errorMap.size() == 0;
	}	
	
	ArrayList<BSControlValidator> personalValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> employmentValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> loanValidators = new ArrayList<BSControlValidator>();
	HashMap<String, String> errorMap = new HashMap<String, String>();

	private void setupValidators()
	{
		HashMap<String, BSTextWidget> controlWidgetsMap = new HashMap<String, BSTextWidget>();
    	controlWidgetsMap.put(ApplicationFormConstants.EMAIL, email);
    	controlWidgetsMap.put(ApplicationFormConstants.TEL_NO, phone);
    	controlWidgetsMap.put(ApplicationFormConstants.SURNAME, surname);
    	controlWidgetsMap.put(ApplicationFormConstants.FIRST_NAME, firstName);
    	controlWidgetsMap.put(ApplicationFormConstants.RESIDENTIAL_ADDRESS, residentialAddress);
    	controlWidgetsMap.put(ApplicationFormConstants.DATE_OF_BIRTH, dob);
    	controlWidgetsMap.put(ApplicationFormConstants.MOBILE_NO, alternateNo);
    	
    	controlWidgetsMap.put(ApplicationFormConstants.OFFICE_EMAIL, employerEmail);
    	controlWidgetsMap.put(ApplicationFormConstants.EMPLOYER_TEL, employerNo);    	
    	controlWidgetsMap.put(ApplicationFormConstants.EMPLOYER, employer);
    	controlWidgetsMap.put(ApplicationFormConstants.EMPLOYER_ADDRESS, employerAddress);
    	
    	controlWidgetsMap.put(ApplicationFormConstants.KIN_ADDRESS, kinAddress);
    	controlWidgetsMap.put(ApplicationFormConstants.NEXT_OF_KIN, kinName);
    	controlWidgetsMap.put(ApplicationFormConstants.KIN_EMAIL, kinEmail);
    	controlWidgetsMap.put(ApplicationFormConstants.KIN_NO, kinPhone);
    	controlWidgetsMap.put(ApplicationFormConstants.RELATIONSHIP, kinRelationship);
    	
    	controlWidgetsMap.put(ApplicationFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay);
    	controlWidgetsMap.put(ApplicationFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay);
    	
    	controlWidgetsMap.put(ApplicationFormConstants.ACCT_NO, acctNo);
    	controlWidgetsMap.put(ApplicationFormConstants.ACCT_TYPE, acctNo);
    	controlWidgetsMap.put(ApplicationFormConstants.BANK_NAME, acctNo);
    	
    	controlWidgetsMap.put(ApplicationFormConstants.LOAN_AMOUNT, loanAmount);
    	
    	setupValidator(controlWidgetsMap, ApplicationFormConstants.NPMB_PERSONAL_VALIDATORS, personalValidators);
    	setupValidator(controlWidgetsMap, ApplicationFormConstants.NPMB_EMPLOYMENT_VALIDATORS, employmentValidators);
    	setupValidator(controlWidgetsMap, ApplicationFormConstants.NPMB_LOAN_VALIDATORS, loanValidators);
	}
	
	private void setupValidator(HashMap<String, BSTextWidget> controlWidgetsMap, HashMap<String, FormValidator[]> validationSet, ArrayList<BSControlValidator> targetValidatorList)
	{
		for(String formKey : validationSet.keySet())
		{
			FormValidator[] fieldValidator = validationSet.get(formKey);
			BSTextWidget inputField = controlWidgetsMap.get(formKey);
			for(FormValidator v : fieldValidator)
				if(v.equals(FormValidator.MANDATORY))
					inputField.markRequired();
			BSLoanFieldValidator submitValidator = new BSLoanFieldValidator(fieldValidator,inputField, formKey, errorMap, true);
			targetValidatorList.add(submitValidator);
		}
	}
	
	public ArrayList<HashMap<String, String>>[] getFormTables()
	{
		ArrayList<HashMap<String, String>>[] result = new ArrayList[1];
		result[ApplicationFormConstants.OBLIGATION_IDX] = obligations.getAllRows();
		return result;
	}
	
	public void setFormTables(ArrayList<HashMap<String, String>>[] tables)
	{
		
		if(tables == null)
		{
			GWT.log("Null Table");
			return;
		}
		GWT.log("Not Null Table");
		obligations.initRows(tables[ApplicationFormConstants.OBLIGATION_IDX]);
	}
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(ApplicationFormConstants.SURNAME, surname.getValue());
		result.put(ApplicationFormConstants.FIRST_NAME, firstName.getValue());
		result.put(ApplicationFormConstants.EMPLOYER_ADDRESS, employerAddress.getValue());
		result.put(ApplicationFormConstants.RESIDENTIAL_ADDRESS, residentialAddress.getValue());
		result.put(ApplicationFormConstants.ADDRESS, mailingAddress.getValue());
		result.put(ApplicationFormConstants.TEL_NO, phone.getValue());		
		result.put(ApplicationFormConstants.MOBILE_NO, alternateNo.getValue());
		result.put(ApplicationFormConstants.EMAIL, email.getValue());
		result.put(ApplicationFormConstants.DATE_OF_BIRTH, dob.getDisplayText());
		result.put(ApplicationFormConstants.CONFIRM_DATE, confirmationDate.getDisplayText());
		result.put(ApplicationFormConstants.EMPLOYMENT_DATE, employmentDate.getDisplayText());
		result.put(ApplicationFormConstants.GENDER, gender.getValue());
		result.put(ApplicationFormConstants.ID_TYPE, identificationType.getValue());
		result.put(ApplicationFormConstants.PROFESSION, profession.getValue());
		
		result.put(ApplicationFormConstants.MOTHER_MAIDEN, maidenName.getValue());
		result.put(ApplicationFormConstants.TAX_ID, taxID.getValue());
		result.put(ApplicationFormConstants.NATIONALITY, nationality.getValue());
		result.put(ApplicationFormConstants.LGA, lga.getValue());
		result.put(ApplicationFormConstants.STATE_ORIGIN, originState.getValue());
		
		result.put(ApplicationFormConstants.DEPT_NAME, department.getValue());
		result.put(ApplicationFormConstants.EMPLOYER, employer.getValue());		
		result.put(ApplicationFormConstants.POSITION, position.getValue());
		result.put(ApplicationFormConstants.EMPLOYER_YEARS, employmentYears.getValue());
		result.put(ApplicationFormConstants.OFFICE_EMAIL, employerEmail.getValue());
		result.put(ApplicationFormConstants.RETIREMENT, retirementYears.getValue());				
		result.put(ApplicationFormConstants.MARITAL_STATUS, maritalStatus.getValue());
		result.put(ApplicationFormConstants.EMPLOYER_TEL, employerNo.getValue());
		result.put(ApplicationFormConstants.KIN_ADDRESS, kinAddress.getValue());
		result.put(ApplicationFormConstants.NEXT_OF_KIN, kinName.getValue());
		result.put(ApplicationFormConstants.RELATIONSHIP, kinRelationship.getValue());
		result.put(ApplicationFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay.getValue());
		result.put(ApplicationFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay.getValue());
		result.put(ApplicationFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay.getValue());
		result.put(ApplicationFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay.getValue());
		result.put(ApplicationFormConstants.TENOR, tenor.getValue());
		result.put(ApplicationFormConstants.LOAN_AMOUNT, loanAmount.getValue());
		
		
		result.put(ApplicationFormConstants.LOAN_PURPOSE, purchaseType.getValue());
		result.put(ApplicationFormConstants.EMPLOYEE_ID, employeeID.getValue());
		result.put(ApplicationFormConstants.KIN_EMAIL, kinEmail.getValue());
		result.put(ApplicationFormConstants.KIN_NO, kinPhone.getValue());
		result.put(ApplicationFormConstants.BANK_NAME, bankName.getValue());
		result.put(ApplicationFormConstants.ACCT_TYPE, acctType.getValue());
		String acctNumber = acctNo.getValue() == null? null : String.valueOf(acctNo.getValue()); 
		result.put(ApplicationFormConstants.ACCT_NO, acctNumber);
		result.put(ApplicationFormConstants.NEXT_OF_KIN_OTHER, kinOtherName.getValue());
		
		return result;
	}
	
	public void setFormData(HashMap<String, String> result)
	{
		surname.setValue(result.get(ApplicationFormConstants.SURNAME));
		firstName.setValue(result.get(ApplicationFormConstants.FIRST_NAME));
		employerAddress.setValue(result.get(ApplicationFormConstants.EMPLOYER_ADDRESS));
		residentialAddress.setValue(result.get(ApplicationFormConstants.RESIDENTIAL_ADDRESS));
		mailingAddress.setValue(result.get(ApplicationFormConstants.ADDRESS));
		phone.setValue(result.get(ApplicationFormConstants.TEL_NO));
		alternateNo.setValue(result.get(ApplicationFormConstants.MOBILE_NO));
		email.setValue(result.get(ApplicationFormConstants.EMAIL));
		dob.setDisplayText(result.get(ApplicationFormConstants.DATE_OF_BIRTH));
		confirmationDate.setDisplayText(result.get(ApplicationFormConstants.CONFIRM_DATE));
		employmentDate.setDisplayText(result.get(ApplicationFormConstants.EMPLOYMENT_DATE));
		gender.setValue(result.get(ApplicationFormConstants.GENDER));		
		identificationType.setValue(result.get(ApplicationFormConstants.ID_TYPE));	
		email.setValue(result.get(ApplicationFormConstants.EMAIL));
		profession.setValue(result.get(ApplicationFormConstants.PROFESSION));

		taxID.setValue(result.get(ApplicationFormConstants.TAX_ID));
		maidenName.setValue(result.get(ApplicationFormConstants.MOTHER_MAIDEN));
		originState.setValue(result.get(ApplicationFormConstants.STATE_ORIGIN));
		lga.setValue(result.get(ApplicationFormConstants.LGA));
		nationality.setValue(result.get(ApplicationFormConstants.NATIONALITY));
		
		
		department.setValue(result.get(ApplicationFormConstants.DEPT_NAME));
		employer.setValue(result.get(ApplicationFormConstants.EMPLOYER));		
		position.setValue(result.get(ApplicationFormConstants.POSITION));
		employmentYears.setValue(result.get(ApplicationFormConstants.EMPLOYER_YEARS));
		employerEmail.setValue(result.get(ApplicationFormConstants.OFFICE_EMAIL));
		retirementYears.setValue(result.get(ApplicationFormConstants.RETIREMENT));				
		kinAddress.setValue(result.get(ApplicationFormConstants.KIN_ADDRESS));
		kinName.setValue(result.get(ApplicationFormConstants.NEXT_OF_KIN));
		kinRelationship.setValue(result.get(ApplicationFormConstants.RELATIONSHIP));
		totalAnnualPay.setValue(result.get(ApplicationFormConstants.TOTAL_ANNUAL_PAY));
		monthlyGrossPay.setValue(result.get(ApplicationFormConstants.MONTHLY_GROSS_PAY));
		totalAnnualPay.setValue(result.get(ApplicationFormConstants.TOTAL_ANNUAL_PAY));
		monthlyGrossPay.setValue(result.get(ApplicationFormConstants.MONTHLY_GROSS_PAY));
		//tenor.setValue(result.get(ApplicationFormConstants.TENOR));
		//loanAmount.setValue(result.get(ApplicationFormConstants.LOAN_AMOUNT));
		maritalStatus.setValue(result.get(ApplicationFormConstants.MARITAL_STATUS));
		employerNo.setValue(result.get(ApplicationFormConstants.EMPLOYER_TEL));

		purchaseType.setValue(result.get(ApplicationFormConstants.LOAN_PURPOSE));
		employeeID.setValue(result.get(ApplicationFormConstants.EMPLOYEE_ID));
		kinEmail.setValue(result.get(ApplicationFormConstants.KIN_EMAIL));
		kinPhone.setValue(result.get(ApplicationFormConstants.KIN_NO));
		bankName.setValue(result.get(ApplicationFormConstants.BANK_NAME));
		acctType.setValue(result.get(ApplicationFormConstants.ACCT_TYPE));
		kinOtherName.setValue(result.get(ApplicationFormConstants.NEXT_OF_KIN_OTHER));
		Integer acctNumber = null;
		if(result.get(ApplicationFormConstants.ACCT_NO) != null)
			acctNumber = Integer.valueOf(result.get(ApplicationFormConstants.ACCT_NO));
		acctNo.setValue(acctNumber);						
		setDeclarationAuthor(result);
	}

	public void enableEditing(boolean isEdit)
	{
		boolean enabled = isEdit;
		surname.setEnabled(enabled);
		firstName.setEnabled(enabled);
		employerAddress.setEnabled(enabled);
		residentialAddress.setEnabled(enabled);
		mailingAddress.setEnabled(enabled);
		phone.setEnabled(enabled);		
		email.setEnabled(false);
		dob.setEnabled(enabled);
		gender.setEnabled(enabled);
		
		confirmationDate.setEnabled(enabled);
		employmentDate.setEnabled(enabled);
		taxID.setEnabled(enabled);
		originState.setEnabled(enabled);
		lga.setEnabled(enabled);
		nationality.setEnabled(enabled);
		maidenName.setEnabled(enabled);
		
		identificationType.setEnabled(enabled);
		profession.setEnabled(enabled);
		department.setEnabled(enabled);
		employer.setEnabled(enabled);		
		position.setEnabled(enabled);
		employmentYears.setEnabled(enabled);
		employerEmail.setEnabled(enabled);
		retirementYears.setEnabled(enabled);				
		kinAddress.setEnabled(enabled);
		kinName.setEnabled(enabled);
		kinRelationship.setEnabled(enabled);
		totalAnnualPay.setEnabled(enabled);
		monthlyGrossPay.setEnabled(enabled);
		totalAnnualPay.setEnabled(enabled);
		monthlyGrossPay.setEnabled(enabled);
		tenor.setEnabled(false);
		loanAmount.setEnabled(false);
		obligations.setEnabled(enabled);
		maritalStatus.setEnabled(enabled);
		employerNo.setEnabled(enabled);
		alternateNo.setEnabled(enabled);
		purchaseType.setEnabled(enabled);
		employeeID.setEnabled(enabled);
		kinEmail.setEnabled(enabled);
		kinPhone.setEnabled(enabled);
		bankName.setEnabled(enabled);
		acctType.setEnabled(enabled);
		kinOtherName.setEnabled(enabled);
		acctNo.setEnabled(enabled);						
		enableButtons(enabled);
	}	
	
	public void setDeclarationAuthor(HashMap<String, String> result)
	{
		String[] nameParts = {result.get(ApplicationFormConstants.SURNAME), result.get(ApplicationFormConstants.FIRST_NAME)};
		declarationAuthor.setText(FormUtils.getFullName(nameParts));		
	}
	
	public void clear()
	{
		setFormData(new HashMap<String, String>());
		obligations.clear();
		enableButtons(false);
	}
	
	public void setLoanID(String loanID) 
	{
		clear();
		CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(loanID, fetchFormDataCallback);
	}
}
