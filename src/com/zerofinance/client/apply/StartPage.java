package com.zerofinance.client.apply;

import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Jumbotron;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.client.shop.ArgumentType;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.client.shop.HyperlinkedPanel;
import com.zerofinance.shared.NameTokens;

public class StartPage extends Composite implements HyperlinkedPanel{
	@UiField
	Jumbotron content;
	GetStartedForm miniApplyForm;
	MapBoard opsDisplay;
	private static WelcomePageUiBinder uiBinder = GWT
			.create(WelcomePageUiBinder.class);	
	
	interface WelcomePageUiBinder extends UiBinder<Widget, StartPage> {
	}

	public StartPage() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Hyperlink getLink() {
		return new Hyperlink("Finance Application", NameTokens.RESPONSIBILITY);
	}

	@Override
	public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
		content.clear();
		if(isOps)
		{
			if(opsDisplay == null)
				opsDisplay = new MapBoard();
			opsDisplay.update(CustomerAppHelper.getLoanID(args));
			content.add(opsDisplay);
		}
		else
		{
			if(miniApplyForm == null)
				miniApplyForm = new GetStartedForm();
			miniApplyForm.updateForm(args, isOps, isLoggedIn);
			content.add(miniApplyForm);
		}
		return this;
	}

	@Override
	public ArgumentType getArgumentType() {
		return ArgumentType.LOAN_ARG;
	}
	
}
