package com.zerofinance.client.apply;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PageJumper extends Composite{
	HTML display = new HTML();
	Hyperlink staticJump = new Hyperlink();
	
	public PageJumper()
	{
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(20);
		container.add(display);
		container.add(staticJump);
		initWidget(container);
		
	}
	
	public void setHTML(String title, String message, String linkText, String linkTarget)
	{
		StringBuilder content = new StringBuilder("<H1>" + title + "</H1><HR/><p>").append(message).append("</p>");
		display.setHTML(content.toString());
		staticJump.setText(linkText);
		staticJump.setTargetHistoryToken(linkTarget);
	}

}
