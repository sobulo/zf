package com.zerofinance.client.apply;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.MyAsyncCallback;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.ApplicationFormConstants;

public class MapBoard extends Composite{
	@UiField
	Paragraph name;
	@UiField
	Small email;
	@UiField
	Anchor map;
	
	final String MAP_URL_PREFIX = "http://www.zerofinance.com.ng/addosserops/#doStatusCW/";
	 
	
	
	private static MapBoardUiBinder uiBinder = GWT
			.create(MapBoardUiBinder.class);

	interface MapBoardUiBinder extends UiBinder<Widget, MapBoard> {
	}

	public MapBoard() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	private void emptyBoard()
	{
		name.setText("No application form was recently viewed");
		email.setText("use link below to select an applicant");
		map.setText("Select an application form on M.A.P");
		map.setHref(GUIConstants.MAP_URL_SELECT);		
	}
	
	public void update(final String loanID)
	{
		emptyBoard();
		if(loanID != null)
		{
			MyAsyncCallback<HashMap<String, String>> statusCallBack = new MyAsyncCallback<HashMap<String,String>>() {

				String wrapNull(String val)
				{
					if(val == null) return "";
					return val;
				}
				
				@Override
				public void onFailure(Throwable caught) {
					CustomerAppHelper.showErrorMessage("Failed to load most recently viewed application. " + caught.getMessage());
				}

				@Override
				public void onSuccess(HashMap<String, String> result) {
					name.setText(wrapNull(result.get(ApplicationFormConstants.FIRST_NAME)) + " " 
							+ wrapNull(result.get(ApplicationFormConstants.SURNAME)));
					email.setText(result.get(ApplicationFormConstants.EMAIL) + " Current Stage: " + result.get(ApplicationFormConstants.ID_STATE));
					map.setText("View application " + result.get(ApplicationFormConstants.ID_NUM) + " on M.A.P");
					map.setHref(MAP_URL_PREFIX + loanID);					
				}

				@Override
				protected void callService(AsyncCallback<HashMap<String, String>> cb) {
					super.enableWarning(false);
					CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(loanID, cb);
				}
			};
			
			statusCallBack.go("Refreshing view...");
		}
	}

}
