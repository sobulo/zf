package com.zerofinance.client.shop.panels;

import org.gwtbootstrap3.client.ui.AnchorButton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.base.ZeroGoddess;
import com.zerofinance.base.ZeroGoddess.MyLoanMessageSetter;
import com.zerofinance.base.ZeroGoddess.MyOrderMessageSetter;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;

public class OrderBookedPanel extends Composite{
	
	@UiField AnchorButton applyLink;
	@UiField AnchorButton orderLink;
	
	String orderId;
	private static OrderBookedPanelUiBinder uiBinder = GWT
			.create(OrderBookedPanelUiBinder.class);

	interface OrderBookedPanelUiBinder extends
			UiBinder<Widget, OrderBookedPanel> {
	}

	public OrderBookedPanel(final boolean isLoggedIn, final boolean isOps, final String opsLoanId) {
		initWidget(uiBinder.createAndBindUi(this));
		applyLink.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
			
				if(isLoggedIn)
				{
					if(isOps)
					{
						String arg = (opsLoanId == null? "":opsLoanId);
						History.newItem(NameTokens.APPLY + "/" + arg);
						return;
					}
					ZeroGoddess.seMyLoanMessage(new MyLoanMessageSetter() {
						
						@Override
						public void showSuccessfulSet() {
							CustomerAppHelper.showInfoMessage("Found an existing application. Click button to continue your application");
						}
						
						@Override
						public void setLoanRepMessage(TableMessage m) {
							String id = "";
							if(m != null && m.getMessageId()!=null) id = m.getMessageId();
							History.newItem(NameTokens.APPLY + "/" + id);
						}
						
						@Override
						public void handleLoanIdRequestFailure(Throwable caught) {
							if(caught instanceof IllegalArgumentException)
								CustomerAppHelper.showInfoMessage("You have no application on file. Get started now!");
							else
								CustomerAppHelper.showErrorMessage("Unable to fetch application. " +
									"Click button to start a new application.<br/>");
							History.newItem(NameTokens.APPLY);
						}
					});
				}
				else
				{
					CustomerAppHelper.showInfoMessage("If you've applied before, please login to access you existing application." +
							" Creating a new application can result in delays in processing your order");
					History.newItem(NameTokens.APPLY);
				}
			}
		});
		orderLink.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(orderId == null)
				{
					if(isLoggedIn)
					{
						if(isOps)
							CustomerAppHelper.showErrorMessage("Staff ID detected. Aborting request to fetch customer's recent order id");
						else
						{
							ZeroGoddess.setMyOrderMessage(new MyOrderMessageSetter() {
								
								@Override
								public void setOrderRepMessage(TableMessage m) {
									String id = "";
									if(m != null && m.getMessageId()!=null) id = m.getMessageId();
									History.newItem(NameTokens.MYORDER + "/" + id);
								}
								
								@Override
								public void handleOrderIdRequestFailure(Throwable caught) {
									if(caught instanceof IllegalArgumentException)
										CustomerAppHelper.showInfoMessage("You have no recent orders");
									else
										CustomerAppHelper.showErrorMessage("Unable to fetch recent orders.<br/>" + caught.getMessage());
								}
							});
						}
					}
					else
						CustomerAppHelper.showErrorMessage("Looks like you haven't purchased any items recently. Contact us if this is not the case");
				}
				else
					History.newItem(NameTokens.MYORDER + "/" + orderId);
			}
		});
		 
	}
	
	public void setOrderIDValue(String id)
	{
		orderId = id;
	}
}
