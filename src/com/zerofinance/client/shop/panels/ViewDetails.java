package com.zerofinance.client.shop.panels;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ImageType;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;

public class ViewDetails extends Composite implements ValueChangeHandler<TableMessage>{

	@UiField
	Button leaseNow;
	@UiField
	Button callToOrder;
	@UiField
	Heading price;
	@UiField
	Heading title;
	@UiField
	ListBox quantity;
	@UiField
	Paragraph contentArea;
	@UiField
	SimplePanel imageWrapper;
	@UiField
	SimplePanel headerWrapper;	
	@UiField
	ListBox tenor;
	@UiField
	Image co;
	
	public static HashMap<String, Image> imageCache = new HashMap<String, Image>();
	
	private static ViewDetailsUiBinder uiBinder = GWT
			.create(ViewDetailsUiBinder.class);

	interface ViewDetailsUiBinder extends UiBinder<Widget, ViewDetails> {
	}
	
	TableMessage selectedProduct = null;

	public ViewDetails() {
		initWidget(uiBinder.createAndBindUi(this));
		leaseNow.setText("Lease " + GUIConstants.NAIRA_UNICODE + "ow");
		quantity.addItem("Quantity");
		quantity.addItem("1 unit", "1");
		for(Integer i = 2; i <= 5; i++)
			quantity.addItem(i.toString() + " units", i.toString());
		tenor.addItem("Repayment Period");
		tenor.addItem("1 month", "1");
		for(Integer i = 2; i <= 6; i++)
			tenor.addItem(i + " months", i.toString());
		ChangeHandler priceHandler = new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				updatePrices();
			}
		};
		tenor.addChangeHandler(priceHandler);
		quantity.addChangeHandler(priceHandler);
		co.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.newItem(NameTokens.CART);
			}
		});
		callToOrder.addClickHandler(new ClickHandler() {
			
			PopupPanel p = new PopupPanel(true);
			{
				p.add(new PhoneNumPanel());
			}
			
			@Override
			public void onClick(ClickEvent event) {
				p.showRelativeTo(callToOrder);
			}
		});
	}

	HandlerRegistration leaseNowRegisration = null;
	protected AsyncCallback<String> leaseCallBack = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage("Cart update failed. Error was: " + caught.getMessage());
		}

		@Override
		public void onSuccess(String result) {
			CustomerAppHelper.showInfoMessage(result);
		}
	};
	void updatePrices()
	{
		if(selectedProduct == null) return;
		if(tenor.getSelectedIndex() == 0)
			tenor.setSelectedIndex(tenor.getItemCount() - 1);
		if(quantity.getSelectedIndex() == 0)
			quantity.setSelectedIndex(1);
		final int noMonths = Integer.valueOf(tenor.getValue(tenor.getSelectedIndex()));
		final double noItems = Integer.valueOf(quantity.getValue(quantity.getSelectedIndex()));
		Double productPrice = selectedProduct.getNumber(noMonths) / noMonths;
		Double quantityPrice = productPrice * noItems;
		price.setText("Price: " + noMonths + " monthly payment(s) of " + GUIConstants.NAIRA_UNICODE + GUIConstants.DEFAULT_NUMBER_FORMAT.format(productPrice));
		String leaseMsg = ("Lease @ " + GUIConstants.NAIRA_UNICODE + GUIConstants.DEFAULT_NUMBER_FORMAT.format(quantityPrice) + "/month");
		leaseNow.setText(leaseMsg);
		if(leaseNowRegisration != null)
			leaseNowRegisration.removeHandler();
		leaseNowRegisration = leaseNow.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				CustomerAppHelper.SHOP_SERVICE.addToCart(selectedProduct.getMessageId(), noMonths,(int) noItems, true, leaseCallBack );
			}
		});
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<TableMessage> event) {
		TableMessage m = event.getValue();
		updateView(m);
		History.newItem(NameTokens.DETAILS);
	}
	
	public void displayProduct(String productID)
	{
		if(productID == null || productID.trim().length() == 0)
			return;
		AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to load product information. Try refreshing your browser");
			}

			@Override
			public void onSuccess(TableMessage result) {
				updateView(result);
			}
		};
		CustomerAppHelper.SHOP_SERVICE.getProductInfo(productID, callback );
	}
	
	private void updateView(TableMessage product)
	{
		selectedProduct = product;
		//setup header
		headerWrapper.clear();
		final String category = product.getText(DTOConstants.PRODUCT_CLASSIFICATION);
		Image headerPic = imageCache.get(category);
		if(headerPic == null)
		{
			headerPic = new Image(ProductListing.categoryImageMap.get(category));
			headerPic.setResponsive(true);
			headerPic.setType(ImageType.ROUNDED);
			headerPic.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					String target = NameTokens.getNameTargetMap().get(category);
					History.newItem(target);
				}
			});
			imageCache.put(category, headerPic);
		}
		headerWrapper.add(headerPic);
		//add image
		imageWrapper.clear();
		final String url = product.getText(DTOConstants.PRODUCT_IMAGE_URL);
		Image productPicture = imageCache.get(url);
		if(productPicture == null)
		{
			final Icon loadingIcon = new Icon(IconType.CIRCLE_O_NOTCH);
			loadingIcon.setSize(IconSize.TIMES5);
			loadingIcon.setSpin(true);
			final HorizontalPanel p = new HorizontalPanel();
			p.add(loadingIcon);
			//imageWrapper.add(p);
			final Image pic = new Image(url);
			/*pic.addLoadHandler(new LoadHandler() {
				
				@Override
				public void onLoad(LoadEvent event) {
					p.remove(loadingIcon);
					pic.setResponsive(true);
					pic.setType(ImageType.THUMBNAIL);
					pic.setVisible(true);		
					imageCache.put(url, pic);
				}
			});*/
			pic.setResponsive(true);
			//pic.setType(ImageType.THUMBNAIL);
			//pic.setVisible(false);
			imageWrapper.add(pic);
		}
		else
			imageWrapper.add(productPicture);
		title.setText(product.getText(DTOConstants.PRODUCT_MANUFACTURER) + " " + product.getText(DTOConstants.PRODUCT_MODEL));
		StringBuilder details = new StringBuilder("<hr/><h4>").append(product.getText(DTOConstants.PRODUCT_TITLE)).append("</h4>").
				append("<div class='pull-right'><b>FEATURES</b><br/>").append(product.getText(DTOConstants.PRODUCT_FEATURES)).append("</div><div><b>DESCRIPTION</b><br/>").
				append(product.getText(DTOConstants.PRODUCT_DESCRIPTION)).append("</div>");
		contentArea.setHTML(details.toString());
		updatePrices();		
	}
}
