package com.zerofinance.client.shop.panels;

import java.util.List;

import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Legend;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.client.shop.utils.TablesView;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.oauth.ClientUtils.BankUserCookie;

public class OrderHistory extends Composite{
	Container content = new Container();

	TablesView orders = new TablesView();
	public OrderHistory(boolean isLoggedIn)
	{
		initWidget(content);
		FlowPanel f = new FlowPanel();
		Legend l = new Legend("Please Log In");
		if(!isLoggedIn)
		{
			Paragraph p = new Paragraph("You must be logged in before you can access your order history. " +
					"If you're already logged in, please try refreshing your browser");
			f.add(l);
			f.add(p);
			content.add(f);
			return;
		}
		content.setFluid(true);
		content.clear();
		Image pic = new Image(CustomerAppHelper.generalImages.ohistory());
		pic.setResponsive(true);
		Row header = new Row();
		Column picWrapper = new Column(ColumnSize.XS_12,  pic);
		picWrapper.addStyleName("productListingHeader");
		picWrapper.add(pic);
		header.add(picWrapper);
		content.add(header);
		Row contentRow = new Row();
		Column tableView = new Column(ColumnSize.XS_12, f);
		BankUserCookie uc = BankUserCookie.getCookie();
		String title = "Listed below are orders for " + uc.getUserName() + " (" + uc.getEmail() + ")"; 
		l.setHTML(title);
		f.add(l);
		f.add(orders);
		Hyperlink x = new Hyperlink("You have not yet placed any orders. Click here to check out our latest offers.", NameTokens.DEALS);
		orders.setEmptyWidget(x);
		contentRow.add(tableView);
		content.add(contentRow);
		CustomerAppHelper.SHOP_SERVICE.getOrders(new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to retrieve your order history. " +
						"Try refreshing your browser.<br/> Error was: " + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				boolean isInit = orders.isInitialized();
				orders.showTable(result);
				if(!isInit)
					orders.addColumn(getEditorColumn(), "View Orders");
			}
		});
		
	}
	
	private com.google.gwt.user.cellview.client.Column<TableMessage, String> getEditorColumn()
	{	
		ButtonCell removeAttachment = new ButtonCell();
		
		com.google.gwt.user.cellview.client.Column<TableMessage, String> invCol = new com.google.gwt.user.cellview.client.Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "View " + object.getText(DTOConstants.ORDER_ID);
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {				
				History.newItem(NameTokens.MYORDER + "/" + object.getMessageId());
			}
		});
		return invCol;
	}
	
}
