package com.zerofinance.client.shop.panels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconFlip;
import org.gwtbootstrap3.client.ui.constants.IconPosition;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.client.ui.html.Span;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.googlecode.gwt.charts.client.corechart.BarChartOptions;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.AnimationEasing;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendAlignment;
import com.googlecode.gwt.charts.client.options.TextStyle;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.MyAsyncCallback;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.client.shop.utils.OrderBioForm;
import com.zerofinance.client.shop.utils.TablesView;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.TableMessageHeader;
import com.zerofinance.shared.TableMessageHeader.TableMessageContent;
import com.zerofinance.shared.oauth.ClientUtils.BankUserCookie;

public class LeaseCart extends Composite{

	BarChart chart;
	
	@UiField
	Column payChartSlot;
	
	@UiField
	TablesView payTable;
	
	@UiField
	TablesView cartList;
	
	@UiField
	Button placeOrder;
	
	@UiField
	OrderBioForm bioForm;
	
	final static double CART_MIN = 80000;
	
	public static boolean isInitialized = false;
	private static LeaseCartUiBinder uiBinder = GWT
			.create(LeaseCartUiBinder.class);

	interface LeaseCartUiBinder extends UiBinder<Widget, LeaseCart> {
	}

	static final int START_DATE_OFFSET = 3;
	
	public LeaseCart(boolean isLoggedIn, boolean isOps) {
		initWidget(uiBinder.createAndBindUi(this));
		payTable.setEmptyWidget(new Small("No items selected. Please note that the total of all your monthly payments must exceed " + 
				GUIConstants.NAIRA_UNICODE + GUIConstants.DEFAULT_NUMBER_FORMAT.format(CART_MIN) +
				". To request an exemption, call us to place your order"));
		cartList.setEmptyWidget(new HTML("Shopping cart is empty"));
		initialize(isLoggedIn, isOps);
		Window.addResizeHandler(new ResizeHandler() {
			
			@Override
			public void onResize(ResizeEvent event) {
				if(chart != null)
					chart.onResize();
			}
		});
		placeOrder.setIconFlip(IconFlip.HORIZONTAL);
		placeOrder.setIconPosition(IconPosition.RIGHT);
		placeOrder.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String[] values = bioForm.getValues();
				if(values == null) return;
				double total = getCartTotal();
				if(total < CART_MIN)
				{
					CustomerAppHelper.showErrorMessage("Please add more items to your cart. Total of monthly repayments must exceed " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(CART_MIN));
					CustomerAppHelper.showErrorMessage("Current total is " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(total));
					return;
				}
				String name = values[OrderBioForm.NAME_IDX];
				String email = values[OrderBioForm.EMAIL_IDX];
				String phone = values[OrderBioForm.PHONE_IDX];
				String company = values[OrderBioForm.ADDRESS_IDX];
				AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Error checking out, try refreshing your browser. " + caught.getMessage());
					}

					@Override
					public void onSuccess(List<TableMessage> result) {
						CustomerAppHelper.showInfoMessage("Order placed. A sales rep will contact you shortly");
						String token = NameTokens.THANKORDER + "/" + result.get(1).getMessageId();
						History.newItem(token);
					}
				};
				CustomerAppHelper.SHOP_SERVICE.checkout(name, email, phone, company, callback);
			}
		});
	}

	private void initialize(final boolean loggedIn, final boolean ops) {
		if(CustomerAppHelper.chartInitialized())
		{
			setupChart(loggedIn, ops);
			return;
		}
		ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				setupChart(loggedIn, ops);
			}
		});
	}	
	
	void setupChart(boolean loggedin, boolean ops)
	{
		// Create and attach the chart
		chart = new BarChart();
		//chart.setWidth("100%");
		//chart.setHeight("400px");
		//chart.addStyleName("img-responsive");
		payChartSlot.add(chart);
		isInitialized = true;
		refresh(loggedin, ops);
	}
	
	public void refresh(final boolean isLoggedIn, final boolean isOps)
	{
		if(!isInitialized)
			return;
		GWT.log("running refresh");
		cartList.clear();
		payTable.clear();
		bioForm.updateForm(isOps, isLoggedIn);
		CustomerAppHelper.SHOP_SERVICE.getCart(new AsyncCallback<List<TableMessage>>() {
			
			@Override
			public void onSuccess(List<TableMessage> result) {

				boolean initialized = cartList.isInitialized();
				cartList.showTable(result);
				if(!initialized)
					cartList.addColumn(getEditorColumn(isLoggedIn, isOps), "Modify Order");
				payTable.showTable(getPaymentTable(result));
				CustomerAppHelper.showInfoMessage("Cart Total: " + GUIConstants.DEFAULT_NUMBER_FORMAT.format(getCartTotal()) + " over a period of " + 
													payTable.getTableData().size() + " month(s)");
					
				draw(result, chart);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Bootbox.alert("Cart fetch failed. Try refreshing your browser. <br/>" + caught.getMessage());
			}
		});		
	}
		
	static List<TableMessage> getPaymentTable(List<TableMessage> cartContents)
	{
		TreeMap<Date, Double> monthlyTotals = new TreeMap<Date, Double>();
		Date today = new Date();
		CalendarUtil.addDaysToDate(today, START_DATE_OFFSET);
		for(TableMessage m : cartContents)
		{
			int numOfMonths = (int) Math.round(m.getNumber(DTOConstants.CART_PRODUCT_REPAY));
			int numOfItems = (int) Math.round(m.getNumber(DTOConstants.CART_PRODUCT_QTY)); //ignore
			double unitPrice = (int) Math.round(m.getNumber(DTOConstants.CART_PRODUCT_UNIT_PRICE)); //ignore, below calculated off both ignore fiels
			double totalPrice = (int) Math.round(m.getNumber(DTOConstants.CART_PRODUCT_TOTAL_PRICE));
			GWT.log("Generating table for " + numOfMonths);
			for(int i = 1; i <= numOfMonths; i++)
			{
				Date startDate = CalendarUtil.copyDate(today);
				CalendarUtil.addMonthsToDate(startDate, i);
				Date key = startDate;
				if(!monthlyTotals.containsKey(key))
					monthlyTotals.put(key, 0.0);
				Double mthTotal = monthlyTotals.get(key) + totalPrice;
				monthlyTotals.put(key, mthTotal);
			}
		}
		
		List<TableMessage> result = new ArrayList<TableMessage>(monthlyTotals.size());
		TableMessageHeader header = new TableMessageHeader(2);
		header.setText(0, "Repayment Period", TableMessageContent.DATE);
		header.setText(1, "Repayment Amount", TableMessageContent.NUMBER);
		result.add(header);
		for(Date month : monthlyTotals.keySet())
		{
			TableMessage m = new TableMessage(0, 1, 1);
			m.setDate(0, month);
			m.setNumber(0, monthlyTotals.get(month));
			result.add(m);
		}
		return result;
	}
	
	static void draw(List<TableMessage> cartContents, BarChart chart) {
		// Prepare the data
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Repayments (monthly)");
		int maxMonth = 0;
		for(int i = 0; i < cartContents.size(); i++)
		{
			dataTable.addColumn(ColumnType.NUMBER, cartContents.get(i).getText(DTOConstants.CART_PRODUCT_ID));
			if(maxMonth < cartContents.get(i).getNumber(DTOConstants.CART_PRODUCT_REPAY))
				maxMonth = (int) Math.round(cartContents.get(i).getNumber(DTOConstants.CART_PRODUCT_REPAY));
		}
		dataTable.addRows(maxMonth);
		Date today = new Date();
		CalendarUtil.addDaysToDate(today, START_DATE_OFFSET);
		for(int i = 0; i < maxMonth; i++)
		{
			CalendarUtil.addMonthsToDate(today, 1);
			dataTable.setValue(i, 0, GUIConstants.DEFAULT_DATE_FORMAT.format(today));
		}
		
		for(int i = 0; i < maxMonth; i++)
		{
			for(int j = 0; j < cartContents.size(); j++)
			{
				int numOfPayments = (int) Math.round(cartContents.get(j).getNumber(DTOConstants.CART_PRODUCT_REPAY));
				double amount = 0.0;
				if(i < numOfPayments)
					amount = cartContents.get(j).getNumber(DTOConstants.CART_PRODUCT_TOTAL_PRICE);
				dataTable.setValue(i, j+1, amount);
			}
		}
		// Set options
		BarChartOptions options = BarChartOptions.create();
		options.setBackgroundColor("#f0f0f0");
		options.setFontName("Tahoma");
		options.setTitle("Estimated* Repayment Chart (*actual dates confirmed upon lease approval)");
		TextStyle titleStyle = TextStyle.create();
		titleStyle.setBold(false);
		titleStyle.setColor("orange");
		options.setTitleTextStyle(titleStyle);
		options.setHAxis(HAxis.create("Amount (" + GUIConstants.NAIRA_UNICODE + ")"));
		options.setIsStacked(true);
		Animation animeOptions = Animation.create();
		animeOptions.setDuration(5000);
		animeOptions.setEasing(AnimationEasing.IN_AND_OUT);
		options.setAnimation(animeOptions);
		Legend legendOptions = Legend.create();
		legendOptions.setAligment(LegendAlignment.START);
		//options.setLegend(legendOptions);
		// Draw the chart
		chart.draw(dataTable, options);
	}
	
	
	private double getCartTotal()
	{
		List<TableMessage> cartPayments = payTable.getTableData();
		double total = 0;
		for(TableMessage m : cartPayments)
			total += m.getNumber(0);
		return total;
	}
	
	private int getSelectedValue(ListBox lb)
	{
		String val = lb.getValue(lb.getSelectedIndex());
		return Integer.valueOf(val);
	}
	
	private void setSelectedValue(ListBox lb, Double val)
	{
		String optionVal = String.valueOf(Math.round(val));
		for(int i = 0; i < lb.getItemCount(); i++)
			if(lb.getValue(i).equals(optionVal))
			{
				lb.setSelectedIndex(i);
				break;
			}
	}
	
	private com.google.gwt.user.cellview.client.Column<TableMessage, String> getEditorColumn(final boolean loggedin, final boolean ops)
	{
		final PopupPanel editPanel = new PopupPanel();
		editPanel.setAnimationEnabled(true);
		editPanel.addStyleName("cartPanel");
		editPanel.setGlassEnabled(false);
		editPanel.setAutoHideEnabled(true);
		final ListBox quantityOptions = new ListBox();
		quantityOptions.addItem("Remove Item", "0");
		quantityOptions.addItem("1 item", "1");
		for(int i = 2; i <= 5; i++)
			quantityOptions.addItem(i + " items", String.valueOf(i));
		final ListBox monthOptions = new ListBox();
		monthOptions.addItem("1 month", "1");
		for(int i = 2; i <= 6; i++)
			monthOptions.addItem(i + " months", String.valueOf(i));
		final Anchor productLink = new Anchor();
		final FlexTable layout = new FlexTable();
		layout.setWidget(0, 0, productLink);
		layout.getFlexCellFormatter().setColSpan(0, 0, 2);
		layout.setWidget(1, 0, new Span("Quantity"));
		layout.setWidget(1, 1, quantityOptions);
		layout.setWidget(2, 0, new Span("Repay In"));
		layout.setWidget(2, 1, monthOptions);
		quantityOptions.setWidth("100%");
		monthOptions.setWidth("100%");
		Button cancelButton = new Button("Cancel");
		cancelButton.setIcon(IconType.CHAIN_BROKEN);
		cancelButton.setType(ButtonType.DANGER);
		cancelButton.setBlock(true);
		cancelButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				editPanel.hide();
			}
		});
		layout.setWidget(3, 1, cancelButton);
		editPanel.add(layout);
		
		ButtonCell removeAttachment = new ButtonCell();
		
		com.google.gwt.user.cellview.client.Column<TableMessage, String> invCol = new com.google.gwt.user.cellview.client.Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "View/Edit";
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {				
				final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Item update failed. Try a browser refresh. <br/>" 
									+ caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						refresh(loggedin, ops);
					}

					@Override
					protected void callService(AsyncCallback<String> cb) {
						enableWarning(false);
						CustomerAppHelper.SHOP_SERVICE.addToCart(object.getMessageId(), getSelectedValue(monthOptions),
								getSelectedValue(quantityOptions), false, cb);
						editPanel.hide();
					}
				};
				
				productLink.setText("View " + object.getText(DTOConstants.CART_PRODUCT_ID));
				productLink.setIcon(IconType.EYE);
				productLink.setIconPosition(IconPosition.LEFT);
				productLink.setTargetHistoryToken(NameTokens.DETAILS + "/" + object.getMessageId());
				setSelectedValue(monthOptions, object.getNumber(DTOConstants.CART_PRODUCT_REPAY));
				setSelectedValue(quantityOptions, object.getNumber(DTOConstants.CART_PRODUCT_QTY));
				final Button save = new Button("Update Cart");
				ClickHandler h = new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						save.setEnabled(false);
						editAttachCallback.go("time to do something");
					}
				};
				save.addClickHandler(h);
				save.setIcon(IconType.CHAIN);
				layout.setWidget(3, 0, save);
				editPanel.center();
				
			}
		});
		return invCol;
	}
	
}
