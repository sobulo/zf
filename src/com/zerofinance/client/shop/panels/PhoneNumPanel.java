package com.zerofinance.client.shop.panels;

import org.gwtbootstrap3.client.ui.html.Text;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class PhoneNumPanel extends Composite{

	private static PhoneNumPanelUiBinder uiBinder = GWT
			.create(PhoneNumPanelUiBinder.class);

	interface PhoneNumPanelUiBinder extends UiBinder<Widget, PhoneNumPanel> {
	}

	public PhoneNumPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
