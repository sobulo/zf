package com.zerofinance.client.shop.panels;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.ThumbnailPanel;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ImageType;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.MyAsyncCallback;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;

public class ProductListing extends Composite implements HasValueChangeHandlers<TableMessage>{

	Container content;
	public final static HashMap<String, ImageResource> categoryImageMap = new HashMap<String, ImageResource>();
	public final static HashMap<String, ImageResource> dealersImageMap = new HashMap<String, ImageResource>();
	static
	{
		categoryImageMap.put(NameTokens.ALT_AC, CustomerAppHelper.generalImages.ac_header());
		categoryImageMap.put(NameTokens.ALT_GC, CustomerAppHelper.generalImages.gc_header());
		categoryImageMap.put(NameTokens.ALT_RFG, CustomerAppHelper.generalImages.rfg_header());
		categoryImageMap.put(NameTokens.ALT_HAT, CustomerAppHelper.generalImages.hat_header());
		categoryImageMap.put(NameTokens.ALT_HAP, CustomerAppHelper.generalImages.hap_header());
		categoryImageMap.put(NameTokens.ALT_WM, CustomerAppHelper.generalImages.wm_header());
		categoryImageMap.put(NameTokens.ALT_GEN, CustomerAppHelper.generalImages.gen_header());
		categoryImageMap.put(NameTokens.ALT_TV, CustomerAppHelper.generalImages.tv_header());
		categoryImageMap.put(NameTokens.ALT_PHONE, CustomerAppHelper.generalImages.mobile_header());
		dealersImageMap.put(NameTokens.ALT_COOL, CustomerAppHelper.generalImages.cool());
		dealersImageMap.put(NameTokens.ALT_SAMSUNG, CustomerAppHelper.generalImages.samsung());
		dealersImageMap.put(NameTokens.ALT_DEALS, CustomerAppHelper.generalImages.xmas_header());
		dealersImageMap.put(NameTokens.ALT_LG, CustomerAppHelper.generalImages.lg_header());
		
	}
	
	public ProductListing(final String[] categories, final String manufacturer, final Integer limit)
	{
		content = new Container();
		content.setFluid(true);
		
				
		//fetch listings
		MyAsyncCallback<List<TableMessage>> fetchListing = new MyAsyncCallback<List<TableMessage>>() {
		
			Icon[] icons = new Icon[4];
			{
				for(int i =0; i < icons.length; i++)
					icons[i] = new Icon(IconType.GEAR);
			}
			
			public String getDisplayPrice(TableMessage m)
			{
				String result = "N/A";
				for(int i = m.getNumberOfDoubleFields() - 1; i > 0; i--)
				{
					if(m.getNumber(i) != null)
					{
						result = GUIConstants.NAIRA_HTML + GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(i)/i);
						break;
					}
				}
				return result;
			}

			@Override
			public void onFailure(Throwable caught) {
				String message = "Oops! Unable to fetch product catalogue. Try refereshing your browser.";
				CustomerAppHelper.showErrorMessage(message);
				setIcons(false, IconType.WARNING, message + " " + caught.getMessage());

			}

			void setIcons(boolean spin, IconType type, String title)
			{
				for(Icon icon : icons)
				{
					icon.setSpin(spin);
					icon.setType(type);
					icon.setTitle(title); 
				}				
			}
			@Override
			public void onSuccess(List<TableMessage> result) {
				content.clear();
				//setup header image
				Image pic = null;
				if(manufacturer != null)
					pic = new Image(dealersImageMap.get(manufacturer));
				else
					pic = new Image(categoryImageMap.get(categories[0]));
				pic.setResponsive(true);
				Row header = new Row();
				Column picWrapper = new Column(ColumnSize.XS_12,  pic);
				picWrapper.addStyleName("productListingHeader");
				picWrapper.add(pic);
				header.add(picWrapper);
				content.add(header);

				result.remove(0);//dump data header				
				CustomerAppHelper.showInfoMessage("Found " + result.size() + " items");
				if(result.size() == 0)
				{
					Row row = new Row();
					Column col = new Column(ColumnSize.XS_12,  pic);
					icons[0].setSpin(false);
					icons[0].setType(IconType.BAN);
					icons[0].setTitle("No results found!");
					col.add(icons[0]);
					row.add(col);
					content.add(row);
					return;					
				}
				final AsyncCallback<String> saveCallback = new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Failed to update cart");
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
					}
				};				
				Row currentRow = new Row();
				Column[] cols = new Column[4];
				for(int i = 0; i < cols.length; i++)
				{
					cols[i] = getDisplayColumn();
					currentRow.add(cols[i]);
				}
				LeaseInfoHandler handler = new LeaseInfoHandler();
				for(int i = 0; i < result.size(); i++)
				{
					/*if( i % 4 == 0)
					{
						if(currentRow != null)
							content.add(currentRow);
						currentRow = new Row();
					}*/
					final TableMessage m = result.get(i);
					GWT.log("IS MESSAGE NULL: " + (m==null));
					GWT.log("TXT: " + m.getNumberOfTextFields() + " NUM: " + m.getNumberOfDoubleFields() + " DAT: " +m.getNumberOfDateFields());
					String model = m.getText(DTOConstants.PRODUCT_MODEL);
					String imageUrl = m.getText(DTOConstants.PRODUCT_IMAGE_URL);
					pic = new Image(imageUrl);
					pic.setResponsive(true);
					pic.setType(ImageType.THUMBNAIL);
					ThumbnailPanel listing = new ThumbnailPanel();
					listing.add(pic);
					
					String manufacturer = m.getText(DTOConstants.PRODUCT_MANUFACTURER);
					StringBuilder display = new StringBuilder("<div class='caption'><h4>").
							append(manufacturer).append(" ").append(model).append("</h4>");
					display.append("<p>").append(m.getText(DTOConstants.PRODUCT_TITLE)).append("</p>");
					display.append("<p>").append(getDisplayPrice(m)).append("</p>");
					Paragraph p = new Paragraph(display.toString());
					listing.add(p);
					
					HorizontalPanel buttonPanel = new HorizontalPanel();
					buttonPanel.setSpacing(5);
					Button b = new Button("Lease " + GUIConstants.NAIRA_UNICODE + "ow");
					b.setIcon(IconType.SHOPPING_CART);
					handler.registerMaapping(b, m);

					b.addClickHandler(new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
							CustomerAppHelper.SHOP_SERVICE.addToCart(m.getMessageId(), 6, 1, true, saveCallback);
						}
					});
					b.setType(ButtonType.PRIMARY);
					b.addStyleName("pull-right");
					listing.add(b);
					b = new Button("Info ...");
					b.setIcon(IconType.QUESTION_CIRCLE);
					handler.registerMaapping(b, m);
					b.addClickHandler(handler);
					buttonPanel.add(b);
					listing.add(b);
					
					int colIdx = i%4;
					Column col = cols[colIdx];//getDisplayColumn();
					col.add(listing);
					currentRow.add(col);
				}
				content.add(currentRow);
			}

			@Override
			protected void callService(AsyncCallback<List<TableMessage>> cb) {
				//loading symbol
				Row row = new Row();
				IconSize[] gearSizes = {IconSize.TIMES2, IconSize.TIMES3, IconSize.TIMES4, IconSize.TIMES5};
				for(int i = 0; i < gearSizes.length; i++)
				{
					Column col = getDisplayColumn();
					icons[i].setSpin(true);
					icons[i].setSize(gearSizes[i]);
					col.add(icons[i]);
					row.add(col);					
				}
				content.add(row);
				if(manufacturer != null) //hack?
					CustomerAppHelper.SHOP_SERVICE.getProductsTable(manufacturer, cb);
				else
					CustomerAppHelper.SHOP_SERVICE.getProductsTable(manufacturer, categories, limit, cb);
			}
		};
		initWidget(content);
		fetchListing.go("Loading...");
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TableMessage> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	Column getDisplayColumn()
	{
		Column col = new Column(ColumnSize.MD_3, ColumnSize.SM_6);
		return col;
	}

	class LeaseInfoHandler implements ClickHandler
	{
		HashMap<Button, TableMessage> buttonMessageMap = new HashMap<Button, TableMessage>();
		@Override
		public void onClick(ClickEvent event) {
			Button source = (Button) event.getSource();
			TableMessage m = buttonMessageMap.get(source);
			if(m != null)
				ValueChangeEvent.fire(ProductListing.this, m);
			
		}
		
		void registerMaapping(Button b, TableMessage m)
		{
			buttonMessageMap.put(b, m);
		}
		
	}
}
