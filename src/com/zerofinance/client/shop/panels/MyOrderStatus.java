package com.zerofinance.client.shop.panels;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.html.Small;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.ZeroGoddess;
import com.zerofinance.base.ZeroGoddess.MyLoanMessageSetter;
import com.zerofinance.client.shop.ArgumentType;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.client.shop.utils.OrderBioForm;
import com.zerofinance.client.shop.utils.TablesView;
import com.zerofinance.shared.ApplicationFormConstants;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.TableMessage;

public class MyOrderStatus extends Composite{
	BarChart chart;
	
	@UiField
	Column payChartSlot;
	
	@UiField
	TablesView payTable;
	@UiField
	TablesView cartList;
	@UiField
	OrderBioForm bioForm;
	@UiField
	Label dateCreated;
	@UiField
	Label orderId;
	@UiField
	Small message;
	@UiField
	Button apply;
	@UiField
	Column  headerSlot;
	
	private static OrderStatusUiBinder uiBinder = GWT
			.create(OrderStatusUiBinder.class);

	
	public static boolean isInitialized = false;
	interface OrderStatusUiBinder extends UiBinder<Widget, MyOrderStatus> {
	}

	String loanID = "";
	public MyOrderStatus(boolean isLoggedIn) {
		initWidget(uiBinder.createAndBindUi(this));
		Image headerPic = new Image(CustomerAppHelper.generalImages.ostatus());
		headerPic.addStyleName("img-responsive");
		headerSlot.add(headerPic);
		headerSlot.addStyleName("productListingHeader");
		bioForm.enableMiniApplyForm(false);
		apply.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				apply.setEnabled(false);
				History.newItem(NameTokens.APPLY + "/" + loanID);
			}
		});
		initialize(isLoggedIn);
		Window.addResizeHandler(new ResizeHandler() {
			
			@Override
			public void onResize(ResizeEvent event) {
				if(chart != null)
					chart.onResize();
			}
		});		
	}

	private void initialize(final boolean isLoggedIn) {
		if(CustomerAppHelper.chartInitialized())
		{
			setupChart(isLoggedIn);
			return;
		}
		ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				setupChart(isLoggedIn);
			}
		});
	}
	
	void setupChart(boolean isLoggedIn)
	{
		// Create and attach the chart
		chart = new BarChart();
		//chart.setWidth("100%");
		//chart.setHeight("400px");
		//chart.addStyleName("img-responsive");
		payChartSlot.add(chart);
		isInitialized = true;
		if(savedId != null)
			fetchOrderDetails(savedId, isLoggedIn);
		
	}
	
	String savedId = null;
	
	void getRecentLoanID()
	{
		ZeroGoddess.seMyLoanMessage(new MyLoanMessageSetter() {
			
			@Override
			public void showSuccessfulSet() {
				CustomerAppHelper.showInfoMessage("Application form found. Click button to view/edit your application");
			}
			
			@Override
			public void setLoanRepMessage(TableMessage m) {
				loanID = m.getMessageId();
			}
			
			@Override
			public void handleLoanIdRequestFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to fetch recent application id. " + caught.getMessage());
				loanID = "";
			}
		});
		
	}
	
	public void fetchOrderDetails(final String orderID, final boolean isLoggedIn)
	{
		if(!isInitialized)
			return;
		savedId = orderID;
		GWT.log("running refresh");
		cartList.clear();
		payTable.clear();

		apply.setEnabled(true);
		if(orderID == null || orderID.trim().length() == 0) return;
		Cookies.setCookie(ArgumentType.ORDER_ARG+CustomerAppHelper.TOKEN_COOKIE, null);
		CustomerAppHelper.SHOP_SERVICE.getItemizedOrder(orderID, new AsyncCallback<List<TableMessage>>() {
			
			@Override
			public void onSuccess(List<TableMessage> result) {
				TableMessage summary = result.remove(0);
				updateSummaryInfo(summary, isLoggedIn);
				cartList.showTable(result);
				List<TableMessage> payments = LeaseCart.getPaymentTable(result);
				payTable.showTable(payments);
				LeaseCart.draw(result, chart);
				Cookies.setCookie(ArgumentType.ORDER_ARG+CustomerAppHelper.TOKEN_COOKIE, orderID);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to retrieve order status, try refreshing your browser." +
						"\n<BR/> Error Msg: " + caught.getMessage());
				
			}
		});
	}
	
	public void updateSummaryInfo(TableMessage m, boolean isLoggedIn)
	{
		String loanID = m.getText(DTOConstants.ORDER_LOAN_ID);
		if(loanID == null || loanID.length() == 0)
		{
			if(isLoggedIn)
				getRecentLoanID();
		}
		else
			this.loanID = loanID; 
			
		HashMap<String, String> data = new HashMap<String, String>();
		data.put(ApplicationFormConstants.SURNAME, m.getText(DTOConstants.ORDER_CUSTOMER_NAME));
		data.put(ApplicationFormConstants.FIRST_NAME, "");
		data.put(ApplicationFormConstants.EMAIL, m.getText(DTOConstants.ORDER_CUSTOMER_EMAIL));
		data.put(ApplicationFormConstants.TEL_NO, m.getText(DTOConstants.ORDER_CUSTOMER_PHONE));
		data.put(ApplicationFormConstants.RESIDENTIAL_ADDRESS, m.getText(DTOConstants.ORDER_CUSTOMER_COMPANY));
		bioForm.setFormData(data);
		
		orderId.setHTML("<i style='color:gold'>ID#</i> " + m.getText(DTOConstants.ORDER_ID)); 
		dateCreated.setHTML("<i style='color:gold'>Date</i> " + GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(DTOConstants.ORDER_DATE_CREATE)));
		StringBuilder status = new StringBuilder("<div>");
		status.append("<span style='color:orange;font-weight:bold'>Status: ").append(m.getText(DTOConstants.ORDER_STATUS)).append("</span><br/>");
		String msg = m.getText(DTOConstants.ORDER_MESSAGE);
		if(msg != null && msg.length() > 0)
			status.append(msg);
		status.append("<p style='float:right;font-size:smaller'>").append(GUIConstants.DEFAULT_DATE_FORMAT.
				format(m.getDate(DTOConstants.ORDER_DATE_MODIFIED))).append("</p>");			
		status.append("</div>");
		message.setHTML(status.toString());
		
	}
}
 