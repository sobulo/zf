package com.zerofinance.client.shop;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Image;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.client.shop.utils.MailingListItem;
import com.zerofinance.shared.NameTokens;

public class WelcomePage extends Composite implements HyperlinkedPanel, ClickHandler{

	@UiField
	Image bannr1;
	@UiField
	Image bannr2;
	@UiField
	Image bannr3;
	@UiField
	Image bannr5;	
	@UiField
	Image bannr4;
	
	@UiField
	Image cat1;
	@UiField
	Image cat2;
	@UiField
	Image cat3;
	@UiField
	Image deals;
	@UiField
	Image responsible;
	@UiField
	Image guarantee;
	@UiField
	Anchor subscribeLink;
	
	MailingListItem mailPanel;
	
	private static WelcomePageUiBinder uiBinder = GWT
			.create(WelcomePageUiBinder.class);	
	
	interface WelcomePageUiBinder extends UiBinder<Widget, WelcomePage> {
	}

	public WelcomePage() {
		initWidget(uiBinder.createAndBindUi(this));
		Image[] imagesThatNeedHandlers = {deals, responsible, guarantee, cat1, cat2, cat3, bannr1, bannr2, bannr3, bannr4, bannr5};
		for(Image homePic : imagesThatNeedHandlers)
			homePic.addClickHandler(this);
		subscribeLink.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				mailPanel.show(subscribeLink);
			}
		});
	}
	
	
	@Override
	public Hyperlink getLink() {
		return new Hyperlink("Welcome", NameTokens.WELCOME);
	}

	@Override
	public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
		if(mailPanel == null)
			mailPanel = new MailingListItem(isLoggedIn);
		return this;
	}

	@Override
	public void onClick(ClickEvent event) {
		Image source  = (Image) event.getSource();
		String imageName = source.getAltText();
		String imageTarget = NameTokens.getNameTargetMap().get(imageName);
		History.newItem(imageTarget);	
	}


	@Override
	public ArgumentType getArgumentType() {
		return ArgumentType.ORDER_ARG;
	} 
}
