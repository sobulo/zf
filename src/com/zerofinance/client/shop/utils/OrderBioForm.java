package com.zerofinance.client.shop.utils;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.client.apply.FormUtils;
import com.zerofinance.client.apply.widgs.BSLoanFieldValidator;
import com.zerofinance.client.apply.widgs.BSTextWidget;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.DuplicateEntitiesException;
import com.zerofinance.shared.FormValidator;
import com.zerofinance.shared.ApplicationFormConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.oauth.ClientUtils.BankUserCookie;

public class OrderBioForm extends Composite{
	@UiField
	BSTextWidget<String> name;
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> address;
	
	private static GetStartedFormUiBinder uiBinder = GWT
			.create(GetStartedFormUiBinder.class);

	interface GetStartedFormUiBinder extends UiBinder<Widget, OrderBioForm> {
	}

	
	public OrderBioForm() {
		initWidget(uiBinder.createAndBindUi(this));
		
	}
	
	public static final int EMAIL_IDX = 0;
	public static final int PHONE_IDX = 1;
	public static final int ADDRESS_IDX = 2;
	public static final int NAME_IDX = 3;
	
	public String[] getValues() {
		BSTextWidget<String>[] fields = new BSTextWidget[4];
		fields[EMAIL_IDX] = email;
		fields[PHONE_IDX] = phone;
		fields[ADDRESS_IDX] = address;
		fields[NAME_IDX] = name;
		String[] formFields = {ApplicationFormConstants.EMAIL, ApplicationFormConstants.TEL_NO, ApplicationFormConstants.RESIDENTIAL_ADDRESS, ApplicationFormConstants.SURNAME};
		
		
		
		HashMap<String, String> errorMap = new HashMap<String, String>();
		for(int i = 0; i < formFields.length; i++)
		{
			String formKey = formFields[i];
			FormValidator[] oldStyleValidator = ApplicationFormConstants.ORDER_VALIDATORS.get(formKey);
			BSLoanFieldValidator validator = new BSLoanFieldValidator(oldStyleValidator, fields[i], formKey, errorMap, true);
			validator.markErrors();
		}
		
		if(errorMap.size() > 0)
		{
			CustomerAppHelper.showErrorMessage(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found. See highlighted fields.");
			return null;
		}
		String[] result = new String[4];
		result[EMAIL_IDX] = email.getValue();
		result[PHONE_IDX] = phone.getValue();
		result[ADDRESS_IDX] = address.getValue();
		result[NAME_IDX] = name.getValue();
		return result;
	}			
	
	public void enableMiniApplyForm(boolean enabled)
	{
		email.setEnabled(enabled);
		phone.setEnabled(enabled);
		address.setEnabled(enabled);
		name.setEnabled(enabled);
		
	}
	
	public void setAndLockEmail(String address)
	{
		email.setValue(address);
		email.setEnabled(false);
	}
	
	public void setName(String myName)
	{
		name.setValue(myName);
	}
	
	public void clear()
	{
		enableMiniApplyForm(true);
		setFormData(new HashMap<String, String>());
	}
	
	public void setFormData(HashMap<String, String> data)
	{
		email.setValue(data.get(ApplicationFormConstants.EMAIL));
		phone.setValue(data.get(ApplicationFormConstants.TEL_NO));
		address.setValue(data.get(ApplicationFormConstants.RESIDENTIAL_ADDRESS));
		String[] nameParts = {data.get(ApplicationFormConstants.SURNAME), data.get(ApplicationFormConstants.FIRST_NAME)}; 
		name.setValue(FormUtils.getFullName(nameParts));
	}
	
	public String[] getNameParts(String fullName)
	{
		String[] parts = fullName.split(" ");
		for(int i = 0; i < parts.length; i++)
			parts[i] = parts[i].trim();
		String[] result = new String[3];
		if(parts.length == 1)
			result[0] = parts[0];
		else if( parts.length == 2)
		{
			result[0] = parts[1];
			result[1] = parts[0];
		}
		else if(parts.length > 2)
		{
			result[0] = parts[2];
			result[1] = parts[0];
			result[2] = parts[1];
		}
		return result;
	}
		
	public void updateForm(boolean isOps, boolean isLoggedIn)
	{
		clear();
			
		if(isLoggedIn && !isOps)
		{
			BankUserCookie uc = BankUserCookie.getCookie();
			setAndLockEmail(uc.getEmail());
			name.setValue(uc.getUserName());
		}
	}

}
