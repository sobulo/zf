/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zerofinance.client.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Styles;
import org.gwtbootstrap3.extras.growl.client.ui.Growl;
import org.gwtbootstrap3.extras.growl.client.ui.GrowlHelper;
import org.gwtbootstrap3.extras.growl.client.ui.GrowlOptions;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Geolocation.PositionOptions;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.zerofinance.base.GUIConstants;
import com.zerofinance.base.LoginService;
import com.zerofinance.base.LoginServiceAsync;
import com.zerofinance.base.MortgageService;
import com.zerofinance.base.MortgageServiceAsync;
import com.zerofinance.base.ShopService;
import com.zerofinance.base.ShopServiceAsync;
import com.zerofinance.client.apply.FileIploadPanel;
import com.zerofinance.client.apply.MortgageApplicationFormPanel;
import com.zerofinance.client.apply.MortgageDownloadPanel;
import com.zerofinance.client.apply.MortgageStatus;
import com.zerofinance.client.apply.StartPage;
import com.zerofinance.client.apply.PageJumper;
import com.zerofinance.client.images.GeneralImageBundle;
import com.zerofinance.client.shop.panels.LeaseCart;
import com.zerofinance.client.shop.panels.MyOrderStatus;
import com.zerofinance.client.shop.panels.OrderBookedPanel;
import com.zerofinance.client.shop.panels.OrderHistory;
import com.zerofinance.client.shop.panels.ProductListing;
import com.zerofinance.client.shop.panels.ViewDetails;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.NameTokens;
import com.zerofinance.shared.oauth.ClientUtils;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 * IMPORTANT: only use package access or private access in this class. This is to prevent
 * unnecessary loading of certain gwt files in parts of the app that are public
 * 
 * TODO run gwt code splitting analytic tools to explicitly confirm which classes get loaded by public 
 * sections of the app
 */
public class CustomerAppHelper {
    
	public final static String TOKEN_COOKIE = "ng.com.zerofinance.pubapparg";
    public final static ShopServiceAsync SHOP_SERVICE = GWT.create(ShopService.class);
    public final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);
    public final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    public final static GeneralImageBundle generalImages = GWT.create(GeneralImageBundle.class);
    private final static String APP_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
    final static String APP_PAGE_URL;
	final static int TOKEN_ARGS_PARAM_IDX = 0;
	final static int TOKEN_ARGS_RECENTLOAN_IDX = 1;
	final static int TOKEN_ARGS_EMAIL_IDX = 2;  
	final static int TOKEN_ARGS_OTHER_PARAM_IDX = 3;
    
    static
    {
    	if(!GWT.isProdMode()) //true if dev mode
    		APP_PAGE_URL = APP_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
    	else
    		APP_PAGE_URL = APP_PAGE_URL_PREFIX;
    }

    static ViewDetails detailPanel;
    static HashMap<String, IconType> CATEGORIES = new HashMap<String, IconType>();
    
    public static HashMap<String, IconType> getCategoryIconMap()
    {

    	if(CATEGORIES.size() == 0)
    	{
	        CATEGORIES.put(NameTokens.ALT_TV, IconType.DESKTOP);
	        CATEGORIES.put(NameTokens.ALT_HAT, IconType.VOLUME_UP);
	        CATEGORIES.put(NameTokens.ALT_AC, IconType.CLOUD);
	        CATEGORIES.put(NameTokens.ALT_RFG, IconType.ASTERISK);
	        CATEGORIES.put(NameTokens.ALT_GC, IconType.FIRE);
	        CATEGORIES.put(NameTokens.ALT_WM, IconType.TINT);
	        CATEGORIES.put(NameTokens.ALT_HAP, IconType.HOME);
	        CATEGORIES.put(NameTokens.ALT_GEN, IconType.LIGHTBULB_O);
	        CATEGORIES.put(NameTokens.ALT_PHONE, IconType.ANDROID);
    	}
        return CATEGORIES;
    }
    
    static ViewDetails getDetailPanel()
    {
    	if(detailPanel == null)
    		detailPanel = new ViewDetails();
    	return detailPanel;
    }
    
    public static boolean chartInitialized()
    {
    	return (MyOrderStatus.isInitialized || LeaseCart.isInitialized);
    }
    
    static HyperlinkedPanel shoppingCart = null;
    static HyperlinkedPanel getShoppingCart()
    {
    	if(shoppingCart != null)
    		return shoppingCart;
		shoppingCart = new HyperlinkedPanel() {
			LeaseCart cartPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(cartPanel == null)
					cartPanel = new LeaseCart(isLoggedIn, isOps);
				else
					cartPanel.refresh(isLoggedIn, isOps);
				return cartPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("View Cart", NameTokens.CART);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
    	return shoppingCart;
    }

    static HyperlinkedPanel myOrderStatus = null;
    static HyperlinkedPanel getOrderPanel()
    {
    	if(myOrderStatus != null)
    		return myOrderStatus;
    	
		myOrderStatus = new HyperlinkedPanel() {
			MyOrderStatus orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new MyOrderStatus(isLoggedIn);
				orderPanel.fetchOrderDetails(args[TOKEN_ARGS_PARAM_IDX], isLoggedIn);
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Order Status", NameTokens.MYORDER);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
		
    	return myOrderStatus;
    }

    static HyperlinkedPanel bookpanel = null;
    static HyperlinkedPanel getOrderBookedPanel()
    {
    	if(bookpanel != null)
    		return bookpanel;
    	
		bookpanel = new HyperlinkedPanel() {
			OrderBookedPanel orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new OrderBookedPanel(isLoggedIn, isOps, args[TOKEN_ARGS_RECENTLOAN_IDX]);
				orderPanel.setOrderIDValue(args[TOKEN_ARGS_PARAM_IDX]);
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Thank You", NameTokens.THANKORDER);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
		
    	return bookpanel;
    }
    
    
    static HyperlinkedPanel orderHistory = null;
    static HyperlinkedPanel getOrderHistoryPanel()
    {
    	if(orderHistory != null)
    		return orderHistory;
    	
		orderHistory = new HyperlinkedPanel() {
			OrderHistory orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new OrderHistory(isLoggedIn);
					return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Order History", NameTokens.ORDER_HISTORY);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
    	return orderHistory;
    }       
    
	static List<HyperlinkedPanel> initializePanels() {
        GWT.log("loading static block");
       
        List<HyperlinkedPanel> result = new ArrayList<HyperlinkedPanel>();
        
        HyperlinkedPanel detailLinkPanel = new HyperlinkedPanel() {
        	Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String arg = args[TOKEN_ARGS_PARAM_IDX];
				ViewDetails panel = getDetailPanel();
				panel.displayProduct(arg);
				return panel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Product Detail", NameTokens.DETAILS);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
				
        result.add(detailLinkPanel);
        for(String category : NameTokens.CATEGORIES)
        {
        	final String cat = category;
        	HyperlinkedPanel panel = new HyperlinkedPanel() {
        		ProductListing widget;
				Hyperlink link;
				
				@Override
				public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
						String[] args) {
					String[] cats = {cat};
					if(widget == null)
					{
						widget = new ProductListing(cats, null, null);
						widget.addValueChangeHandler(getDetailPanel());
					}
					return widget;
				}
				
				@Override
				public Hyperlink getLink() {
					if(link == null)
						link = new Hyperlink(cat, NameTokens.getNameTargetMap().get(cat));
					return link;
				}

				@Override
				public ArgumentType getArgumentType() {
					return ArgumentType.ORDER_ARG;
				}
			};
			result.add(panel);
        }
        result.add(getShoppingCart());
        result.add(getOrderPanel());
        result.add(getOrderHistoryPanel());
        result.add(getOrderBookedPanel());
        result.addAll(getCreditApplicationPanels());
        result.addAll(getDealerPanels());
        result.add(new StartPage());
        return result;
    }

	public static List<HyperlinkedPanel> getDealerPanels()
	{
		List<HyperlinkedPanel> result = new ArrayList<HyperlinkedPanel>();
		
		HyperlinkedPanel panelCursor = new HyperlinkedPanel() {
    		ProductListing widget;
			Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String[] cats = {NameTokens.ALT_TV, NameTokens.ALT_HAT};
				if(widget == null)
				{
					widget = new ProductListing(null, DTOConstants.APP_PARAM_SAMSUNG_DEALS, null);
					widget.addValueChangeHandler(getDetailPanel());
				}
				return widget;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("SAMSUNG", NameTokens.SAMSUNG);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
        result.add(panelCursor);
        
        panelCursor = new HyperlinkedPanel() {
    		ProductListing widget;
			Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String[] cats = {NameTokens.ALT_RFG};
				if(widget == null)
				{
					widget = new ProductListing(null, DTOConstants.APP_PARAM_THERMO_DEALS, null);
					widget.addValueChangeHandler(getDetailPanel());
				}
				return widget;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("THERMOCOOL", NameTokens.COOL);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
        result.add(panelCursor);        

        panelCursor = new HyperlinkedPanel() {
    		ProductListing widget;
			Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String[] cats = {NameTokens.ALT_RFG};
				if(widget == null)
				{
					widget = new ProductListing(null, DTOConstants.APP_PARAM_LG_DEALS, null);
					widget.addValueChangeHandler(getDetailPanel());
				}
				return widget;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("LG", NameTokens.LG);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
        result.add(panelCursor);        
        
        panelCursor = new HyperlinkedPanel() {
    		ProductListing widget;
			Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String[] cats = {NameTokens.ALT_RFG};
				if(widget == null)
				{
					widget = new ProductListing(null, DTOConstants.APP_PARAM_CHRISTMAS_DEAS, null);
					widget.addValueChangeHandler(getDetailPanel());
				}
				return widget;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("GREAT OFFERS", NameTokens.DEALS);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};        
        result.add(panelCursor);        

        return result;
	}	
	
	public static List<HyperlinkedPanel> getCreditApplicationPanels()
	{
		List<HyperlinkedPanel> result = new ArrayList<HyperlinkedPanel>();
		final ArgumentType type = ArgumentType.LOAN_ARG;
		HyperlinkedPanel panelCursor = new HyperlinkedPanel() {

            private Hyperlink link;
            private MortgageApplicationFormPanel displayPanel;
            private PageJumper tempDisplay;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Continue Application", NameTokens.APPLY);
                }
                return link;
            }
          
            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
        		String loanID = getLoanID(args);
            	if(loanID == null)
        		{
        			if(tempDisplay == null)
        				tempDisplay = new PageJumper();
        			if(isOps)
        				tempDisplay.setHTML("No Applicant Specified", "Use the <a href='http://www.zerofinance.com.ng/addosserops/'>zerofinance staff portal</a>" +
        						" to select an applicant", "Back to Home Page", NameTokens.WELCOME);
        			else
        			{
        				String msg = isLoggedIn? "Use the link below to get started on a new application" : 
        					"If you've applied before login to access your records or use the link in the automated email that was sent" +
        					" to your inbox at the time you applied<br/><br/><b>New to Zero Finance?Login for a better shopping experience, alternatively:</b>";
        				tempDisplay.setHTML("No Recent Application Found", msg, "Click to start a new application", NameTokens.RESPONSIBILITY);
        			}
        			
        			return tempDisplay;
        		}
                if (displayPanel == null) {
                    displayPanel = new MortgageApplicationFormPanel();
                }
                displayPanel.clear();
                
                displayPanel.setLoanID(loanID);
                return displayPanel;  
            }

			@Override
			public ArgumentType getArgumentType() {
				return type;
			}
        };
        result.add(panelCursor);
        panelCursor = new HyperlinkedPanel() {

            private Hyperlink link;
            private MortgageDownloadPanel displayPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Download/Print", NameTokens.PRINT);
                }
                return link;
            }
          
            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
                if (displayPanel == null) {
                    displayPanel = new MortgageDownloadPanel();
                }
                displayPanel.setLoanID(getLoanID(args), isOps, isLoggedIn);
                return displayPanel;
            }

			@Override
			public ArgumentType getArgumentType() {
				return type;
			}
        };
        result.add(panelCursor);
        
        panelCursor = new HyperlinkedPanel() {

            private Hyperlink link;
            private FileIploadPanel displayPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Upload/Review", NameTokens.UPLOAD);
                }
                return link;
            }
          
            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
                if (displayPanel == null) {
                    displayPanel = new FileIploadPanel();
                }
                displayPanel.setLoanID(getLoanID(args), isOps, isLoggedIn);
                return displayPanel;
            }

			@Override
			public ArgumentType getArgumentType() {
				return type;
			}
        };        
        result.add(panelCursor);
        
        panelCursor = new HyperlinkedPanel() {

            private Hyperlink link;
            private MortgageStatus displayPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Status", NameTokens.STATUS);
                }
                return link;
            }
          
            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
                if (displayPanel == null) {
                    displayPanel = new MortgageStatus();
                }
                displayPanel.setLoandID(getLoanID(args), isOps, isLoggedIn);
                return displayPanel;
            }

			@Override
			public ArgumentType getArgumentType() {
				return type;
			}
        };
        result.add(panelCursor);
		return result;
	}
  
    
    
	static Icon ico = new Icon();
	static Modal infoBox = new Modal();
	static HTML message = new HTML();
	static
	{
		ico.setSize(IconSize.TIMES3);
		HorizontalPanel temp = new HorizontalPanel();
		temp.setSpacing(10);
		temp.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		temp.add(ico);
		temp.add(message);
		temp.setSpacing(20);
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(10);
		Button dismiss = new Button("OK");
		dismiss.setType(ButtonType.PRIMARY);
		dismiss.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				infoBox.hide();
			}
		});
		dismiss.setBlock(true);
		container.add(temp);
		container.add(dismiss);
		infoBox.add(container);				
	}
	
	private static GrowlOptions goGood, goBad; 
	static
	{
		goGood = GrowlHelper.getNewOptions();
		goBad = GrowlHelper.getNewOptions();
		goGood.setSuccessType();
		goBad.setDangerType();
	}
	
	public static void showErrorMessage(String error)
	{
		  Growl.growl("ERROR",error,Styles.FONT_AWESOME_BASE + " " + IconType.WARNING.getCssName(),goBad);		
	}	

	public static void showInfoMessage(String info)
	{
		Growl.growl("INFO", info, Styles.FONT_AWESOME_BASE + " " + IconType.INFO_CIRCLE.getCssName(), goGood);
	}
	
	
	public static void setupCompanyOracle(SuggestBox suggestBox)
	{
		String[] companyListVals = {};
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
		for (int counter = 0; counter < companyListVals.length; counter++) {
			oracle.add(companyListVals[counter]);
		}		
	}
	
	public static void updateLatLngVals(final String loanId)
	{
		if(ClientUtils.BankUserCookie.getCookie().isOps()) return; //ensure only applicant positions are tracked
		if(Geolocation.isSupported())
		{
			PositionOptions opts = new Geolocation.PositionOptions();
			opts.setHighAccuracyEnabled(true);
			opts.setTimeout(15 * 1000); //wait 15 seconds
			Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() { 
				@Override
				public void onSuccess(Position result) {
					if(result == null || result.getCoordinates() == null)
						onFailure(null);
					else
						CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId,
							result.getCoordinates().getLatitude(), result.getCoordinates().getLongitude(), null);
				}
				
				@Override
				public void onFailure(PositionError reason) 
				{
					String message = reason==null?"":reason.getMessage();
					CustomerAppHelper.showErrorMessage("Position request failed/denied. " + message);
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
				}
			}, opts);
		}
		else
		{
			CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
		}
	}
	
	public static String getLoanID(String[] args)
	{
		return (args[TOKEN_ARGS_PARAM_IDX] == null?
				args[TOKEN_ARGS_RECENTLOAN_IDX] : args[TOKEN_ARGS_PARAM_IDX]);		
	}
	
}
