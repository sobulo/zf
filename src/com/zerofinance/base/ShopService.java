package com.zerofinance.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.zerofinance.shared.MissingEntitiesException;
import com.zerofinance.shared.TableMessage;

@RemoteServiceRelativePath("../shop")
public interface ShopService extends RemoteService
{

	TableMessage getProductInfo(String manufacturer, String modelNumber);
	TableMessage getProductInfo(String productID);
	String saveProductInfo(TableMessage productInfo);

	HashMap<String, String> getProductDefinitiions();

	List<TableMessage> getProductsTable(String manufacturer,
			String[] categories, Integer limit) throws MissingEntitiesException;

	List<TableMessage> getProductsTable(String dealID) throws MissingEntitiesException;
	
	List<TableMessage> getProductsPricing(String manufacturer) throws MissingEntitiesException;
	
	String addToCart(String productID, int numOfMonths, int numOfItems, boolean isUpdate);
	List<TableMessage> getCart();
	List<TableMessage> checkout(String name, String email, String phone, String company) throws MissingEntitiesException;
	
	List<TableMessage> getOrders();
	List<TableMessage> getOrders(String email);
	List<TableMessage> getOrdersByDate(Date start, Date end);
	List<TableMessage> getItemizedOrder(String orderID);
	List<TableMessage> getItemizedOrder(long orderID);
	String subscribeToMailingList(String name, String email);
}
