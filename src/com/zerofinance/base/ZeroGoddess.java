package com.zerofinance.base;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jdt.internal.core.util.HandleFactory;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.zerofinance.client.shop.CustomerAppHelper;
import com.zerofinance.shared.DTOConstants;
import com.zerofinance.shared.OrderStatus;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.TableMessageHeader;
import com.zerofinance.shared.TableMessageHeader.TableMessageContent;
import com.zerofinance.shared.WorkflowStateInstance;
import com.zerofinance.shared.oauth.ClientUtils.BankUserCookie;

public class ZeroGoddess {
    private final static ShopServiceAsync SHOP_SERVICE = GWT.create(ShopService.class);
    private final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);

    static MyOrderMessageSetter setter = null;
    
	static AsyncCallback<List<TableMessage>> orderCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			setter.handleOrderIdRequestFailure(caught);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.get(0) instanceof TableMessageHeader) //sanity check
				result.remove(0); //remove order header
			if(result.size() == 0)
			{
				onFailure(new IllegalArgumentException("no orders found"));
				return;
			}
			Comparator<TableMessage> comparator = TableMessage.getMessageComparator(DTOConstants.ORDER_DATE_CREATE, TableMessageContent.DATE);
			Collections.sort(result, comparator);
			for(int i = result.size()-1; i >= 0; i--)
			{
				TableMessage m = result.get(i);
				OrderStatus status = OrderStatus.valueOf(m.getText(DTOConstants.ORDER_STATUS));
				if(status.isOrderLinkable())
				{
					setter.setOrderRepMessage(m);
					return;
				}
			}
		}
	};
    
    public static void setMyOrderMessage(final MyOrderMessageSetter setter)
    {
    	setMyOrderMessage(null, setter);
    }

    
    public static void setMyOrderMessage(String email, final MyOrderMessageSetter setter)
    {
    	ZeroGoddess.setter = setter;
    	if(email == null || email.trim().length() == 0)
    		SHOP_SERVICE.getOrders(orderCallback);
    	else
    		SHOP_SERVICE.getOrders(email, orderCallback);
    		
    }
    
    public static void seMyLoanMessage(final MyLoanMessageSetter setter)
    {
    	AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				setter.handleLoanIdRequestFailure(caught);
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				if(result.get(0) instanceof TableMessageHeader) //sanity check
					result.remove(0); //remove order header
				if(result.size() == 0)
				{
					onFailure(new IllegalArgumentException("no applications found"));
					return;
				}
				Comparator<TableMessage> comparator = TableMessage.getMessageComparator(DTOConstants.DATE_CREATED_IDX, TableMessageContent.DATE);
				Collections.sort(result, comparator);
				for(int i = result.size()-1; i >= 0; i--)
				{
					TableMessage m = result.get(i);
					WorkflowStateInstance status = WorkflowStateInstance.valueOf(m.getText(DTOConstants.STATUS_IDX));
					if(status.isOpenState())
					{
						setter.setLoanRepMessage(m);
						setter.showSuccessfulSet();
						return;
					}
				}
			}
		};
		READ_SERVICE.getLoanApplications(callback );
    }

    public static interface MyLoanMessageSetter
    {
    	void setLoanRepMessage(TableMessage m);
    	void handleLoanIdRequestFailure(Throwable caught);
    	void showSuccessfulSet();
    }
    
    public static interface MyOrderMessageSetter
    {
    	void setOrderRepMessage(TableMessage m);
    	void handleOrderIdRequestFailure(Throwable caught);
    }

}


