package com.zerofinance.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.zerofinance.shared.DuplicateEntitiesException;
import com.zerofinance.shared.MissingEntitiesException;
import com.zerofinance.shared.TableMessage;
import com.zerofinance.shared.TableMessageHeader;
import com.zerofinance.shared.WorkflowStateInstance;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("../mortgage")
public interface MortgageService extends RemoteService {
	String[] saveLoanApplicationData(HashMap<String, String> appData, ArrayList<HashMap<String, String>>[] supplementaryData, String loanKeyStr, boolean isSubmit) throws MissingEntitiesException;	
	String[] startLoanApplication(HashMap<String, String> appData) throws DuplicateEntitiesException, MissingEntitiesException;
	HashMap<String, String> getStoredLoanApplication(String loanKeyStr);
	TableMessage getLoanState(String loanID);
	TableMessage getLoanState(long loanID);
	HashMap<String, String> getLoanApplicationWithLoanID(String loanKeyStr);
	ArrayList<HashMap<String, String>>[] getSupplementaryData(String loanKeyStr);
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);		
	List<TableMessage> getSaleLeads(Date startDate, Date endDate);
	String getApplicationFormDownloadLink(String appFormID);
	List<TableMessage> getSalesLeadCollection(String loanID);
	String changeApplicationState(String loanStr, WorkflowStateInstance state,
			String message) throws MissingEntitiesException;
	HashMap<WorkflowStateInstance, Integer> getLeadAggregates(Date startDate,Date endDate);
}
