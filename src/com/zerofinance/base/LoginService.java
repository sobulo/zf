package com.zerofinance.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.zerofinance.shared.MissingEntitiesException;
import com.zerofinance.shared.TableMessage;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../login")
public interface LoginService extends RemoteService {
	//LoginInfo login(String requestUrl);
	//LoginInfo loginForApplications(String requestUrl);
	List<TableMessage> getLoanApplications(String email);
	List<TableMessage> getLoanApplications();
	String loadActivityComments(String parentKey);
	List<TableMessage> getRecentActivityComments();
	void saveActivityComment(String commentID, String html, boolean b);
	List<TableMessage> getAttachments(String loanID);
	String saveAttachments(String loanId, List<TableMessage> attachments);
	String saveMapAttachment(String loanId, Double lat, Double lng);
	String deleteAttachment(String attachID);	
	List<TableMessage> getMapAttachments(Date startDate, Date endDate);
	List<TableMessage> getUploads(String loanID) throws MissingEntitiesException;
	String getUploadUrl();
	String deleteUpload(String uploadID) throws MissingEntitiesException;
	List<TableMessage> getApplicationParameter(String ID);
	String saveApplicationParameter(String id, HashMap<String, String> val) throws MissingEntitiesException;
	String getImageUploadHistory(String imageID);
}
