package com.zerofinance.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.zerofinance.shared.TableMessage;

public interface ShopServiceAsync {
	void getProductInfo(String manufacturer, String modelNumber,
			AsyncCallback<TableMessage> callback);

	void saveProductInfo(TableMessage productInfo,
			AsyncCallback<String> callback);

	void getProductDefinitiions(AsyncCallback<HashMap<String, String>> callback);

	void getProductsTable(String manufacturer, String[] categories, Integer limit, 
			AsyncCallback<List<TableMessage>> callback);

	void addToCart(String productID, int numOfMonths, int numOfItems,
			boolean isUpdate, AsyncCallback<String> callback);

	void getCart(AsyncCallback<List<TableMessage>> callback);

	void checkout(String name, String email, String phone, String company,
			AsyncCallback<List<TableMessage>> callback);

	void getProductInfo(String productID, AsyncCallback<TableMessage> callback);

	void getOrders(AsyncCallback<List<TableMessage>> callback);

	void getOrders(String email, AsyncCallback<List<TableMessage>> callback);

	void getOrdersByDate(Date start, Date end,
			AsyncCallback<List<TableMessage>> callback);

	void getItemizedOrder(String orderID,
			AsyncCallback<List<TableMessage>> callback);

	void getItemizedOrder(long orderID,
			AsyncCallback<List<TableMessage>> callback);

	void getProductsPricing(String manufacturer,
			AsyncCallback<List<TableMessage>> callback);

	void getProductsTable(String dealID,
			AsyncCallback<List<TableMessage>> callback);

	void subscribeToMailingList(String name, String email,
			AsyncCallback<String> callback);
}
