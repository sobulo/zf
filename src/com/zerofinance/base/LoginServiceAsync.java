package com.zerofinance.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.zerofinance.shared.TableMessage;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface LoginServiceAsync {
	void getLoanApplications(String email,
			AsyncCallback<List<TableMessage>> callback);

	void getLoanApplications(AsyncCallback<List<TableMessage>> callback);

	void loadActivityComments(String parentKey, AsyncCallback<String> callback);

	void getRecentActivityComments(AsyncCallback<List<TableMessage>> callback);

	void saveActivityComment(String commentID, String html, boolean b,
			AsyncCallback<Void> saveCallback);

	void getAttachments(String loanID,
			AsyncCallback<List<TableMessage>> callback);

	void saveAttachments(String loanId, List<TableMessage> attachments,
			AsyncCallback<String> callback);

	void deleteAttachment(String attachID, AsyncCallback<String> callback);

	void getUploads(String loanID, AsyncCallback<List<TableMessage>> callback);

	void deleteUpload(String uploadID, AsyncCallback<String> callback);

	void getUploadUrl(AsyncCallback<String> callback);

	void getApplicationParameter(String ID,
			AsyncCallback<List<TableMessage>> callback);

	void saveApplicationParameter(String id, HashMap<String, String> val,
			AsyncCallback<String> callback);

	void getImageUploadHistory(String imageID, AsyncCallback<String> callback);

	void getMapAttachments(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	void saveMapAttachment(String loanId, Double lat, Double lng,
			AsyncCallback<String> callback);
}
